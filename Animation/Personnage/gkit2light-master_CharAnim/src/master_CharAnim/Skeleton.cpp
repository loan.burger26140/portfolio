
#include "Skeleton.h"

using namespace chara;

void Skeleton::init(const BVH& bvh)
{
    int nbJoint = bvh.getNumberOfJoint();
    for (int i = 0; i < nbJoint; i++) {
        SkeletonJoint sj;
        sj.m_parentId = bvh.getJoint(i).getParentId();
        m_joints.push_back(sj);
    }
}


Point Skeleton::getJointPosition(int i) const
{
    return m_joints[i].m_l2w(Point(0, 0, 0));
}


int Skeleton::getParentId(const int i) const
{
    
    return m_joints[i].m_parentId;
}

float Skeleton::getDataInterpole(float i, BVH _bvh, BVHChannel _bvhc) {
    int entier = int(i);
    int entierSuivant = (int)(i + 1) % (_bvh.getNumberOfFrame()-1);
    float decimal = i - entier;
    return (_bvhc.getData(entier) * (1 - decimal)) + (_bvhc.getData(entierSuivant) * (decimal));
}

void Skeleton::setPose(const BVH& bvh, float frameNumber)//, const BVH& bvh2)
{
    for (int idJoint = 0; idJoint < m_joints.size(); idJoint++) {
        float x, y, z, x2, y2, z2;
        bvh.getJoint(idJoint).getOffset(x, y, z);
        Transform l2f = Translation(x, y, z);
        for (int idChannel = 0; idChannel < bvh.getJoint(idJoint).getNumberOfChannel(); idChannel++) {
            BVHChannel bvhc = bvh.getJoint(idJoint).getChannel(idChannel);
            if (bvhc.isRotation()) {
                switch (bvhc.getAxis()) {

                case AXIS_X:
                    l2f = l2f * RotationX(getDataInterpole(frameNumber, bvh, bvhc));    //getData surcharger si c'est un float alors interpolation on a besoin de bvh pour savoir combien de pos il y a de frame pour que la dernier pos s'interpole avec la premiere
                    break;

                case AXIS_Y:
                    l2f = l2f * RotationY(getDataInterpole(frameNumber, bvh, bvhc));
                    break;

                case AXIS_Z:
                    l2f = l2f * RotationZ(getDataInterpole(frameNumber, bvh, bvhc));
                    break;

                default:
                    break;
                }

            }
            else if (bvhc.isTranslation()) {
                switch (bvhc.getAxis()) {

                case AXIS_X:
                    l2f = l2f * Translation(getDataInterpole(frameNumber, bvh, bvhc), 0, 0);
                    break;

                case AXIS_Y:
                    l2f = l2f * Translation(0, getDataInterpole(frameNumber, bvh, bvhc), 0);
                    break;

                case AXIS_Z:
                    l2f = l2f * Translation(0, 0, getDataInterpole(frameNumber, bvh, bvhc));
                    break;

                default:
                    break;
                }
            }
        }
        if (m_joints[idJoint].m_parentId != -1)
            l2f = m_joints[getParentId(idJoint)].m_l2w * l2f;

        m_joints[idJoint].m_l2w = l2f;
    }

        //for (int idJoint = 0; idJoint < m_joints.size(); idJoint++) {
        //    float x2, y2, z2;
        //    bvh2.getJoint(idJoint).getOffset(x2, y2, z2);
        //    Transform l2f2 = Translation(x2, y2, z2);
        //    for (int idChannel = 0; idChannel < bvh2.getJoint(idJoint).getNumberOfChannel(); idChannel++) {
        //        BVHChannel bvhc = bvh2.getJoint(idJoint).getChannel(idChannel);
        //        if (bvhc.isRotation()) {
        //            switch (bvhc.getAxis()) {

        //            case AXIS_X:
        //                l2f2 = l2f2 * RotationX(getDataInterpole(frameNumber, bvh2, bvhc));    //getData surcharger si c'est un float alors interpolation on a besoin de bvh pour savoir combien de pos il y a de frame pour que la dernier pos s'interpole avec la premiere
        //                break;

        //            case AXIS_Y:
        //                l2f2 = l2f2 * RotationY(getDataInterpole(frameNumber, bvh2, bvhc));
        //                break;

        //            case AXIS_Z:
        //                l2f2 = l2f2 * RotationZ(getDataInterpole(frameNumber, bvh2, bvhc));
        //                break;

        //            default:
        //                break;
        //            }

        //        }
        //        else if (bvhc.isTranslation()) {
        //            switch (bvhc.getAxis()) {

        //            case AXIS_X:
        //                l2f2 = l2f2 * Translation(getDataInterpole(frameNumber, bvh2, bvhc), 0, 0);
        //                break;

        //            case AXIS_Y:
        //                l2f2 = l2f2 * Translation(0, getDataInterpole(frameNumber, bvh2, bvhc), 0);
        //                break;

        //            case AXIS_Z:
        //                l2f2 = l2f2 * Translation(0, 0, getDataInterpole(frameNumber, bvh2, bvhc));
        //                break;

        //            default:
        //                break;
        //            }
        //        }
        //    }
        //    if (m_joints[idJoint].m_parentId != -1)
        //        l2f2 = m_joints[getParentId(idJoint)].m_l2w * l2f2;
        //    m_joints[idJoint].m_l2w = l2f2;
        //}



}

float Skeleton::Distance(const Skeleton& a, const Skeleton& b) {
    float sumDistance = 0;
    for (int i = 0; i < a.numberOfJoint(); i++) {
        sumDistance += abs(distance(a.getJointPosition(i), b.getJointPosition(i)));
    }
    return sumDistance;
}
