#include "CharacterController.h"

void CharacterController::accelerate(const float& speed_inc) {
	if (m_v <= 0)
		m_v = 0;

	if (m_v + speed_inc < m_vMax) {
		m_v += speed_inc;
		_state = walk;
	}
	else {
		m_v = m_vMax;
		_state = run;
	}

	m_ch2w = m_ch2w * Translation(0, 0, m_v);  //reel demande = m_ch2w * Translation(m_v, 0, 0);   mais pourquoi deplacer vers x ?
}

void CharacterController::deccelerate(const float& speed_inc) {

	if (m_v >= 0)
		m_v = 0;

	if (m_v - speed_inc > -m_vMax) {
		m_v -= speed_inc;
		_state = walk;
	}
	else {
		m_v = -m_vMax;
		_state = run;
	}
	m_ch2w = m_ch2w * Translation(0, 0, m_v);  //reel demande = m_ch2w * Translation(m_v, 0, 0);   mais pourquoi deplacer vers x ?
}

void CharacterController::turnXZ(const float& rot_angle_v) {
	m_ch2w = m_ch2w * RotationY(rot_angle_v);
}

const Point CharacterController::position() const {
	return Point(m_ch2w.m[0][3], m_ch2w.m[1][3], m_ch2w.m[2][3]);
}

//void CharacterController::jump() {
//	float tempsEcoule = 0.0f;
//	float vitesseVerticale = 2.0f * 1 / 2; //2*hauteur saut / temps saut
//	
//	while (tempsEcoule <= 2) {
//		m_ch2w = m_ch2w * Translation(0, vitesseVerticale * tempsEcoule - 0.5f * 9.81f * tempsEcoule * tempsEcoule, 0);
//		tempsEcoule += 0.01f; // delta de temps arbitraire
//	}
//}

void CharacterController::update(const float dt) {
	if (key_state('z')) {
		accelerate(0.1);
	}
	if (key_state('q')) {
		turnXZ(10);
	}
	if (key_state('s')) {
		deccelerate(0.1);
	}
	if (key_state('d')) {
		turnXZ(-10);
	}
	if (key_state('n')) {
		_state = kick;
	}
	if (!(key_state('z') || key_state('s')) && _state != kick) {
		m_v = 0;
		_state = wait;
	}

}

void CharacterController::afficheState() {
	switch (_state) {
	case walk :
		std::cout << "walk" << std::endl;
		break;
	case run:
		std::cout << "run" << std::endl;
		break;
	case wait:
		std::cout << "wait" << std::endl;
		break;
	case kick:
		std::cout << "kick" << std::endl;
		break;
	}
}
