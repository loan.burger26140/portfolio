#pragma once
#include "mat.h"
#include "window.h"

class CharacterController
{
public:
	CharacterController() : m_ch2w(Identity()), m_v(0), m_vMax(5), _state(wait) {}

	void update(const float dt);

	void turnXZ(const float& rot_angle_v);
	//void jump();
	void accelerate(const float& speed_inc);
	void deccelerate(const float& speed_inc);
	void setVelocityMax(const float vmax) { m_vMax = vmax; };

	const Point position() const;
	const Vector direction() const { m_ch2w(Vector(1, 0, 0)); };
	float velocity() const { return m_v * m_ch2w(Vector(1, 0, 0)).x; };
	const Transform& controller2world() const { return m_ch2w; }

	enum state { wait = 0, walk = 1, run = 2, kick = 3};

	state getState() { return _state; }
	void setState(state _newState) { _state = _newState; }
	void afficheState();

protected:
	Transform m_ch2w;   // matrice du character vers le monde
	// le personnage se d�place vers X
	// il tourne autour de Y
	// Z est sa direction droite
	state _state;
	float m_v;          // le vecteur vitesse est m_v * m_ch2w * Vector(1,0,0)
	float m_vMax;       // ne peut pas acc�l�rer plus que m_vMax
};



