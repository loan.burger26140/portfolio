
#include <cassert>
#include <cmath>
#include <cstdio>
#include <iostream>

#include "CharAnimViewer.h"

using namespace std;
using namespace chara;


CharAnimViewer* CharAnimViewer::psingleton = NULL;


CharAnimViewer::CharAnimViewer() : Viewer(), m_frameNumber(0)
{
	psingleton = this;
}


int CharAnimViewer::init()
{
    Viewer::init();
    cout<<"==>master_CharAnim/CharAnimViewer"<<endl;
    m_camera.lookat( Point(0,0,0), 1000 );
	m_camera.rotation(180, 0);
    gl.light( Point(300, 300, 300 ) );

    //b_draw_grid = false;

    m_world.setParticlesCount( 10 );


    init_cylinder();
    init_sphere();


    //m_bvh[0].init(smart_path("data/bvh/motionFSM/avatar_smoke_idle.bvh"));
	m_bvh[0].init(smart_path("data/bvh/motionFSM/avatar_talk.bvh"));
	m_bvh[1].init(smart_path("data/bvh/motionFSM/avatar_walk.bvh"));
	m_bvh[2].init(smart_path("data/bvh/motionFSM/avatar_run.bvh"));
	m_bvh[3].init(smart_path("data/bvh/motionFSM/avatar_kick_roundhouse_R.bvh"));
	//m_bvh.init( smart_path("data/bvh/danse.bvh") );

    m_frameNumber = 0;
    cout<<endl<<"========================"<<endl;
    cout<<"BVH decription"<<endl;
    cout<<m_bvh<<endl;
    cout<<endl<<"========================"<<endl;

    m_ske.init( m_bvh[0]); 
    m_ske.setPose( m_bvh[0], -1);// met le skeleton a la pose au repos

	testSke.init(m_bvh[0]);
	testSke.setPose(m_bvh[0], 0);// met le skeleton a la pose au repos

	std::vector<chara::BVH> bvhDyna;
	for (int i = 0; i < 4; i++) {
		bvhDyna.push_back(m_bvh[i]);
		
	}
	MG.init(bvhDyna);
	//MG.print();

    return 0;
}



void CharAnimViewer::draw_skeleton(const Skeleton& ske , Transform t)
{
	for (int i = 0; i < ske.numberOfJoint(); i++) {
		draw_sphere(t(ske.getJointPosition(i)));
		if (ske.getParentId(i) != -1)
			draw_cylinder(t(ske.getJointPosition(ske.getParentId(i))), t(ske.getJointPosition(i)),0.5);
	}
}



int CharAnimViewer::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //draw_quad( RotationX(-90)*Scale(500,500,1) );

    Viewer::manageCameraLight();
    gl.camera(m_camera);

	// Affiche les particules physiques (Question 3 : interaction personnage sphere/ballon)
    //m_world.draw();


	// Affiche le skeleton � partir de la structure lin�aire (tableau) Skeleton
	draw_skeleton(m_ske, CC.controller2world());
	//draw_skeleton(m_ske, Identity());
	
	







#if 0			// exercice du cours
	float A[] = { 0, -10, -20, -30, -40 };
	float B[] = { 0, -10, -20, -30, -40 };
	float C[] = { 0, -10, -20, -30, -40 };
	float D[] = { 0, 10, 20, 30, 40 };
	float a, b, c, d;

	static int t = 3;
	if (t == 4) t = 0; else t++;

	a = A[t];
	b = B[t];
	c = C[t];
	d = D[t];

	Transform scaleS = Scale(12, 12, 12);
	Transform scaleA = Scale(20, 20, 20);
	Transform scale = Scale(10, 100, 10);
	Transform A2W = RotationZ(a);
	draw_cylinder(A2W*scale);
	draw_sphere(A2W*scaleS);
	draw_axe(A2W*scaleA);

	Transform B2A = Translation(0, 100, 0) * RotationZ(b);
	Transform B2W = A2W * B2A;
	draw_cylinder(B2W*scale);
	draw_sphere(B2W*scaleS);
	draw_axe(B2W*scaleA);

	Transform scaleP = Scale(5, 50, 5);
	Transform C2B = Translation(0, 100, 0) * RotationZ(c);
	Transform C2W = B2W * C2B;
	draw_cylinder(C2W*scaleP);
	draw_sphere(C2W*scaleS);
	draw_axe(C2W*scaleA);

	Transform D2B = Translation(0, 100, 0) * RotationZ(d);
	Transform D2W = B2W * D2B;
	draw_cylinder(D2W*scaleP);
	draw_axe(D2W*scaleA);
	draw_sphere(D2W*scaleS);
#endif


    return 1;
}

float modulo(float numerateur, float diviseur) {
	float quotient = numerateur / diviseur;
	int entier = (int)quotient;
	float reste = numerateur - entier * diviseur;
	return reste;
}



int CharAnimViewer::update( const float time, const float delta )
{
    // time est le temps ecoule depuis le demarrage de l'application, en millisecondes,
    // delta est le temps ecoule depuis l'affichage de la derniere image / le dernier appel a draw(), en millisecondes.

	int minIndex = 0;
	CC.update(delta);
	if (currentState != CC.getState()) {
		timeCurrentAnim = time;
		currentState = CC.getState();
		float minDis = 1000;

		for (int i = 0; i < m_bvh[currentState].getNumberOfFrame(); i++) {
			testSke.setPose(m_bvh[currentState], i);
			if (m_ske.Distance(m_ske, testSke) < minDis) {
				minDis = m_ske.Distance(m_ske, testSke);
				std::cout << minDis << std::endl;
				minIndex = i;
			}
		}
		std::cout << minIndex << std::endl;
	}
	
	float timeFrame = minIndex + modulo(((time - timeCurrentAnim) / 100) , m_bvh[CC.getState()].getNumberOfFrame()-1);
	if (CC.getState() == CharacterController::state::kick) {
		if (timeFrame <= 0.5){
			CC.setState(CharacterController::state::wait);
		}
	}

	m_ske.setPose(m_bvh[CC.getState()], timeFrame);//, m_bvh[currentState]);
	m_world.update(0.1f);

    return 0;
}




