#include "MotionGraph.h"

void MotionGraph::init(std::vector<chara::BVH> _BVH) {
	for (int i = 0; i < _BVH.size(); i++) {
		m_BVH.push_back(_BVH.at(i));
	}


	Skeleton skeRef;
	Skeleton skeTest;
	int compteur = 0;

	for (int indexBVH = 0; indexBVH < m_BVH.size(); indexBVH++) {
		for (int indexFrame = 0; indexFrame < m_BVH[indexBVH].getNumberOfFrame(); indexFrame++) {
			std::vector<GrapheNodeID> nxt;
			nxt.clear();
			m_GrapheNode.push_back(GrapheNode(indexBVH, indexFrame, compteur, nxt));
			compteur++;
		}
	}



	for (int i = 0; i < m_GrapheNode.size(); i++) {
		//std::cout << m_GrapheNode.at(i).id_bvh<< "   et   " << m_GrapheNode.at(i).frame << std::endl;
		//skeRef.init(m_BVH[m_GrapheNode.at(i).id_bvh]);
		skeRef.setPose(m_BVH[m_GrapheNode.at(i).id_bvh], m_GrapheNode.at(i).frame);
		for (int indexOtherBVH = 0; indexOtherBVH < m_BVH.size(); indexOtherBVH++) {
			for (int indexOtherFrame = 0; indexOtherFrame < m_BVH[indexOtherBVH].getNumberOfFrame(); indexOtherFrame++) {
				//skeTest.init(m_BVH[indexOtherBVH]);
				//std::cout << indexOtherBVH<< "   et   " << indexOtherFrame << std::endl;
				skeTest.setPose(m_BVH[indexOtherBVH], indexOtherFrame);
				//std::cout << skeRef.Distance(skeRef, skeTest) << std::endl;
				//distance a 0 tout le temps car ske pas init mais si init probleme dans setPose getJoint
				if (skeRef.Distance(skeRef, skeTest) < 100) {
					m_GrapheNode.at(i).ids_next.push_back(getIDGlobal(indexOtherBVH, indexOtherFrame));
				}
			}
		}
	}
}

int MotionGraph::getBVHID(int idGlobal) {
	for (int i = 0; i < m_GrapheNode.size(); i++) {
		if (m_GrapheNode.at(i).idglobal == idGlobal)
			return m_GrapheNode.at(i).id_bvh;
	}
}

int MotionGraph::getFrame(int idGlobal) {
	for (int i = 0; i < m_GrapheNode.size(); i++) {
		if (m_GrapheNode.at(i).idglobal == idGlobal)
			return m_GrapheNode.at(i).frame;
	}
}

int MotionGraph::getIDGlobal(int idBVH, int frame) {
	for (int i = 0; i < m_GrapheNode.size(); i++) {
		if (m_GrapheNode.at(i).id_bvh == idBVH && m_GrapheNode.at(i).frame == frame)
			return m_GrapheNode.at(i).idglobal;
	}
}

void MotionGraph::print() {
	std::cout << "Graph size " << m_GrapheNode.size() << std::endl;
	for (int i = 0; i < m_GrapheNode.size(); i++) {
		GrapheNode node = m_GrapheNode.at(i);
		std::cout << "Node " << node.idglobal << " is Frame " << node.frame << " from BVH " << node.id_bvh << " and has nexts ";
		for (int j = 0; j < m_GrapheNode.at(i).ids_next.size(); j++) {
			std::cout << m_GrapheNode.at(i).ids_next.at(j) << " ";
		}
		std::cout << std::endl;
	}
}