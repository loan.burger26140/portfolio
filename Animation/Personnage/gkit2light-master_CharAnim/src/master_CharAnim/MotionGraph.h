#pragma once
#include "BVH.h"
#include "Skeleton.h"

class MotionGraph
{
public :
    void init(std::vector<chara::BVH> _BVH);
    int getBVHID(int idGlobal);
    int getFrame(int idGlobal);
    int getIDGlobal(int idBVH, int frame);
    void print();

    
protected:
    //! L'ensemble des BVH du graphe d'animation
    std::vector<chara::BVH> m_BVH;   //liste de tout les bvh

    //! Un noeud du graphe d'animation est rep�r� par un entier=un identifiant
    typedef int GrapheNodeID;       

    //! Une animation BVH est rep�r�e par un identifiant=un entier 
    typedef int BVH_ID;   //enum de CC

    //! Un noeud du graphe contient l'identifiant de l'animation, le num�ro 
    //! de la frame et les identifiants des noeuds successeurs 
    //! Remarque : du code plus "joli" aurait cr�er une classe CAGrapheNode
    struct GrapheNode
    {
        BVH_ID id_bvh;
        int frame;
        int idglobal;
        std::vector<GrapheNodeID> ids_next;     //! Liste des n�uds successeurs 

        GrapheNode(BVH_ID idBVH, int _frame, int idGlobal, std::vector<GrapheNodeID> nxt) : id_bvh(idBVH), frame(_frame), idglobal(idGlobal), ids_next(nxt){};
    };


    //! Tous les noeuds du graphe d'animation
    std::vector<GrapheNode> m_GrapheNode;

};

