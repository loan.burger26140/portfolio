/*
 * CalculsMSS.cpp :
 * Copyright (C) 2016 Florence Zara, LIRIS
 *               florence.zara@liris.univ-lyon1.fr
 *               http://liris.cnrs.fr/florence.zara/
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/** \file CalculsMSS.cpp
Programme calculant pour chaque particule i d un MSS son etat au pas de temps suivant
 (methode d 'Euler semi-implicite) : principales fonctions de calculs.
\brief Fonctions de calculs de la methode semi-implicite sur un systeme masses-ressorts.
*/

#include <stdio.h>
#include <math.h>
#include <vector>
#include <iostream>

#include "vec.h"
#include "ObjetSimule.h"
#include "ObjetSimuleMSS.h"
#include "Viewer.h"

using namespace std;





/**
* Calcul des forces appliquees sur les particules du systeme masses-ressorts.
 */
void ObjetSimuleMSS::CalculForceSpring()
{
    ///f = somme_i (ki * (l(i,j)-l_0(i,j)) * uij ) + //(nuij * (vi - vj) * uij) + //(m*g) + force_ext;

    float nu ,lengthAB, raideur, Lrepos;
    Particule* partA;
    Particule* partB;
    Vector AB, uAB, fRaideur, fAmmort, mssForce, vitAB, posA, posB;
    int idA,idB;

    for(int i = 0; i<P.size(); i++)
        Force[i] = 0;

    std::vector<Ressort *>& ressortList = _SystemeMasseRessort->GetRessortList();
    int index = 0;
    for (Ressort* ressort : ressortList){
        Lrepos = ressort->GetLrepos();
        raideur = ressort->GetRaideur();
        nu = ressort->GetAmortissement();
        partA = ressort->GetParticuleA();
        partB = ressort->GetParticuleB();

        idA = partA->GetId();
        idB = partB->GetId();
        vitAB = V[idA] - V[idB];

        posA = P[idA];
        posB = P[idB];
        AB = posA - posB;
        lengthAB = length(AB);
        uAB = AB/ lengthAB;

        fRaideur = raideur * (lengthAB - Lrepos) * uAB;
        fAmmort = nu * dot(vitAB,uAB);
        mssForce = fRaideur+fAmmort;
        if (lengthAB >= 100.0f) {
            vector<Ressort*>::iterator it = ressortList.begin();

            while (it != ressortList.end()) {
                if (*it == ressort) {
                    it = ressortList.erase(it);
                }
                it++;
            }
        }
        Force[idA] = Force[idA] - mssForce;
        Force[idB] = Force[idB] + mssForce;
        index++;
    }
}



/**
 * Gestion des collisions avec le sol.
 */
void ObjetSimuleMSS::Collision()
{
    float rayon = 1;
    Vector center = Vector(3, -7, -3);


     //Arret de la vitesse quand touche le plan
    for (int i = 0; i < P.size(); i++) {
        if (P[i].y < -10) {
            P[i].y = -10;
            V[i] = 0;
            A[i] = 0;
        }
        if (length(center - P[i]) <= rayon) {
            P[i] = center + (P[i] - center) * (rayon / length(P[i] - center) * 1.01);   //1.05 pour etre au dessus de la sphere et pas un peu dedans 
            V[i] = 0;
            A[i] = 0;
        }
    }
}// void

