// exemple de code test a compiler pour verifier que tout est ok
#include "color.h"
#include "image.h"
#include "mesh_io.h"
#include "image_io.h"
#include "vec.h"
#include <vector>
#include <algorithm>

const char* filenameEx2 = "data/robot.obj";
const char* filenameEx3 = "data/geometry.obj";

const float inf = std::numeric_limits<float>::infinity();

Vector findMin(Vector a, Vector b, Vector c) {
    float xmin;
    float ymin;
    if (a.x <= b.x) {
        if (a.x <= c.x) {
            xmin = a.x;
        }
        else {
            xmin = c.x;
        }
    }
    else {
        if (b.x <= c.x) {
            xmin = b.x;
        }
        else {
            xmin = c.x;
        }
    }

    if (a.y <= b.y) {
        if (a.y <= c.y) {
            ymin = a.y;
        }
        else {
            ymin = c.y;
        }
    }
    else {
        if (b.y <= c.y) {
            ymin = b.y;
        }
        else {
            ymin = c.y;
        }
    }
    return Vector(xmin, ymin, 0);
}

Vector findMax(Vector a, Vector b, Vector c) {
    float xmax;
    float ymax;
    if (a.x >= b.x) {
        if (a.x >= c.x) {
            xmax = a.x;
        }
        else {
            xmax = c.x;
        }
    }
    else {
        if (b.x >= c.x) {
            xmax = b.x;
        }
        else {
            xmax = c.x;
        }
    }

    if (a.y >= b.y) {
        if (a.y >= c.y) {
            ymax = a.y;
        }
        else {
            ymax = c.y;
        }
    }
    else {
        if (b.y >= c.y) {
            ymax = b.y;
        }
        else {
            ymax = c.y;
        }
    }
    return Vector(xmax, ymax, 0);
}

float areaTriangle(float hauteur, float base) {
    return 0.5 * base * hauteur;
}


void ex1()
{
    // cree l'image resultat
    Image image(1024, 1024);
    Vector a(100, 150, 0);
    Vector b(500, 600, 0);
    Vector c(280, 900, 0);
    Vector min = findMin(a, b, c);
    Vector max = findMax(a, b, c);
    for (int i = 0; i < image.width(); i++) {
        for (int j = 0; j < image.height(); j++) {
            image(i, j) = Black();
        }
    }

    image(a.x, a.y) = Blue();
    image(b.x, b.y) = Green();
    image(c.x, c.y) = Red();
    for (int i = 0; i < image.width(); i++) {
        for (int j = 0; j < image.height(); j++) {
            Vector p(i, j, 0);
            if (p.x >= min.x && p.x<max.x && p.y>min.y && p.y < max.y) {
                if (cross((b - a), (p - a)).z >= 0 &&
                    cross((c - b), (p - b)).z >= 0 &&
                    cross((a - c), (p - c)).z >= 0) {

                    //float areaABC = areaTriangle(length(min), length(max));   //air du triangle d'en face (lambdaA = air triangle bcp)

                    float baryA = 1.f - (length(p - a) / (length(p - a) + length(p - b) + length(p - c)));
                    float baryB = 1.f - (length(p - b) / (length(p - a) + length(p - b) + length(p - c)));
                    float baryC = 1.f - (length(p - c) / (length(p - a) + length(p - b) + length(p - c)));
                    image(i, j) = image(a.x, a.y) * baryA + image(b.x, b.y) * baryB + image(c.x, c.y) * baryC;
                }
            }
        }
    }

    write_image(image, "image.png");
}

void setPosition(std::vector<Point>& positions, const char* filename, Vector translation) {
    if (!read_positions(filename, positions))
        assert(false);
    for (unsigned i = 0; i < positions.size(); i++) {
        positions[i] = positions[i] + translation;
    }
}


void ex2() {
    // cree l'image resultat
    Image image(1024, 1024, Black());

    std::vector<Point> positions;
    setPosition(positions, filenameEx2, Vector(0, -2, -4)); // a ajuster en fonction de l'objet...

    // englobant des points, verifier qu'ils sont bien devant la camera...
    Point pmin = positions[0];
    Point pmax = positions[0];
    for (unsigned i = 1; i < positions.size(); i++)
    {
        pmin = min(pmin, positions[i]);
        pmax = max(pmax, positions[i]);
    }
    printf("bounds [%f %f %f]x[%f %f %f]\n", pmin.x, pmin.y, pmin.z, pmax.x, pmax.y, pmax.z);

    //Volume
    std::vector<float> V(3);

    //lambda
    std::vector<float> lambda(3);

    Image zBuffer(1024, 1024, Color(-1000)); //image 
    float ztest;

    for (unsigned i = 0; i + 2 < positions.size(); i += 3)
    {
        // triangle courant
        Point P[3] = {
            positions[i],
            positions[i + 1],
            positions[i + 2]
        };

        // normale du triangle courant
        Vector N[3] = {
            normalize(cross(Vector(P[2]), Vector(P[1]))),   //p0 de ce triangle
            normalize(cross(Vector(P[0]), Vector(P[2]))),   //p1 de ce triangle
            normalize(cross(Vector(P[1]), Vector(P[0])))   //p2 de ce triangle
        };


        for (int py = 0; py < image.height(); py++) {
            for (int px = 0; px < image.width(); px++)
            {
                // rayon
                float x = (static_cast<float>(px) / image.width()) * 2 - 1;
                float y = (static_cast<float>(py) / image.height()) * 2 - 1;
                float z = -1;
                Point p = Point(x, y, z);
                Point o = Point(0, 0, 0);
                Vector d = Vector(o, p);    // ou Vector d= p - o; // si vous preferrez...

                //Volume
                for (int j = 0; j < 3; j++) {
                    V[j] = dot(N[j], d);
                }

                if ((V[0] >= 0) &&
                    (V[1] >= 0) &&
                    (V[2] >= 0)) {

                    //calcul coord bary
                    for (int j = 0; j < V.size(); j++) {
                        lambda[j] = V[j] / (V[0] + V[1] + V[2]);
                    }

                    //calcul Z et modifie valeur zBuffer si z> ZBuffer
                    ztest = P[0].z * lambda[0] + P[1].z * lambda[1] + P[2].z * lambda[2];
                    if (ztest > zBuffer(px, py).r) {
                        zBuffer(px, py) = Color(ztest);
                        image(px, py) = Color(std::abs(N[0].x * lambda[0] + N[1].x * lambda[1] + N[2].x * lambda[2]),
                            std::abs(N[0].y * lambda[0] + N[1].y * lambda[1] + N[2].y * lambda[2]),
                            std::abs(N[0].z * lambda[0] + N[1].z * lambda[1] + N[2].z * lambda[2]));
                    }
                }
            }
        }
    }
    write_image(image, "image3D.png");
}


bool isInTriangle(const Point& inter, const Vector& d,  const Point p[3]) {
    // Normale
    Vector N[3];
    N[0] = normalize(cross(Vector(p[0], p[1]), Vector(inter, p[0])));
    N[1] = normalize(cross(Vector(p[1], p[2]), Vector(inter, p[1])));
    N[2] = normalize(cross(Vector(p[2], p[0]), Vector(inter, p[2])));

    // Volume
    float V[3];
    for (int j = 0; j < 3; j++) {
		V[j] = dot(d,N[j]);     //TODO inverse param pour test (change rien)
	}

    if (V[0] > 0 && V[1] > 0 && V[2] > 0)
        return true;
    else
        return false;
}

struct Hit
{
    float t;        // position sur le rayon ou inf
    Vector n;       // normale du point d'intersection, s'il existe
    Color color;    // couleur du point d'intersection, s'il existe

    operator bool() { return t >= 0 && t < inf; } // renvoie vrai si l'intersection existe et quelle est valide...
};

Hit intersectTriangle(const Point P[3], const Point& o, const Vector& d, Color color) {

        // Normale
        Vector n = cross(Vector(P[0], P[1]), Vector(P[0], P[2]));

        // Intersection avec le rayon o , d
        float t = dot(n, Vector(o, P[0])) / dot(n, d);

        // point d'intersection
        Point p = o + t * d;

        // l'intersection n'est pas valide / derriere l'origine du rayon
        if ((dot(n, cross(Vector(P[0], P[1]), Vector(P[0], p))) < 0)
            || (dot(n, cross(Vector(P[1], P[2]), Vector(P[1], p))) < 0)
            || (dot(n, cross(Vector(P[2], P[0]), Vector(P[2], p))) < 0)
            ) {
            return { inf, Vector(), color };
        }

        return { t, normalize(n), color };
    }

Hit intersect(const MeshIOData& data, const Point& o, const Vector& d) {

    Hit s;
    s.t = inf;

    for (unsigned i = 0; i + 2 < data.indices.size(); i += 3)
    {
        Point P[3] = {
            data.positions[data.indices[i]],
            data.positions[data.indices[i + 1]],
            data.positions[data.indices[i + 2]]
        };
        Hit  tmp = intersectTriangle(P, o, d, data.materials.materials[data.material_indices[i / 3]].diffuse);

        if (tmp)
        {
            if (tmp.t < s.t)
            {
                s = tmp;
            }
        }
    }
    return s;
}





void ex3() {
    // cree l'image resultat
    Image image(1024, 1024, Black());

    MeshIOData data = read_meshio_data(filenameEx3);
    // deplace tous les sommets devant la camera
    for (unsigned i = 0; i < data.positions.size(); i++)
        data.positions[i] = data.positions[i] + Vector(0, -200, -600); //Vector(0,-2,-4); 

    // englobant des points, verifier qu'ils sont bien devant la camera...
    Point pmin = data.positions[0];
    Point pmax = data.positions[0];
    for (unsigned i = 1; i < data.positions.size(); i++)
    {
        pmin = min(pmin, data.positions[i]);
        pmax = max(pmax, data.positions[i]);
    }
    printf("bounds [%f %f %f]x[%f %f %f]\n", pmin.x, pmin.y, pmin.z, pmax.x, pmax.y, pmax.z);
    std::vector<Point> lumiere;
    lumiere.push_back(Point(400, 230, -190));
    lumiere.push_back(Point(-600, -330, -300));
    lumiere.push_back(Point(400.0, 315.0, -400.0));
    //lumiere.push_back(Point(200, 200, 100));

    //lumiere.push_back(Point(200, 200, 100));



    for (int py = 0; py < image.height(); py++)
    {
        for (int px = 0; px < image.width(); px++)
        {
            // rayon centre camera
            float x = float(px) / image.width() * 2 - 1;
            float y = float(py) / image.height() * 2 - 1;
            float z = -1;
            Point o = Point(0, 0, 0);
            Point p = Point(x, y, z);
            Vector d = Vector(o, p);    // ou Vector d= p - o; // si vous preferrez...

            Hit h = intersect(data, o, d);
            
            if (h)
            {
                Point p = o + h.t * d;
                p = p + h.n * 0.001; // on d�cale d'un epsilon
                Color pixel = Color(0, 0, 0);
                float intensite = 0;

                for (Point lum : lumiere)
                {
                    //for(int j = 0; j<=25; j++)  //soft shadow
                    //{
                    //    float delta = 1.0 / 25.0;
                    //    Point variance(rand() * 2 - 1, rand() * 2 - 1, rand() * 2 - 1);  //soft shadow

                        // calcul �clairage
                        //Vector l = Vector(p, lum + variance);  //soft shadow
                        Vector l = Vector(p, lum);
                        float cos_theta = std::max(float(0), dot(normalize(h.n), normalize(l))); // cosinus de l'angle entre la normale et le rayon lumineux

                        // Calcul l'intersection avec la lumiere et la scene
                        Hit i = intersect(data, p, l);

                        //if (i) {
                        //    intensite += delta;  //soft shadow
                        //}

                        if (!i || (i.t > 1)) // V�rifier ombre
                        {
                            pixel = (pixel + (White() * h.color * cos_theta));
                        }
                    //}     //soft shadow
                }
                // ajouter le pixel
                image(px, py) = Color(pixel, 1);
                //image(px, py) = Color(pixel, 1 - intensite);   //soft shadow
            }            
        }
       
    }
    write_image(image, "imageIntersection.png");
}







int main(void) {
    ex1();
    //ex2();
    //ex3();

    return 0;
}