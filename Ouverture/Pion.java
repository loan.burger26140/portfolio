public class Pion {
    private Couleur couleur;
    private TypePion type;
    private boolean selected;

    public Pion(Couleur couleur) {
        this.couleur = couleur;
        this.type = TypePion.PION;
        this.selected = false;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public void setCouleur(Couleur couleur) {
        this.couleur = couleur;
    }

    public boolean isPion() {
        if (type != TypePion.PION) {
            return false;
        }
        return true;
    }

    public boolean isDame() {
        if (type != TypePion.DAME) {
            return false;
        }
        return true;
    }

    public void setDame() {
        this.type = TypePion.DAME;
    }

    public boolean isRoi() {
        if (type != TypePion.ROI) {
            return false;
        }
        return true;
    }

    public void upgrade(){
        if (this.type == TypePion.DAME){
            this.type = TypePion.ROI;
        }
        if (this.type == TypePion.PION){
            this.type = TypePion.DAME;
        }
    }


    public void setRoi() {
        this.type = TypePion.ROI;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected() {
        this.selected = true;
    }

    public void setUnselected() {
        this.selected = false;
    }

    TypePion getType() {
        return type;
    }

}

enum Couleur {
    ROUGE, NOIR
}

enum TypePion {
    PION, DAME, ROI
}

//REGLE du ROI :
// Pour obtenir un roi il faut retourné dans son camps avec une dame.
// Le roi peut etre capturé comme un pion.
// Le roi perd les fonctionalité de la dame.
// Le roi peut se déplacer dans n'importe quelle direction d'autant de cases qu'il veut, sauf pour manger ou il peut se deplacer en diagonale d'une case.
// Le roi ne mange pas de pions quand il se deplace en vertical ou horizontal. Utilité : ne pas etre trop broken et juste pouvoir construire le jeux
// Le roi ne peux pas capture apres un deplacement horizontal ou vertical.

//REGLE de la DAME :
// Pour obtenir une dame il faut aller dans le camps adverse avec un pion.
// La dame peut etre capturé comme un pion.
// La dame peut se deplacer en diagonale d'autant de cases qu'elle veut.
// La dame peut manger un pion sur ca diagonale





