import java.util.ArrayList;

public class Board {
    private Pion[][] cases;
    public boolean PreviousIsCaptureMove = false;

    public Board() {
        cases = new Pion[8][8];
        initialize();
    }

    // Initialise le plateau de jeu avec les pièces de départ
    private void initialize() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 8; col++) {
                if ((row + col) % 2 == 0) {
                    cases[row][col] = new Pion(Couleur.NOIR);
                }
            }
        }
        for (int row = 5; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if ((row + col) % 2 == 0) {
                    cases[row][col] = new Pion(Couleur.ROUGE);

                }
            }
        }
    }

    public Pion getPion(int row, int col) {
        return cases[row][col];
    }

    public int getXPion(Pion p){
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if(cases[row][col] == p){
                    return row;
                }
            }
        }
        return -1;
    }

    public int getYPion(Pion p){
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if(cases[row][col] == p){
                    return col;
                }
            }
        }
        return -1;
    }

    public Pion getSelectedPion(){
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if(cases[row][col] != null && cases[row][col].isSelected()){
                    return cases[row][col];
                }
            }
        }
        return null;
    }

    public boolean haveSelectedPion(){
        return getSelectedPion() != null;
    }


    
    public boolean invalidMovee(int fromRow, int fromCol, int toRow, int toCol){
        Pion pion = cases[fromRow][fromCol];
        return ((toRow < 0 || toRow > 7 || toCol < 0 || toCol > 7)                                          // Si la case d'arrivée n'est pas vide
            ||  cases[toRow][toCol] != null                                                             // Si le déplacement est hors du plateau
            || (Math.abs(toRow - fromRow) != 1 || Math.abs(toCol - fromCol) != 1) && pion.isPion())     // Si le déplacement n'est pas de 1 case
            || (pion.getCouleur() == Couleur.ROUGE && toRow > fromRow && pion.isPion())                     // Si le pion est rouge et qu'il avance vers le bas
            || (pion.getCouleur() == Couleur.NOIR && toRow < fromRow && pion.isPion())                  // Si le pion est noir et qu'il avance vers le haut
            || (Math.abs(toRow - fromRow) != Math.abs(toCol - fromCol) && pion.isDame())                // Si le déplacement n'est pas diagonal
            || ((pion.isRoi() && ((toRow - fromRow != 0 && (toCol - fromCol)%2 != 0 ) || (toCol - fromCol != 0 && (toRow - fromRow)%2 != 0))));             // Si le déplacement n'est pas de %2 cases Verticalement     
    }


    public boolean haveCaptureMovee(int row, int col, Couleur couleur){ // Vérifie si le pion peut capturer depuis la position (row,col)
        if (row > 1 && col > 1 && cases[row - 1][col - 1]!= null  
        && cases[row - 1][col - 1].getCouleur() != couleur 
        && cases[row - 2][col - 2] == null) { // Si le pion peut capturer vers le haut gauche
            return true;
        }
        if (row > 1 && col < 8 - 2 && cases[row - 1][col + 1]!= null  
        && cases[row - 1][col + 1].getCouleur() != couleur
        && cases[row - 2][col + 2] == null) { // Si le pion peut capturer vers le haut droit
            return true;
        }
        if (row < 8 - 2 && col > 1 && cases[row + 1][col - 1]!= null 
        && cases[row + 1][col - 1].getCouleur() != couleur
        && cases[row + 2][col - 2] == null) { // Si le pion peut capturer vers le bas gauche
            return true;
        }
        if (row < 8 - 2 && col < 8 - 2 && cases[row + 1][col + 1]!= null 
        && cases[row + 1][col + 1].getCouleur() != couleur 
        && cases[row + 2][col + 2] == null) { // Si le pion peut capturer vers le bas droit
            return true;
        }
        return false;
    }
    
    public boolean isNormalMove(int fromRow, int fromCol, int toRow, int toCol){
        return (Math.abs(toRow - fromRow) == 1 
                && Math.abs(toCol - fromCol) == 1 
                && cases[toRow][toCol] == null);
    }

    public boolean isDameMove(int fromRow, int fromCol, int toRow, int toCol){
        Pion pion = cases[fromRow][fromCol];
        if (pion.isDame() && Math.abs(toRow - fromRow) == Math.abs(toCol - fromCol)){
            int row = fromRow;
            int col = fromCol;
            while (row != toRow && col != toCol){
                if (row < toRow){
                    row++;
                } else {
                    row--;
                }
                if (col < toCol){
                    col++;
                } else {
                    col--;
                }
                if (cases[row][col] != null){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public boolean isRoiMove(int fromRow, int fromCol, int toRow, int toCol){
        Pion pion = cases[fromRow][fromCol];
        return pion.isRoi() && ((toRow - fromRow == 0 && (toCol - fromCol)%2 == 0 ) || (toCol - fromCol == 0 && (toRow - fromRow)%2 == 0));
    }

    public boolean isNormalCaptureMove(int fromRow, int fromCol, int toRow, int toCol){
        int captureRow = (fromRow + toRow) / 2;
        int captureCol = (fromCol + toCol) / 2;
        Pion pion = cases[fromRow][fromCol];
        return (Math.abs(toRow - fromRow) == 2 
                && Math.abs(toCol - fromCol) == 2 
                && cases[captureRow][captureCol] != null
                && cases[captureRow][captureCol].getCouleur() != pion.getCouleur());
    }

    public boolean isDameCaptureMove(int fromRow, int fromCol, int toRow, int toCol){
        Pion pion = cases[fromRow][fromCol];
        if(!pion.isDame())
            return false;
        int nbRow = toRow - fromRow;
        int nbCol = toCol - fromCol;
        int captureCol = fromCol;
        int captureRow = fromRow;
        for (int i = 1; i<Math.abs(nbRow) ; i++){
            captureRow = nbRow<0?captureRow-1:captureRow+1;
            captureCol = nbCol<0?captureCol-1:captureCol+1;
            if(cases[captureRow][captureCol] != null
                && cases[captureRow][captureCol].getCouleur() == pion.getCouleur()){ // Si le pion est de la même couleur
                return false;
            }
            if (cases[captureRow][captureCol] != null
                && (cases[captureRow][captureCol].getCouleur() != pion.getCouleur())){  // Si le pion est de la couleur adverse
                    int nextRow = nbRow<0?captureRow-1:captureRow+1;
                    int nextCol = nbCol<0?captureCol-1:captureCol+1;
                    if (cases[nextRow][nextCol] == null
                        && (nextRow == toRow && nextCol == toCol)){ // si ma case d'arrivée est vide et que je suis juste apres ma capture
                        return true;
                    }else{
                        return false;
                    }
            }
        }
        return false;
    }

    public boolean isValidMove(int fromRow, int fromCol, int toRow, int toCol){
        if (notInTheBoard(fromRow, fromCol, toRow, toCol)){
            return false;
        }else
        if (cases[toRow][toCol] != null){
            return false;
        }
        if (isNormalCaptureMove(fromRow, fromCol, toRow, toCol)){
            return true;
        }
        if (isDameCaptureMove(fromRow, fromCol, toRow, toCol)){
            return true;
        }
        if (invalidMovee(fromRow, fromCol, toRow, toCol)){
            return false;
        }
        if (isNormalMove(fromRow, fromCol, toRow, toCol)){
            return true;
        }
        if (isDameMove(fromRow, fromCol, toRow, toCol)){
            return true;
        }
        if (isRoiMove(fromRow, fromCol, toRow, toCol)){
            return true;
        }


        assert(false);
        return false;
    }

    private boolean notInTheBoard(int fromRow, int fromCol, int toRow, int toCol) {
         return (fromRow < 0 || fromRow > 7 || fromCol < 0 || fromCol > 7 || toRow < 0 || toRow > 7 || toCol < 0 || toCol > 7);
                
    }

    public boolean isCaptureMove(int fromRow, int fromCol, int toRow, int toCol){
        
        return (isNormalCaptureMove(fromRow, fromCol, toRow, toCol) || isDameCaptureMove(fromRow, fromCol, toRow, toCol));
    }

    public boolean canUpgrade(int row, Pion pion){
        return (pion.isPion() && ((row == 0 && pion.getCouleur() == Couleur.ROUGE)
                                ||(row == 7 && pion.getCouleur() == Couleur.NOIR)))
            ||  pion.isDame() && ((row == 0 && pion.getCouleur() == Couleur.NOIR)
                                ||(row == 7 && pion.getCouleur() == Couleur.ROUGE));
    }

    public boolean movePiece(int fromRow, int fromCol, int toRow, int toCol) {
        Pion pion = cases[fromRow][fromCol];
        PreviousIsCaptureMove = false;

        if(cases[toRow][toCol] != null){
            return false;
        }
        if(pion==null){
            return false;
        }else{
            if(isValidMove(fromRow, fromCol, toRow, toCol)){
                if (isCaptureMove(fromRow, fromCol, toRow, toCol)){
                    PreviousIsCaptureMove = true;
                    int nbRow = toRow - fromRow;
                    int nbCol = toCol - fromCol;
                    int captureCol = fromCol;
                    int captureRow = fromRow;
                    for (int i = 1; i<Math.abs(nbRow) ; i++){
                        captureRow = nbRow<0?captureRow-1:captureRow+1;
                        captureCol = nbCol<0?captureCol-1:captureCol+1;
                        if (cases[captureRow][captureCol] != null
                            && (cases[captureRow][captureCol].getCouleur() != pion.getCouleur())){
                                cases[captureRow][captureCol] = null;

                        }
                    }
                }
                if (canUpgrade(toRow, pion)){
                    pion.upgrade();
                }
                cases[toRow][toCol] = pion;
                cases[fromRow][fromCol] = null;

                if(!(PreviousIsCaptureMove && haveCaptureMovee(toRow, toCol, pion.getCouleur()))) // Si le pion n'a pas capturé ou qu'il ne peut plus capturer
                    pion.setUnselected();
                return true;
            }
        }
        return false;
    }
    

    /****************************************  Partie IA  ****************************************/
    public Pion [] getPionsIA(Player p) {
        ArrayList<Pion> pions = new ArrayList<Pion>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (cases[i][j] != null && cases[i][j].getCouleur() == p.couleur) {
                    pions.add(cases[i][j]);
                }
            }
        }
        //p.setPions(pions); // On met à jour la liste des pions du joueur
        return pions.toArray(new Pion[pions.size()]);

    }

    // Renvoie la liste des mouvements possibles pour un pion
    public Mouvement[] getMouvements(Pion p) {
        int currentX = getXPion(p);
        int currentY = getYPion(p);
        ArrayList<Mouvement> mouvements = new ArrayList<Mouvement>();
        // On regarde si le pion peut capturer

        if(haveCaptureMovee(currentX, currentY, p.getCouleur())){
            if (p.isPion() || p.isRoi()){
                capturePion(mouvements, currentX, currentY, p.getCouleur());
            } 
            if (p.isDame()){
                captureDame(mouvements, currentX, currentY, p.getCouleur());
            }
        }else{ // si le pion ne peut pas capturer, on regarde si il peut se déplacer
            if (p.isPion()){
                deplacementPion(mouvements, currentX, currentY, p.getCouleur());
            } 
            if (p.isDame()){
                deplacementDame(mouvements, currentX, currentY, p.getCouleur());
            }
            if (p.isRoi()){
                deplacementRoi(mouvements, currentX, currentY, p.getCouleur());
            }
        }
        return mouvements.toArray(new Mouvement[mouvements.size()]);
    }

    private void deplacementRoi(ArrayList<Mouvement> mouvements, int currentX, int currentY, Couleur couleur) {
        for (int i = 2; i < 8; i+=2) {
            if (isValidMove(currentX, currentY, currentX, currentY+i)){
                int value = howValue(currentX, currentY, currentX, currentY+i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX, currentY+i, value)); 
            }
            if (isValidMove(currentX, currentY, currentX, currentY-i)){
                int value = howValue(currentX, currentY, currentX, currentY-i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX, currentY-i, value)); 

            }
            if (isValidMove(currentX, currentY, currentX+i, currentY)){
                int value = howValue(currentX, currentY, currentX+i, currentY, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX+i, currentY, value)); 
            }

            if (isValidMove(currentX, currentY, currentX-i, currentY)){
                int value = howValue(currentX, currentY, currentX-i, currentY, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX-i, currentY, value));
            }
        }
    }

    private void deplacementDame(ArrayList<Mouvement> mouvements, int currentX, int currentY, Couleur couleur) {
        for (int i = 1; i < 8; i++) {
            if (isValidMove(currentX, currentY, currentX+i, currentY+i)){
                int value = howValue(currentX, currentY, currentX+i, currentY+i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX+i, currentY+i, value)); 
            }
            if (isValidMove(currentX, currentY, currentX+i, currentY-i)){
                int value = howValue(currentX, currentY, currentX+i, currentY-i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX+i, currentY-i, value)); 
            }
            if (isValidMove(currentX, currentY, currentX-i, currentY+i)){
                int value = howValue(currentX, currentY, currentX-i, currentY+i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX-i, currentY+i, value)); 
            }
            if (isValidMove(currentX, currentY, currentX-i, currentY-i)){
                int value = howValue(currentX, currentY, currentX-i, currentY-i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX-i, currentY-i, value));
            }
        }
    }


    private void deplacementPion(ArrayList<Mouvement> mouvements, int currentX, int currentY, Couleur couleur) {
        if (isValidMove(currentX, currentY, currentX+1, currentY+1)){
            int value = howValue(currentX, currentY, currentX+1, currentY+1, couleur);
            mouvements.add(new Mouvement(currentX, currentY, currentX+1, currentY+1, value)); 
        }
        if (isValidMove(currentX, currentY, currentX+1, currentY-1)){
            int value = howValue(currentX, currentY, currentX+1, currentY-1, couleur);
            mouvements.add(new Mouvement(currentX, currentY, currentX+1, currentY-1, value));
        }
        if (isValidMove(currentX, currentY, currentX-1, currentY+1)){
            int value = howValue(currentX, currentY, currentX-1, currentY+1, couleur);
            mouvements.add(new Mouvement(currentX, currentY, currentX-1, currentY+1, value));
        }
        if (isValidMove(currentX, currentY, currentX-1, currentY-1)){
            int value = howValue(currentX, currentY, currentX-1, currentY-1, couleur);
            mouvements.add(new Mouvement(currentX, currentY, currentX-1, currentY-1, value)); 
        }

    }

    private void captureDame(ArrayList<Mouvement> mouvements, int currentX, int currentY, Couleur couleur) {
        for (int i = 2; i < 8; i++) {
            if (isValidMove(currentX, currentY, currentX+i, currentY+i) && isCaptureMove(currentX, currentY, currentX+i, currentY+i)){
                int value = howValue(currentX, currentY, currentX+i, currentY+i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX+i, currentY+i, value));          
            }
            if (isValidMove(currentX, currentY, currentX+i, currentY-i) && isCaptureMove(currentX, currentY, currentX+i, currentY-i)){
                int value = howValue(currentX, currentY, currentX+i, currentY-i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX+i, currentY-i, value));
            }
            if (isValidMove(currentX, currentY, currentX-i, currentY+i) && isCaptureMove(currentX, currentY, currentX-i, currentY+i)){
                int value = howValue(currentX, currentY, currentX-i, currentY+i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX-i, currentY+i, value));
            }
            if (isValidMove(currentX, currentY, currentX-i, currentY-i) && isCaptureMove(currentX, currentY, currentX-i, currentY-i)){
                int value = howValue(currentX, currentY, currentX-i, currentY-i, couleur);
                mouvements.add(new Mouvement(currentX, currentY, currentX-i, currentY-i, value));
            }
        }
    }

    private void capturePion(ArrayList<Mouvement> mouvements, int currentX, int currentY, Couleur couleur) {
        if (isValidMove(currentX, currentY, currentX+2, currentY+2) && isNormalCaptureMove(currentX, currentY, currentX+2, currentY+2)){
            int value = howValue(currentX, currentY, currentX+2, currentY+2, couleur);
            mouvements.add(new Mouvement(currentX, currentY, currentX+2, currentY+2, value));
        }
        if (isValidMove(currentX, currentY, currentX+2, currentY-2) && isNormalCaptureMove(currentX, currentY, currentX+2, currentY-2)){
            int value = howValue(currentX, currentY, currentX+2, currentY-2, couleur);
            mouvements.add(new Mouvement(currentX, currentY, currentX+2, currentY-2, value));
        }
        if (isValidMove(currentX, currentY, currentX-2, currentY+2) && isNormalCaptureMove(currentX, currentY, currentX-2, currentY+2)){
            int value = howValue(currentX, currentY, currentX-2, currentY+2, couleur);
            mouvements.add(new Mouvement(currentX, currentY, currentX-2, currentY+2, value));
        }
        if (isValidMove(currentX, currentY, currentX-2, currentY-2) && isNormalCaptureMove(currentX, currentY, currentX-2, currentY-2)){
            int value = howValue(currentX, currentY, currentX-2, currentY-2, couleur);
            mouvements.add(new Mouvement(currentX, currentY, currentX-2, currentY-2, value));
        }
    }


    boolean canIBeCaptured(int x, int y, Couleur couleur) {
        
        if(x==0 || y==0 || x==7 || y==7) // Si la case est sur le bord, on ne peut pas être capturé
            return false;

        if (cases[x+1][y+1] != null && cases[x+1][y+1].getCouleur() != couleur && cases[x-1][y-1] == null){
            return true;
        }else if (cases[x+1][y-1] != null && cases[x+1][y-1].getCouleur() != couleur && cases[x-1][y+1] == null){
            return true;
        }else if (cases[x-1][y+1] != null && cases[x-1][y+1].getCouleur() != couleur && cases[x+1][y-1] == null){
            return true;
        }else if (cases[x-1][y-1] != null && cases[x-1][y-1].getCouleur() != couleur && cases[x+1][y+1] == null){
            return true;
        }
        return false;
    }

    TypePion whoICapture(int fromX, int fromY, int toX, int toY){
        int nbRow = toX - fromX;
        int nbCol = toY - fromY;
        int captureCol = fromY;
        int captureRow = fromX;
        for (int i = 1; i<Math.abs(nbRow) ; i++){
            captureRow = nbRow<0?captureRow-1:captureRow+1;
            captureCol = nbCol<0?captureCol-1:captureCol+1;
            if (cases[captureRow][captureCol] != null
                && (cases[captureRow][captureCol].getCouleur() != cases[fromX][fromY].getCouleur())){ 
                    return cases[captureRow][captureCol].getType();
            }
        }
        return null;
    }

    boolean toBordure(int x, int y){
        return x==0 || y==0 || x==7 || y==7;
    }

    int howValue(int fromX, int fromY, int toX, int toY, Couleur couleur){
        int value = 4;

        if(canIBeCaptured(toX, toY, couleur)){
            if((cases[fromX][fromY].getType() == TypePion.PION)){ //si je suis un pion
                value -= 4;
            }else if(cases[fromX][fromY].getType() == TypePion.DAME){ //si je suis une dame
                value -= 7;
            }
        }
        if(isCaptureMove(fromX, fromY, toX, toY)){
            TypePion type = whoICapture(fromX, fromY, toX, toY);
            if(haveCaptureMovee(toX,toY,couleur)){
                value += 4;
            }
            if(cases[fromX][fromY].getType() == TypePion.PION){
                if(type == TypePion.PION){
                    value += 5;
                }else if(type == TypePion.ROI){
                    value += 6;
                }else if(type == TypePion.DAME){
                    value += 9;
                }
            }else if(cases[fromX][fromY].getType() == TypePion.DAME){
                if(type == TypePion.PION){
                    value += 4;
                }else if(type == TypePion.ROI){
                    value += 5;
                }else if(type == TypePion.DAME){
                    value += 8;
                }
            }
        }else{
            if(cases[fromX][fromY].getType() == TypePion.DAME)
                value -= 2;
            if(cases[fromX][fromY].getType() == TypePion.ROI)
                value -= 1;
        }

        if(canUpgrade(toX, cases[fromX][fromY])){
            value += 2;
        }
        if(toBordure(toX, toY) && cases[fromX][fromY].getType() != TypePion.ROI){
            value += 1;
        }

        return value;
    }

}
