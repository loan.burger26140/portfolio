
public class Mouvement {
    int fromPosX;
    int fromPosY;
    int toPosX;
    int toPosY;
    int value;

    Mouvement(){
        this.fromPosX = 0;
        this.fromPosY = 0;
        this.toPosX = 0;
        this.toPosY = 0;
        this.value = 0;
    }

    Mouvement(int fromPosX, int fromPosY, int toPosX, int toPosY, int value){
        this.fromPosX = fromPosX;
        this.fromPosY = fromPosY;
        this.toPosX = toPosX;
        this.toPosY = toPosY;
        this.value = value;
    }

    void afficher(){
        System.out.println("fromPosX : " + fromPosX + " fromPosY : " + fromPosY + " toPosX : " + toPosX + " toPosY : " + toPosY + " value : " + value);
    }

}
