public class Player {
    Couleur couleur;
    TypePlayer type;

    public Player(Couleur couleur, TypePlayer type) {
        this.couleur = couleur;
        this.type = type;
    }
};


enum TypePlayer {
    HUMAIN, IA
}