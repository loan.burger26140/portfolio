# Projet "Ouverture à la recherche" : IA-Modélisation distribuée d’un jeu stratégique, cas d’une variante du jeu de dames

Ce projet a été réalisé dans le cadre de l'UE Ouverture à la recherche avec l'encadrant Samir Aknine par Antoine BILLOD, Loan BURGER et Mathieu MURA (alternant).

## Description

L'objectif de ce projet était de développer une intelligence artificielle pour une variante du jeu de dames, en utilisant une modélisation distribuée pour améliorer les performances. Nous avons choisi de développer le projet en Java, en utilisant la librairie Swing pour l'affichage.

Nous avons commencé le projet from scratch, en développant l'interface utilisateur et le jeu de dames. Nous avons également ajouté la règle du roi pour cette variante, avec des fonctionnalités et des limitations spécifiques.

Concernant la partie IA, nous avons créé une unité centralisée qui lance en parallèle différents fils d'exécution pour déterminer le meilleur coup à jouer. Notre application permet de jouer humain contre humain, humain contre IA ou IA contre IA.

## Comment exécuter le programme

Pour lancer une partie, il suffit de compiler notre programme et de l'executer avec les commandes :
javac Affichage.java
java Affichage

Ensuite il suffit de sélectionner l'un des trois modes de jeu disponibles :

- 1V1 : une partie en humain contre humain.
- BotvBot : une partie entre 2 IA.
- 1vBot : une partie entre un humain et une IA.

## Vidéo de démonstration

Une partie d'IA contre IA est disponible en vidéo dans le dépôt Git (videoGamePlay.mp4).

## Conclusion

Ce projet nous a permis de découvrir les défis de la modélisation distribuée pour les applications d'IA, ainsi que de développer nos compétences en programmation en Java et en utilisation de la bibliothèque Swing.
