import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.*;


public class Affichage extends JFrame {

    private Board board;
    private JPanel drawPanel;
    private Player players [];
    private Player joueur;
    JButton button_2p = new JButton("1V1");
    JButton button_1p = new JButton("1VBOT");
    JButton button_0p = new JButton("BOTVBOT");
    private static final int SIZE = 70;

    private enum Etat {
        MENU, JOUER, FIN
    }
    private Etat etat = Etat.MENU;

    public Affichage() {
        players = new Player[2];
        board = new Board();
        setTitle("Jeu de dames");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        // Créer une zone de dessin de taille fixe pour le plateau de jeu
        drawPanel = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                restart(g);
            }
        };

        drawPanel.add(button_2p);
        drawPanel.add(button_0p);
        drawPanel.add(button_1p);
        drawPanel.setPreferredSize(new Dimension(SIZE * 8, SIZE * 8));
        add(drawPanel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void restart(Graphics g){
        if(etat == Etat.MENU){
            drawMenu(g);
        }else if (etat == Etat.JOUER) {
            drawBoard(g);
        }else if (etat == Etat.FIN) {
            drawEnd(g);
        }
    }

    // Dessine le Menu
    private void drawMenu(Graphics g) {
        drawPanel.setLayout(new BoxLayout(drawPanel, BoxLayout.Y_AXIS));
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.BOLD, 30));
        g.drawString("Jeu de dames", 100, 100);

        button_0p.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                etat = Etat.JOUER;
                players[0] = new Player(Couleur.ROUGE, TypePlayer.IA);
                players[1] = new Player(Couleur.NOIR, TypePlayer.IA);
                joueur = players[0];
                for (int i = 0; i < players.length; i++) {
                    System.out.println(players[i].couleur + " " + players[i].type);
                }
                drawPanel.remove(button_0p);
                drawPanel.remove(button_1p);
                drawPanel.remove(button_2p);
                drawPanel.repaint();
            }
        });
    
        button_1p.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(etat == Etat.MENU){
                    etat = Etat.JOUER;
                    players[0] = new Player(Couleur.ROUGE, TypePlayer.HUMAIN);
                    players[1] = new Player(Couleur.NOIR, TypePlayer.IA);
                }
                joueur = players[0];
                for (int i = 0; i < players.length; i++) {
                    System.out.println(players[i].couleur + " " + players[i].type);
                }
                drawPanel.remove(button_0p);
                drawPanel.remove(button_1p);
                drawPanel.remove(button_2p);
                drawPanel.repaint();
            }
        });

        button_2p.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(etat == Etat.MENU){
                    etat = Etat.JOUER;
                    players[0] = new Player(Couleur.ROUGE, TypePlayer.HUMAIN);
                    players[1] = new Player(Couleur.NOIR, TypePlayer.HUMAIN);
                }
                joueur = players[0];
                for (int i = 0; i < players.length; i++) {
                    System.out.println(players[i].couleur + " " + players[i].type);
                }
                drawPanel.remove(button_0p);
                drawPanel.remove(button_1p);
                drawPanel.remove(button_2p);
                drawPanel.repaint();
            }
        });
        drawPanel.add(Box.createHorizontalStrut((drawPanel.getWidth() - button_2p.getWidth()) / 2));
        drawPanel.add(button_2p);
        drawPanel.add(button_0p);
        drawPanel.add(button_1p);
        drawPanel.add(Box.createHorizontalStrut((drawPanel.getWidth() - button_2p.getWidth()) / 2));
        drawPanel.add(Box.createVerticalGlue());
    }


    // Dessine le plateau de jeu
    private void drawBoard(Graphics g) {
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if ((row + col) % 2 == 0) {
                    g.setColor(Color.WHITE);
                } else {
                    g.setColor(Color.GRAY);
                }
                g.fillRect(col * SIZE, row * SIZE, SIZE, SIZE);
                Pion pion = board.getPion(row, col);
                if (pion != null) {
                    g.setColor(pion.getCouleur() == Couleur.ROUGE ? Color.RED : Color.BLACK);
                    g.fillOval(col * SIZE + SIZE / 4, row * SIZE + SIZE / 4, SIZE / 2, SIZE / 2);
                    if(pion.isDame()) {
                        g.setColor(Color.YELLOW);
                        g.drawString("Q",col * SIZE + SIZE / 2, row * SIZE + SIZE / 2);
                    }
                    if(pion.isRoi()) {
                        g.setColor(Color.YELLOW);
                        g.drawString("K",col * SIZE + SIZE / 2, row * SIZE + SIZE / 2);
                    }
                    if(pion.isSelected()){
                        g.setColor(Color.GREEN);
                        g.drawRect(col * SIZE , row * SIZE  , SIZE-1 , SIZE-1);
                    }
                }
            }
        }

        // si le joueur est une IA, on ne fait rien
        if (joueur.type == TypePlayer.IA) {
            tourIA(joueur);
        }else{
            drawPanel.addMouseListener(new MouseAdapter() {
                private int fromX, fromY;
                @Override
                public void mousePressed(MouseEvent e) {
                    int currentX = e.getX() / SIZE; 
                    int currentY = e.getY() / SIZE;
                    Pion pion = board.getPion(currentY, currentX);
                    if(!board.haveSelectedPion()){ // Si aucun pion n'est sélectionné
                        if(pion != null && pion.getCouleur() == joueur.couleur){ // Si on clique sur un pion de la bonne couleur
                            pion.setSelected();
                            fromX = currentX;
                            fromY = currentY;
                        } 
                    }
                    else{
                        if(pion != null && pion.getCouleur() == joueur.couleur){ // Si un pion est sélectionné et qu'on clique sur un autre pion de la même couleur
                            board.getSelectedPion().setUnselected();
                            pion.setSelected();
                            fromX = currentX;
                            fromY = currentY;
                        }
                        else if(pion == null){ // Si un pion est sélectionné et qu'on clique sur une case vide
                            if(board.movePiece(fromY, fromX, currentY, currentX)){
                                if(!board.haveSelectedPion()){
                                    joueur = joueur == players[0] ? players[1] : players[0];
                                }
                                fromY = currentY;
                                fromX = currentX;
                            }
                        }

                    }
                    drawPanel.repaint();
                    
                }
            });
        }
    }

    // Dessine le menu de fin de partie
    private void drawEnd(Graphics g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.BOLD, 30));
        g.drawString("Fin de la partie", 100, 100);
        g.setFont(new Font("Arial", Font.BOLD, 20));
        g.drawString("Cliquez pour recommencer", 100, 150);
        drawPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                etat = Etat.MENU;
                drawPanel.removeMouseListener(this);
                drawPanel.repaint();
            }
        });
    }

    public void defaite(Player p){
        System.out.println("L'IA " + p.couleur + " a perdu");
        drawPanel.repaint();
        etat = Etat.FIN;
        //on attend un peu pour que voir le plateau de fin de partie
        try {
            TimeUnit.MILLISECONDS.sleep(2500);
        } catch (InterruptedException e) {
            
            e.printStackTrace();
        }

        joueur = players[0];
        board = new Board();

        restart(getGraphics());
        drawPanel.repaint();

    }

    public void tourIA(Player p){
        System.out.println("Tour de l'IA " + p.couleur);
        //recuperation de la liste des pions de l'ia
        Pion[] pions = board.getPionsIA(p);
        System.out.println(pions.length + "   " + p.couleur);
        if(pions.length == 0){
            defaite(p);
            return;
        }
        //recherche du meilleur mouvement possible
        int nextX;
        int nextY;
        do{
            Mouvement m = bestMouvement(pions);
            if(m == null){
                System.out.println("L'IA " + p.couleur + " a perdu");
                etat = Etat.FIN;
                return;
            }
            
            //on attend un peu pour que l'ia ne soit pas trop rapide
            try {
                TimeUnit.MILLISECONDS.sleep(400);
            } catch (InterruptedException e) {
                
                e.printStackTrace();
            }

            //deplacement du pion
            board.movePiece(m.fromPosX, m.fromPosY, m.toPosX, m.toPosY); 

            //si le pion peut encore capturer, on continue
            nextX = m.toPosX;
            nextY = m.toPosY;
        }while(board.PreviousIsCaptureMove && board.haveCaptureMovee(nextX, nextY, p.couleur));
        //changer de joueur à la fin du tour
        joueur = joueur == players[0] ? players[1] : players[0];

        drawPanel.repaint();
    }

    //retourne le meilleur mouvement possible pour l'ia
    public Mouvement bestMouvement(Pion [] p){
        Mouvement[] mouvements = null;
        
        try {
            mouvements = allMouvementsAsync(p);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } 
        if(mouvements.length == 0){
            System.out.println("Erreur : aucun mouvement possible");
            return null;
        }

        //parcours de la liste des mouvements possibles et rercherche du meilleur mouvement
        int bestvalue = mouvements[0].value;
        int bestindex = 0;
        for(int i = 0; i < mouvements.length; i++) {    // On parcourt tous les mouvements
            if(mouvements[i].value > bestvalue) {
                bestvalue = mouvements[i].value;
                bestindex = i;
            }
            if(mouvements[i].value == bestvalue){       //une chance sur deux de choisir le mouvement
                if(Math.random() < 0.5){
                    bestvalue = mouvements[i].value;
                    bestindex = i;
                }
            }
        }

        if(bestindex == -1){
            System.out.println("Erreur : aucun mouvement possible");
            return null;
        }

        return mouvements[bestindex];
    }

    //retourne la liste des mouvements possibles pour l'ia
    public Mouvement[] allMouvementsAsync(Pion[] p) throws InterruptedException {
        ArrayList<Mouvement> mouvements = new ArrayList<Mouvement>();
        AtomicInteger counter = new AtomicInteger(p.length);
        
        for (int i = 0; i < p.length; i++) { // On crée un thread pour chaque pion
            int y = i;
            new Thread(new Runnable() { //
                public void run() {
                    Mouvement[] m = board.getMouvements(p[y]);
                    
                    synchronized(mouvements) {
                        for (int j = 0; j < m.length; j++) {
                            mouvements.add(m[j]);
                        }
                    }
                    
                    if (counter.decrementAndGet() == 0) {
                        synchronized(counter) {
                            counter.notify();
                        }
                    }
                }
            }).start();
        }
        
        synchronized(counter) {
            while (counter.get() > 0) {
                counter.wait();
            }
        }
    
        Mouvement[] m = mouvements.toArray(new Mouvement[mouvements.size()]);
        
        return m;
    }

    //retourne la liste de tous les mouvements possibles pour l'ia
    public Mouvement[] allMouvements(Pion [] p){
        ArrayList<Mouvement> mouvements = new ArrayList<Mouvement>();
        //parcours de la liste des pions de l'ia

        for(int i = 0; i < p.length; i++) {
            //recuperation des mouvements possibles pour chaque pion
            Mouvement[] m = board.getMouvements(p[i]);
            //parcours de la liste des mouvements possibles
            for(int j = 0; j < m.length; j++) {
                //on ajoute le mouvement à la liste
                mouvements.add(m[j]);
            }
        }

        Mouvement m[] = mouvements.toArray(new Mouvement[mouvements.size()]);
        return m;
    }
    
    public static void main(String[] args) {
        Affichage affichage = new Affichage();
    }
}
