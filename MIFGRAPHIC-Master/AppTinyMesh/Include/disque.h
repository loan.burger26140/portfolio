#pragma once

#include <vector>
#include <iostream>

#include "mathematics.h"

class Disque
{
public:
    Disque(const float& r = 1, const Vector& c = Vector(0,0,0));
    Vector getCenter() const{ return center;}
    float getRayon() const{ return rayon;}
    Vector Vertex(int k) const;
    static const Vector normal;
private :
    Vector center;
    float rayon;
    std::vector<Vector> listVertex;
};
