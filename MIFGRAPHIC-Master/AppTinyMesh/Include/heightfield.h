#ifndef HEIGHTFIELD_H
#define HEIGHTFIELD_H

#include <QVector2D>
#include <QVector3D>
#include <mathematics.h>
#include <QImage>

class HeightField
{
public:
    QVector2D a,b;
    int nx, ny;
    std::vector<float> z;

public:
    HeightField(QVector2D a1, QVector2D b1, QImage image, float e);

    //retour l'index du point i, j dans le tablau

    int index (int i, int j) const;

    //retour le vecteur(=vertex) au point i,j
    Vector P(int i, int j) const;

    //retourne la proportion entre nx et la distance A.x B.x;
    float ScaleX() const;

    //retourne la proportion entre ny et la distance A.y B.y;
    float ScaleY() const;

    //calul gradiant en point i,j et le retourne
    QVector2D gradiant(int i, int j) const;

    //calcul la normal au point i, j
    Vector normal(int i, int j) const;

    //retourne la norme d'un vecteur
    float norme(QVector3D v) const;

};
#endif // HEIGHTFIELD_H
