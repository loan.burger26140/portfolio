#pragma once

#include <vector>
#include <iostream>

#include "mathematics.h"
#include "disque.h"

class Sphere
{
public:
    Sphere(const float& r = 1, const Vector& c = Vector(0,0,0));
    Vector Vertex(int k) const;
    Vector Normal(int k) const;

    float getRayon() const { return rayon; }
    Vector getCenter() const {return center; }

private :
    Vector center;
    float rayon;
    std::vector<Vector> listVertex;
    std::vector<Vector> listNormal;

};
