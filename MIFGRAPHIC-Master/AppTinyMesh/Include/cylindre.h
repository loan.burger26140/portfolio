#pragma once

#include <vector>
#include <iostream>

#include "mathematics.h"
#include "disque.h"

class Cylindre
{
public:
    Cylindre(const float& r = 1, const float& h = 1,const Vector& c = Vector(0,0,0));
    Vector Vertex(int k) const;
    Vector Normal(int k) const;

    float getRayon() const { return rayon; }
    float getHauteur() const { return hauteur; }
    Vector getCenter() const {return center; }
private :
    Vector center;
    float rayon;
    float hauteur;
    std::vector<Vector> listVertex;
    std::vector<Vector> listNormal;
    //Disque disque[2];  //[0] bottom circle && [1] top circle
};
