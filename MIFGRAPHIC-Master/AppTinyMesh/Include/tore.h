#pragma once

#include <vector>
#include <iostream>

#include "mathematics.h"

class Tore
{
public:
    Tore(const float& d = 1, const float& r = 1, const Vector& c = Vector(0,0,0), const int& nbCercle = 20, const int& nbPtCercle=20);
    Vector Vertex(int k) const;
    Vector Normal(int k) const;

    float getRayon() const { return rayon; }
    Vector getCenter() const {return center; }
    float getDistance() const { return distance; }
    int getNbPtCercle() const { return divPtCercle; }
    int getNbCercle() const {return divCercle; }


private :
    int divPtCercle;
    int divCercle;
    Vector center;
    float rayon;
    float distance;
    std::vector<Vector> listVertex;
    std::vector<Vector> listNormal;

};
