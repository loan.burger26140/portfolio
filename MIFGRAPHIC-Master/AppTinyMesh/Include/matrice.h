#include "mathematics.h"

class Matrice{

public:
    Matrice();
    Matrice(double a, double b, double c, double d, double e, double f, double g, double h, double i);

    void transpose();
    Matrice inverse();

//    const Matrice & operator=(const Matrice & tmp);
    const Matrice & operator+(const Matrice & tmp);
    const Matrice & operator*(const Matrice & tmp);
    const Matrice & operator*=(const Matrice & tmp);
    const Vector & operator*(const Vector & tmp);
    const Matrice & operator+=(const Matrice & tmp);
    const Matrice & operator-(const Matrice & tmp);
//    const Matrice & operator/(double x) const;
//    const Matrice & operator=(const Matrice& tmp);

    static Matrice RotationX(double alpha);
    static Matrice RotationY(double alpha);
    static Matrice RotationZ(double alpha);



private:
    double value[9];
};
