#include "sphere.h"

Sphere::Sphere(const float& r, const Vector& c) : center(c), rayon(r){
    int divBeta = 100;
    int divAlpha = divBeta/2 ;
    //int nbV = divAlpha * divBeta;
    double alpha1, alpha2, beta;
    for(int i=0; i<divAlpha; i++){
        alpha1 = -0.5f * M_PI + float(i) * M_PI / divAlpha;
        alpha2 = -0.5f * M_PI + float(i+1) * M_PI / divAlpha;
        for(int j=0; j<divBeta; j+=2){
            beta = float(j) * 2.f * M_PI / (divBeta);
            listVertex.push_back(c + r * Vector(cos(alpha1)*cos(beta), sin(alpha1), cos(alpha1)*sin(beta)));
            listVertex.push_back(c + r *Vector(cos(alpha2)*cos(beta), sin(alpha2), cos(alpha2)*sin(beta)));

            listNormal.push_back(Vector(cos(alpha1)*cos(beta), sin(alpha1), cos(alpha1)*sin(beta)));
            listNormal.push_back(Vector(cos(alpha2)*cos(beta), sin(alpha2), cos(alpha2)*sin(beta)));
        }
    }
}

Vector Sphere::Vertex(int k) const {
    return listVertex[k];
}

Vector Sphere::Normal(int k) const {
    return listNormal[k];
}
