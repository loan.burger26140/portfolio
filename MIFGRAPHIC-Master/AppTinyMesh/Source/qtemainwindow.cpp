#include "qte.h"
#include "implicits.h"
#include "ui_interface.h"

MainWindow::MainWindow() : QMainWindow(), uiw(new Ui::Assets)
{
	// Chargement de l'interface
    uiw->setupUi(this);

	// Chargement du GLWidget
	meshWidget = new MeshWidget;
	QGridLayout* GLlayout = new QGridLayout;
	GLlayout->addWidget(meshWidget, 0, 0);
	GLlayout->setContentsMargins(0, 0, 0, 0);
    uiw->widget_GL->setLayout(GLlayout);

	// Creation des connect
	CreateActions();

	meshWidget->SetCamera(Camera(Vector(10, 0, 0), Vector(0.0, 0.0, 0.0)));
}

MainWindow::~MainWindow()
{
	delete meshWidget;
}

void MainWindow::CreateActions()
{
	// Buttons
    connect(uiw->boxMesh, SIGNAL(clicked()), this, SLOT(BoxMeshExample()));
    connect(uiw->sphereImplicit, SIGNAL(clicked()), this, SLOT(SphereImplicitExample()));
    connect(uiw->disqueButton, SIGNAL(clicked()), this, SLOT(DisqueMeshExample()));
    connect(uiw->CylindreButton, SIGNAL(clicked()), this, SLOT(CylindreMeshExample()));
    connect(uiw->SphereButton, SIGNAL(clicked()), this, SLOT(SphereMeshExample()));
    connect(uiw->RotationButton, SIGNAL(clicked()), this, SLOT(RotationButton()));
    connect(uiw->ToreButton, SIGNAL(clicked()), this, SLOT(ToreMeshExample()));
    connect(uiw->CapsuleButton, SIGNAL(clicked()), this, SLOT(CapsuleMeshExample()));
    connect(uiw->TerrainButton, SIGNAL(clicked()), this, SLOT(TerrainMeshExample()));
    connect(uiw->DeformationButton, SIGNAL(clicked()), this, SLOT(DeformationMeshExemple()));
    connect(uiw->ComplexeButton, SIGNAL(clicked()), this, SLOT(ComplexeMeshExample()));
    connect(uiw->resetcameraButton, SIGNAL(clicked()), this, SLOT(ResetCamera()));
    connect(uiw->wireframe, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_1, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_2, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));

	// Widget edition
	connect(meshWidget, SIGNAL(_signalEditSceneLeft(const Ray&)), this, SLOT(editingSceneLeft(const Ray&)));
	connect(meshWidget, SIGNAL(_signalEditSceneRight(const Ray&)), this, SLOT(editingSceneRight(const Ray&)));
}

void MainWindow::editingSceneLeft(const Ray&)
{
}

void MainWindow::editingSceneRight(const Ray&)
{
}

void MainWindow::BoxMeshExample()
{
    Mesh boxMesh = Mesh(Box(1.0));

	std::vector<Color> cols;
	cols.resize(boxMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(boxMesh, cols, boxMesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::DisqueMeshExample()
{
    Mesh disqueMesh = Mesh(Disque());

    std::vector<Color> cols;
    cols.resize(disqueMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
        cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

    meshColor = MeshColor(disqueMesh, cols, disqueMesh.VertexIndexes());
    UpdateGeometry();

}

void MainWindow::CylindreMeshExample()
{

    Mesh CylindreMesh = Mesh(Cylindre());
    std::vector<Color> cols;
    cols.resize(CylindreMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
        cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

    meshColor = MeshColor(CylindreMesh, cols, CylindreMesh.VertexIndexes());
    UpdateGeometry();
}

void MainWindow::SphereMeshExample()
{

    Mesh SphereMesh = Mesh(Sphere());
    std::vector<Color> cols;
    cols.resize(SphereMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
        cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

    meshColor = MeshColor(SphereMesh, cols, SphereMesh.VertexIndexes());
    UpdateGeometry();
}

void MainWindow::RotationButton()
{
    Mesh SphereMesh = Mesh(Box(1.0));
    SphereMesh.Rotate(Matrice::RotationZ(90));
    std::vector<Color> cols;
    cols.resize(SphereMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
        cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

    meshColor = MeshColor(SphereMesh, cols, SphereMesh.VertexIndexes());
    UpdateGeometry();
}

void MainWindow::DeformationMeshExemple(){
    Mesh SphereMesh = Mesh(Sphere(5));
    SphereMesh.SphereWarp(Sphere(4,Vector(5,0,0)),Vector(10,1,0));
    std::vector<Color> cols;
    cols.resize(SphereMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
        cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

    meshColor = MeshColor(SphereMesh, cols, SphereMesh.VertexIndexes());
    UpdateGeometry();

}

void MainWindow::ToreMeshExample()
{

    //Mesh ToreMesh = Mesh(Tore(3,1,Vector(0,0,0)));
    Mesh ToreMesh = Mesh(1,3,Vector(0,0,0),20,20);
    std::vector<Color> cols;
    cols.resize(ToreMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
        cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

    meshColor = MeshColor(ToreMesh, cols, ToreMesh.VertexIndexes());
    UpdateGeometry();
}

void MainWindow::CapsuleMeshExample()
{
    Mesh CapsuleMesh = Mesh(Cylindre(1,5),1);
    std::vector<Color> cols;
    cols.resize(CapsuleMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
        cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);
    meshColor = MeshColor(CapsuleMesh, cols, CapsuleMesh.VertexIndexes());
    UpdateGeometry();
}

void MainWindow::TerrainMeshExample()
{
    QVector2D a1(-100,-100);
    QVector2D b1(100,100);
    float e =0.5;

    QString fileName = "../mifgraphique/imageTerrain/imageMontagne.JPG";
    QImage image;
    Mesh TerrainMesh;

    if(image.load(fileName)){
        TerrainMesh = Mesh(HeightField(a1,b1,image , e));
    }
    std::vector<Color> cols;
    cols.resize(TerrainMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
        if(TerrainMesh.Vertex(i)[3] < 80 * e){
            cols[i] = Color(0.03,0.34,0.76);
        }else if(TerrainMesh.Vertex(i)[3] > 180 * e){
            cols[i] = Color(0.87,0.8,0.6);
        }else{
            cols[i] = Color(0.95,0.95,0.95);
        }


    meshColor = MeshColor(TerrainMesh, cols, TerrainMesh.VertexIndexes());
    UpdateGeometry();
}


void MainWindow::SphereImplicitExample()
{
  AnalyticScalarField implicit;

  Mesh implicitMesh;
  implicit.Polygonize(31, implicitMesh, Box(2.0));

  std::vector<Color> cols;
  cols.resize(implicitMesh.Vertexes());
  for (size_t i = 0; i < cols.size(); i++)
    cols[i] = Color(0.8, 0.8, 0.8);

  meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::ComplexeMeshExample()
{
  //Mesh complexe = Mesh::ovni();

//  Mesh planete = Mesh(Sphere(2));
//  planete.merge(Mesh(0.75,4,Vector(0,0,0),50,3));
//  planete.Rotate(Matrice::RotationX(30));

  Mesh planete = Mesh::planete();

  std::vector<Color> cols;
  cols.resize(planete.Vertexes());
  for (size_t i = 0; i < cols.size(); i++)
    cols[i] = Color(0.2, 0.8, 0.8);

  meshColor = MeshColor(planete, cols, planete.VertexIndexes());
  UpdateGeometry();
}

void MainWindow::UpdateGeometry()
{
	meshWidget->ClearAll();
	meshWidget->AddMesh("BoxMesh", meshColor);

    uiw->lineEdit->setText(QString::number(meshColor.Vertexes()));
    uiw->lineEdit_2->setText(QString::number(meshColor.Triangles()));

	UpdateMaterial();
}

void MainWindow::UpdateMaterial()
{
    meshWidget->UseWireframeGlobal(uiw->wireframe->isChecked());

    if (uiw->radioShadingButton_1->isChecked())
		meshWidget->SetMaterialGlobal(MeshMaterial::Normal);
	else
		meshWidget->SetMaterialGlobal(MeshMaterial::Color);
}

void MainWindow::ResetCamera()
{
	meshWidget->SetCamera(Camera(Vector(-10.0), Vector(0.0)));
}
