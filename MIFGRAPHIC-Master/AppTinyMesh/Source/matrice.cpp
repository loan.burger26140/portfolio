#include "matrice.h"


Matrice::Matrice() {
    for(int i =0; i<9; i++){
        if(i==0 || i==4 || i==8){
            value[i]=1;
        }else{
            value[i]=0;
        }
    }
}

Matrice::Matrice(double a, double b, double c, double d, double e, double f, double g, double h, double i){
    value[0] = a;
    value[1] = b;
    value[2] = c;
    value[3] = d;
    value[4] = e;
    value[5] = f;
    value[6] = g;
    value[7] = h;
    value[8] = i;
}

Matrice Matrice::RotationX(double alpha){
    double alphaRad = alpha * (M_PI/180);
    return Matrice(1,0,0,0,cos(alphaRad),-sin(alphaRad),0,sin(alphaRad),cos(alphaRad));
}

Matrice Matrice::RotationY(double alpha){
    double alphaRad = alpha * (M_PI/180);
    return Matrice(cos(alphaRad),0,sin(alphaRad),0,1,0,-sin(alphaRad),0,cos(alphaRad));
}

Matrice Matrice::RotationZ(double alpha){
    double alphaRad = alpha * (M_PI/180);
    return Matrice(cos(alphaRad), -sin(alphaRad), 0, sin(alphaRad), cos(alphaRad),0, 0, 0, 1);
}

const Matrice & Matrice::operator+=(const Matrice & tmp){
    for(int i =0; i<9; i++){
        value[i]+=tmp.value[i];
    }
    return *this;
}

const Matrice & Matrice::operator+(const Matrice & tmp){
    Matrice * nouvelleMat = new Matrice();
    for(int i =0; i<9; i++){
        nouvelleMat->value[i] = value[i] + tmp.value[i];
    }
    return *nouvelleMat;
}

const Matrice & Matrice::operator-(const Matrice & tmp){
    Matrice * nouvelleMat = new Matrice();
    for(int i =0; i<9; i++){
        nouvelleMat->value[i] = value[i] - tmp.value[i];
    }
    return *nouvelleMat;
}

const Matrice & Matrice::operator*(const Matrice & tmp){
    Matrice * nouvelleMat = new Matrice();
        for(int i =0; i<3; i++){
            for(int j=0; j<3; j++){
                nouvelleMat->value[j*3 + i] = 0;
                for(int k=0; k<3; k++){
                    nouvelleMat->value[j*3+1] += value[k*3+i] * tmp.value[j*3 + k];
                }
            }
    }  
    return *nouvelleMat;
}


const Vector & Matrice::operator*(const Vector & tmp){
    double a = value[0] * tmp[0] + value[1] * tmp[1] + value[2] * tmp[2];
    double b = value[3] * tmp[0] + value[4] * tmp[1] + value[5] * tmp[2];
    double c = value[6] * tmp[0] + value[7] * tmp[1] + value[8] * tmp[2];
    Vector *nouv = new Vector(a,b,c);
    return *nouv;
}

//const Matrice & Matrice::operator/(double x) const {
//    Matrice * nouvelleMat = new Matrice();

//    for (int i = 0; i < 9; i++) {
//        nouvelleMat->value[i] = value[i] / x;
//    }

//    return *nouvelleMat;
//}

//const Matrice & Matrice::operator=(const Matrice& tmp) {
//    for (int i = 0; i < 9; i++) {
//        value[i] = tmp.value[i];
//    }
//    return *this;
//}

void Matrice::transpose(){
    Matrice * nouvelleMat = new Matrice();

    for(int i =0; i<3; i++){
        for(int j=0; j<3; j++){
            nouvelleMat->value[j*3 +i]=value[i*3 + j];
        }
    }
    *this = *nouvelleMat;
}


//Matrice Matrice::inverse() {
//    double m11 = value[4] * value[8] - value[5] * value[7];
//    double m12 = value[3] * value[8] - value[5] * value[6];
//    double m13 = value[3] * value[7] - value[4] * value[6];
//    double m21 = value[1] * value[8] - value[2] * value[7];
//    double m22 = value[0] * value[8] - value[2] * value[6];
//    double m23 = value[0] * value[7] - value[1] * value[6];
//    double m31 = value[1] * value[5] - value[2] * value[4];
//    double m32 = value[0] * value[5] - value[2] * value[3];
//    double m33 = value[0] * value[4] - value[1] * value[3];

//    Matrice com(m11, -m12, m13, -m21, m22, -m23, m31, -m32, m33);

//    com = com.transpose();

//    //Calcul du déterminant via le développement de cofacteur
//    double det = value[0] * m11 - value[1] * m12 + value[2] * m13;

//    if (det != 0) {
//        return com / det;
//    }
//    else {
//        return *this;
//    }
//}


