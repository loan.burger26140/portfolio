#include "mesh.h"
#define M_PI 3.14159265358979323846
/*!
\class Mesh mesh.h

\brief Core triangle mesh class.
*/



/*!
\brief Initialize the mesh to empty.
*/
Mesh::Mesh()
{
}

/*!
\brief Initialize the mesh from a list of vertices and a list of triangles.

Indices must have a size multiple of three (three for triangle vertices and three for triangle normals).

\param vertices List of geometry vertices.
\param indices List of indices wich represent the geometry triangles.
*/
Mesh::Mesh(const std::vector<Vector>& vertices, const std::vector<int>& indices) :vertices(vertices), varray(indices)
{
  normals.resize(vertices.size(), Vector::Z);
}

/*!
\brief Create the mesh.

\param vertices Array of vertices.
\param normals Array of normals.
\param va, na Array of vertex and normal indexes.
*/
Mesh::Mesh(const std::vector<Vector>& vertices, const std::vector<Vector>& normals, const std::vector<int>& va, const std::vector<int>& na) :vertices(vertices), normals(normals), varray(va), narray(na)
{
}

/*!
\brief Reserve memory for arrays.
\param nv,nn,nvi,nvn Number of vertices, normals, vertex indexes and vertex normals.
*/
void Mesh::Reserve(int nv, int nn, int nvi, int nvn)
{
  vertices.reserve(nv);
  normals.reserve(nn);
  varray.reserve(nvi);
  narray.reserve(nvn);
}

/*!
\brief Empty
*/
Mesh::~Mesh()
{
}

/*!
\brief Smooth the normals of the mesh.

This function weights the normals of the faces by their corresponding area.
\sa Triangle::AreaNormal()
*/
void Mesh::SmoothNormals()
{
  // Initialize 
  normals.resize(vertices.size(), Vector::Null);

  narray = varray;

  // Accumulate normals
  for (int i = 0; i < (int)varray.size(); i += 3)
  {
    Vector tn = Triangle(vertices[varray.at(i)], vertices[varray.at(i + 1)], vertices[varray.at(i + 2)]).AreaNormal();
    normals[narray[i + 0]] += tn;
    normals[narray[i + 1]] += tn;
    normals[narray[i + 2]] += tn;
  }

  // Normalize 
  for (int i = 0; i < (int)normals.size(); i++)
  {
    Normalize(normals[i]);
  }
}

/*!
\brief Add a smooth triangle to the geometry.
\param a, b, c Index of the vertices.
\param na, nb, nc Index of the normals.
*/
void Mesh::AddSmoothTriangle(int a, int na, int b, int nb, int c, int nc)
{
  varray.push_back(a);
  narray.push_back(na);
  varray.push_back(b);
  narray.push_back(nb);
  varray.push_back(c);
  narray.push_back(nc);
}

/*!
\brief Add a triangle to the geometry.
\param a, b, c Index of the vertices.
\param n Index of the normal.
*/
void Mesh::AddTriangle(int a, int b, int c, int n)
{
  varray.push_back(a);
  narray.push_back(n);
  varray.push_back(b);
  narray.push_back(n);
  varray.push_back(c);
  narray.push_back(n);
}

/*!
\brief Add a smmoth quadrangle to the geometry.

Creates two smooth triangles abc and acd.

\param a, b, c, d  Index of the vertices.
\param na, nb, nc, nd Index of the normal for all vertices.
*/
void Mesh::AddSmoothQuadrangle(int a, int na, int b, int nb, int c, int nc, int d, int nd)
{
  // First triangle
  AddSmoothTriangle(a, na, b, nb, c, nc);

  // Second triangle
  AddSmoothTriangle(a, na, c, nc, d, nd);
}

/*!
\brief Add a quadrangle to the geometry.

\param a, b, c, d  Index of the vertices and normals.
*/
void Mesh::AddQuadrangle(int a, int b, int c, int d)
{
  AddSmoothQuadrangle(a, a, b, b, c, c, d, d);
}

/*!
\brief Compute the bounding box of the object.
*/
Box Mesh::GetBox() const
{
  if (vertices.size() == 0)
  {
    return Box::Null;
  }
  return Box(vertices);
}

/*!
\brief Creates an axis aligned box.

The object has 8 vertices, 6 normals and 12 triangles.
\param box The box.
*/
Mesh::Mesh(const Box& box)
{
  // Vertices
  vertices.resize(8);

  for (int i = 0; i < 8; i++)
  {
    vertices[i] = box.Vertex(i);
  }

  // Normals
  normals.push_back(Vector(-1, 0, 0));
  normals.push_back(Vector(1, 0, 0));
  normals.push_back(Vector(0, -1, 0));
  normals.push_back(Vector(0, 1, 0));
  normals.push_back(Vector(0, 0, -1));
  normals.push_back(Vector(0, 0, 1));

  // Reserve space for the triangle array
  varray.reserve(12 * 3);
  narray.reserve(12 * 3);

  AddTriangle(0, 2, 1, 4);
  AddTriangle(1, 2, 3, 4);

  AddTriangle(4, 5, 6, 5);
  AddTriangle(5, 7, 6, 5);

  AddTriangle(0, 4, 2, 0);
  AddTriangle(4, 6, 2, 0);

  AddTriangle(1, 3, 5, 1);
  AddTriangle(3, 7, 5, 1);

  AddTriangle(0, 1, 5, 2);
  AddTriangle(0, 5, 4, 2);

  AddTriangle(3, 2, 7, 3);
  AddTriangle(6, 7, 2, 3);
}

/*!
\brief Creates an axis aligned circle.

The object has 21 vertices (center + 20 point arround the circle), 1 normals and 9 triangles.
\param box The box.
*/
Mesh::Mesh(const Disque& disque){
  // Vertices
  vertices.resize(101);
  for (int i = 0; i <(int) vertices.size(); i++){
    vertices[i] = disque.Vertex(i);
  }

  // Normals
  normals.push_back(disque.normal);

  // Reserve space for the triangle array
  varray.reserve(vertices.size()-1 * 3);
  narray.reserve(1 * 3);

  for (int i = 0; i < (int)vertices.size()-1 ; i++)
  {
    AddSmoothTriangle(vertices.size()-1, 0, i, 0, i+1, 0);
  }
}

/*!
\brief Creates an axis aligned cylindre.

The object has 40 vertices, 40 normals and 40 triangles.
\param box The box.
*/
Mesh::Mesh(const Cylindre& cylindre){
    vertices.resize(100);
    for (int i = 0; i < (int)vertices.size(); i++){
           vertices[i] = cylindre.Vertex(i);
           normals.push_back(cylindre.Normal(i));
    }

    // Reserve space for the triangle array
    varray.reserve(vertices.size() * 3);
    narray.reserve(vertices.size() * 3);

    for (int i = 0; i <=(int)vertices.size()-1 ; i++){
      AddSmoothTriangle(i,i,(i+1)%vertices.size(),(i+1)%vertices.size(),(i+2)%vertices.size(),(i+2)%vertices.size());
    }

    float rayon = cylindre.getRayon();
    float hauteur = cylindre.getHauteur();
    Vector centre = cylindre.getCenter();

    Mesh top(Disque(rayon, Vector(centre[1], centre[2] + hauteur, centre[3])));
    merge(top);
    Mesh bottom(Disque(rayon, centre));
    merge(bottom);
  }

Mesh::Mesh(const Cylindre& cylindre, int a){
    vertices.resize(100);
    for (int i = 0; i < (int)vertices.size(); i++){
           vertices[i] = cylindre.Vertex(i);
           normals.push_back(cylindre.Normal(i));
    }

    // Reserve space for the triangle array
    varray.reserve(vertices.size() * 3);
    narray.reserve(vertices.size() * 3);

    for (int i = 0; i <=(int)vertices.size()-1 ; i++){
      AddSmoothTriangle(i,i,(i+1)%vertices.size(),(i+1)%vertices.size(),(i+2)%vertices.size(),(i+2)%vertices.size());
    }

    float rayon = cylindre.getRayon();
    float hauteur = cylindre.getHauteur();
    Vector hauteurV = Vector(0,hauteur,0);
    Vector centre = cylindre.getCenter();

    Mesh top(Sphere(rayon, centre + hauteurV));
    merge(top);
    Mesh bottom(Sphere(rayon, centre));
    merge(bottom);
    SmoothNormals();
  }

Mesh::Mesh(const Sphere& sphere){
    int divBeta = 100;
    int divAlpha = divBeta/2 ;
    int nbV = divAlpha * divBeta;
    vertices.resize(nbV);

    for(int i=0; i<nbV-1; i++){
        vertices[i] = sphere.Vertex(i);
        normals.push_back(sphere.Normal(i));
    }

    varray.reserve(vertices.size() * 3);
    narray.reserve(vertices.size() * 3);

    for (int i = 0; i <= (int)vertices.size() - 4 ; i++){
        AddSmoothTriangle(i,i,(i+1)%vertices.size(),(i+1)%vertices.size(),(i+2)%vertices.size(),(i+2)%vertices.size());
    }
}

Mesh::Mesh(const float rayon, const float dist, const Vector& c, int nbCercle, int nbPtCercle){
    int divCercle = nbCercle;
    int divPtCercle = nbPtCercle;
    double step = 360/ (divCercle-1);
    Matrice Rotate;
    Vector res;
    for (int j = 0; j < divCercle; j++){
        double alphaCercle;
        double stepCercle = (2* M_PI)/ (divPtCercle-1);
        double alpha = step * j;
        Rotate = Matrice::RotationZ(alpha);

        for (int i = 0; i < divPtCercle; i++){
          alphaCercle = i * stepCercle;
          res = Rotate * Vector(dist + rayon * cos(alphaCercle), 0 , rayon * sin(alphaCercle)) + c;
          vertices.push_back(res);
          normals.push_back(res - c);
        }
    }

    varray.reserve(vertices.size() * 3);
    narray.reserve(vertices.size() * 3);

    for (int i = 0; i <= (int)vertices.size()-2 ; i++){
        AddSmoothTriangle(i, i, i+1, i+1, (i+divPtCercle)%vertices.size(), (i+divPtCercle)%vertices.size());
        AddSmoothTriangle((i+divPtCercle)%vertices.size(),(i+divPtCercle)%vertices.size(), (i+1+divPtCercle)%vertices.size(),(i+1+divPtCercle)%vertices.size(), i+1, i+1);
    }
}

Mesh::Mesh(const Tore& tore){
    int nbPtCercle = tore.getNbPtCercle();
    int nbCercle = tore.getNbCercle();
    vertices.resize(nbPtCercle * nbCercle);

    for(int i=0; i<(int)vertices.size()-1; i++){
        vertices[i] = tore.Vertex(i);
        normals.push_back(tore.Normal(i));
    }

    varray.reserve(vertices.size() * 3);
    narray.reserve(vertices.size() * 3);

    for (int i = 0; i <= (int)vertices.size()-3 ; i++){
        AddTriangle(i, i+1, (i+nbPtCercle)%vertices.size(),0);
        AddTriangle((i+nbPtCercle)%vertices.size(), (i+1+nbPtCercle)%vertices.size(), i+1, 0);
    }
}

Mesh::Mesh(const HeightField &terrain){

    for(int j=0; j<terrain.ny; j++){
        for(int i=0; i<terrain.nx; i++){
            vertices.push_back(terrain.P(i,j));
        }
    }

    //normals.push_back(Vector(0, 0, 1));
    varray.reserve(vertices.size() * 3);
    narray.reserve(vertices.size() * 3);

    int ligne = terrain.nx;
    for(int j=0; j<terrain.ny-1; j++){
        for(int i=0; i<terrain.nx-1; i++){
            if(terrain.z[terrain.index(i,j)]==0){
               normals.push_back(Vector(1, 0, 0));
            }else{
                normals.push_back(Vector(i, j, 1));
            }
            AddTriangle(i+(ligne*j),i+ligne+(ligne*j),i+ligne+1+(ligne*j),normals.size()-1);
            AddTriangle(i+(ligne*j),i+1+(ligne*j),i+ligne+1+(ligne*j),normals.size()-1);
        }
    }



}

void Mesh::merge(Mesh toMerge){
    int nbVertex = vertices.size();
    int nbNewVertex = toMerge.varray.size();
    int nbNormal = normals.size();
    int nbNewNormal = toMerge.narray.size();

    for(int i = 0; i<(int)toMerge.vertices.size();i++){
        vertices.push_back(toMerge.vertices[i]);
    }

    for(int i = 0; i<(int)toMerge.normals.size();i++){
        normals.push_back(toMerge.normals[i]);
    }

    for (int i = 0 ; i < nbNewVertex ; i++){
        varray.push_back(toMerge.varray[i] + nbVertex);
    }

    for (int i = 0 ; i < nbNewNormal ; i++){
        narray.push_back(toMerge.narray[i] + nbNormal);
    }

}

float distance(const Vector& a, const Vector& b){
    return sqrt(powf(b[2] - a[2],2) + powf(b[1] - a[2],2) + powf(b[0] - a[2],2));
}

void Mesh::SphereWarp(const Sphere& s, const Vector& t) {
    double d;
    Vector res;

    for (int i = 0; i < (int)vertices.size(); i++) {
        res = s.getCenter() - vertices[i];
        d = Norm(res) / s.getRayon();

        if (d < 1) {
            vertices[i] += t * ((1.0 - d * d) * (1.0 - d * d));
        }
    }
}

/*!
\brief Scale the mesh.
\param s Scaling factor.
*/
void Mesh::Scale(double s){
    // Vertexes
    for (int i = 0; i <(int) vertices.size(); i++)
    {
        vertices[i] *= s;
    }

    if (s < 0.0)
    {
        // Normals
        for (int i = 0; i < (int)normals.size(); i++)
        {
            normals[i] = -normals[i];
        }
    }
}

void Mesh::Translate(Vector t) {
    for (int i = 0; i <(int) vertices.size(); i++){
        vertices[i] += t;
    }
}

void Mesh::Rotate(Matrice r) {
    for (int i = 0; i <(int) vertices.size(); i++){
        vertices[i] = r * vertices[i] ;
    }
    for (int i = 0; i < (int)normals.size(); i++){
        normals[i] = r * normals[i];
    }
}

Mesh Mesh::ovni(const float& s, const Vector& c){
    Mesh ovni(Sphere(2,Vector(0,0,0.5)));
    ovni.merge(Mesh(1.5,2,Vector(0,0,0),50,4));

    Mesh pied1 = Mesh(Cylindre(0.25,2));
    pied1.merge(Sphere(0.35,Vector(0,0,0)));
    pied1.Rotate(Matrice::RotationX(90));
    pied1.Translate(Vector(2.25*cos(M_PI/2),2*sin(M_PI/2),-2));
    ovni.merge(pied1);

    Mesh pied2 = Mesh(Cylindre(0.25,2));
    pied2.merge(Sphere(0.35,Vector(0,0,0)));
    pied2.Rotate(Matrice::RotationX(90));
    pied2.Translate(Vector(2.25*cos(7*M_PI/6),2*sin(7*M_PI/6),-2));
    ovni.merge(pied2);

    Mesh pied3 = Mesh(Cylindre(0.25,2));
    pied3.merge(Sphere(0.35,Vector(0,0,0)));
    pied3.Rotate(Matrice::RotationX(90));
    pied3.Translate(Vector(2.25*cos(11*M_PI/6),2*sin(11*M_PI/6),-2));
    ovni.merge(pied3);

    ovni.Scale(s);
    ovni.Translate(c);

    return ovni;
}

Mesh Mesh::planete(const Vector& c){
    Sphere maSphere = Sphere(2);
    Sphere Orbite = Sphere(3);
    Mesh planete(maSphere);
    int aleaV;
    float aleaX,aleaY,aleaZ,aleaR;
    Mesh ovni;

    for(int i = 0; i<50;i++){
        aleaV = rand()% 4500 + 250;
        aleaX = (rand()% 500 - 250) / 1000.f;
        aleaY = (rand()% 500 - 250) / 1000.f;
        aleaZ = (rand()% 500 - 250) / 1000.f;
        aleaR = (rand()% 200) / 100;
        planete.SphereWarp(Sphere(aleaR, maSphere.Vertex(aleaV)), Vector(aleaX,aleaY,aleaZ));
    }
    planete.merge(Mesh(0.75,4,Vector(0,0,0),50,3));
    planete.Rotate(Matrice::RotationX(30));

    for (int i = 0 ; i <= 3 ; i++){
        aleaV = rand() % 4500;
        planete.merge(Sphere(0.1,Orbite.Vertex(aleaV)));
    }

    for (int i = 0 ; i <= 3 ; i++){
        aleaV = rand() % 4500;
        ovni = Mesh::ovni(0.1,Orbite.Vertex(aleaV));

        planete.merge(ovni);
    }
    planete.Translate(c);

    return planete;
}


#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QRegularExpression>
#include <QtCore/qstring.h>

/*!
\brief Import a mesh from an .obj file.
\param filename File name.
*/
void Mesh::Load(const QString& filename)
{
  vertices.clear();
  normals.clear();
  varray.clear();
  narray.clear();

  QFile data(filename);

  if (!data.open(QFile::ReadOnly))
    return;
  QTextStream in(&data);

  // Set of regular expressions : Vertex, Normal, Triangle
  QRegularExpression rexv("v\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)");
  QRegularExpression rexn("vn\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)\\s*([-|+|\\s]\\d*\\.\\d+)");
  QRegularExpression rext("f\\s*(\\d*)/\\d*/(\\d*)\\s*(\\d*)/\\d*/(\\d*)\\s*(\\d*)/\\d*/(\\d*)");
  while (!in.atEnd())
  {
    QString line = in.readLine();
    QRegularExpressionMatch match = rexv.match(line);
    QRegularExpressionMatch matchN = rexn.match(line);
    QRegularExpressionMatch matchT = rext.match(line);
    if (match.hasMatch())//rexv.indexIn(line, 0) > -1)
    {
      Vector q = Vector(match.captured(1).toDouble(), match.captured(2).toDouble(), match.captured(3).toDouble()); vertices.push_back(q);
    }
    else if (matchN.hasMatch())//rexn.indexIn(line, 0) > -1)
    {
      Vector q = Vector(matchN.captured(1).toDouble(), matchN.captured(2).toDouble(), matchN.captured(3).toDouble());  normals.push_back(q);
    }
    else if (matchT.hasMatch())//rext.indexIn(line, 0) > -1)
    {
      varray.push_back(matchT.captured(1).toInt() - 1);
      varray.push_back(matchT.captured(3).toInt() - 1);
      varray.push_back(matchT.captured(5).toInt() - 1);
      narray.push_back(matchT.captured(2).toInt() - 1);
      narray.push_back(matchT.captured(4).toInt() - 1);
      narray.push_back(matchT.captured(6).toInt() - 1);
    }
  }
  data.close();
}

/*!
\brief Save the mesh in .obj format, with vertices and normals.
\param url Filename.
\param meshName %Mesh name in .obj file.
*/
void Mesh::SaveObj(const QString& url, const QString& meshName) const
{
  QFile data(url);
  if (!data.open(QFile::WriteOnly))
    return;
  QTextStream out(&data);
  out << "g " << meshName << Qt::endl;
  for (int i = 0; i < (int)vertices.size(); i++)
    out << "v " << vertices.at(i)[0] << " " << vertices.at(i)[1] << " " << vertices.at(i)[2] << QString('\n');
  for (int i = 0; i <(int) normals.size(); i++)
    out << "vn " << normals.at(i)[0] << " " << normals.at(i)[1] << " " << normals.at(i)[2] << QString('\n');
  for (int i = 0; i < (int)varray.size(); i += 3)
  {
    out << "f " << varray.at(i) + 1 << "//" << narray.at(i) + 1 << " "
      << varray.at(i + 1) + 1 << "//" << narray.at(i + 1) + 1 << " "
      << varray.at(i + 2) + 1 << "//" << narray.at(i + 2) + 1 << " "
      << "\n";
  }
  out.flush();
  data.close();
}

