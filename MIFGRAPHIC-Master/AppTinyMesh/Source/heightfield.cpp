#include "heightfield.h"
#include <QString>
#include <iostream>


    //QVector2D a1, QVector2D b1,
HeightField::HeightField(QVector2D a1, QVector2D b1, QImage image, float e){
    this->a = a1;
    this->b = b1;

    this->nx = image.width();
    this->ny = image.height();

        //calcule des Z
    z.resize(nx*ny);
    QRgb gris;
    if(image.isGrayscale()){
        for(int i =0; i<nx; i++){
            for(int j=0; j<ny; j++){
                gris = image.pixel(i,j);
                z[index(i,j)]= qRed(gris) * e;
            }

        }
    }
}


int HeightField::index(int i, int j) const {
    return i+j*nx;
}


float HeightField::ScaleX() const{
    float dist = a.distanceToPoint(QVector2D(b.x(),a.y()));
    float scale = dist/nx;
    return scale;
}

float HeightField::ScaleY() const{
    float dist = a.distanceToPoint(QVector2D(b.y(),a.x()));
    float scale = dist/ny;
    return scale;
}


Vector HeightField::P(int i, int j) const {
    return Vector(a.x() + ScaleX() * i, a.y() + ScaleY() * j,z[index(i,j)]);
}

QVector2D HeightField::gradiant(int i, int j) const
{
    double dx = z[index(i + 1, j)] - z[index(i - 1, j)];
    double dy = z[index(i, j + 1)] - z[index(i, j - 1)];
    return QVector2D(dx, dy);
}

Vector HeightField::normal(int i, int j) const
{
    QVector2D grad2 = -gradiant(i, j);
    QVector3D grad3 = QVector3D(grad2.x(), grad2.y(), 1);
    QVector3D n = grad3 / norme(grad3);
    return Vector(n.x(), n.y(), n.z());
}

float HeightField::norme(QVector3D v) const{
    return sqrt(pow(v.x(),2) + pow(v.y(),2)+ pow(v.z(),2));
}



