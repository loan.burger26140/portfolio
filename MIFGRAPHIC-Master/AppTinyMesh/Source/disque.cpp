#include "disque.h"
#define M_PI 3.14159265358979323846

const Vector Disque::normal = Vector(0.0,1.0,0.0);

Disque::Disque(const float& r, const Vector& c) : center(c), rayon(r){
    int div = 100;
    double alpha;
    double step = (2* M_PI)/ (div-1);

    for (int i = 0; i < div; i++){
      alpha = i * step;
      listVertex.push_back(Vector(center[0] + rayon * cos(alpha), center[1] , center[2]+ rayon * sin(alpha)));
    }

    listVertex.push_back(center);
}

Vector Disque::Vertex(int k) const {
    return listVertex[k];
}
