#include "tore.h"
#include "matrice.h"

Tore::Tore(const float& d, const float& r, const Vector& c, const int& nbCercle, const int& nbPtCercle ) : divPtCercle(nbPtCercle), divCercle(nbCercle), center(c), rayon(r), distance(d){
    double step = (2* M_PI)/ (divCercle-1);
    double alphaCercle;
    double stepCercle;
    double alpha;
    Matrice Rotate;
    Vector res;
    listVertex.clear();
    for (int j = 0; j < divCercle; j++){
        stepCercle = (2* M_PI)/ (divPtCercle-1);
        alpha = step * j;
        Rotate = Matrice::RotationZ(alpha);
        for (int i = 0; i < divPtCercle; i++){
            alphaCercle = i * stepCercle;
            res = c + Rotate * Vector(distance + rayon * cos(alphaCercle), 0 , rayon * sin(alphaCercle));

            listVertex.push_back(c + Rotate * Vector(distance + rayon * cos(alphaCercle), 0 , rayon * sin(alphaCercle)));
            listNormal.push_back(Vector(0,1,0));
        }
    }
}

Vector Tore::Vertex(int k) const {
    return listVertex[k];
}

Vector Tore::Normal(int k) const {
    return listNormal[k];
}
