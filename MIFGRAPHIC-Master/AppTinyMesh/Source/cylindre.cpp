#include "cylindre.h"

Cylindre::Cylindre(const float& r, const float& h, const Vector& c) : center(c), rayon(r), hauteur(h){
    int div = 100;
    double alpha;
    double step = (2* 3.14159265358979323846)/ (div-1);
    for (int i = 0; i < div; i+=2)
    {
      alpha = i * step;
      listVertex.push_back(Vector(center[0] + rayon * cos(alpha), center[1] + hauteur , center[2]+ rayon * sin(alpha)));
      listVertex.push_back(Vector(center[0] + rayon * cos(alpha), center[1] , center[2]+ rayon * sin(alpha)));

      listNormal.push_back(Vector(cos(alpha), 0, sin(alpha)));
      listNormal.push_back(Vector(cos(alpha), 0, sin(alpha)));
    }



}

Vector Cylindre::Vertex(int k) const {
    return listVertex[k];
}

Vector Cylindre::Normal(int k) const {
    return listNormal[k];
}



