#include "bezier.h"

bezier::bezier()
{
}

bezier::~bezier()
{
}

void bezier::addControlPoint(std::vector<Vector> point)
{
	controlPoints.push_back(point);
}

Vector bezier::getPoint(float u, float v)
{
	Vector result(0,0,0);
	float x = 0.f;
	float y = 0.f;
	float z = 0.f;
	int n = controlPoints.size() - 1;
	int m = controlPoints[0].size() - 1;
	for (int i = 0; i <= n; i++)
	{
		float b = binomials[n][i] * pow(u, i) * pow(1 - u, n - i);
		for (int j = 0; j <= m; j++)
		{
			float b2 = binomials[m][j] * pow(v, j) * pow(1 - v, m - j);
			result[0] += b * b2 * controlPoints[i][j][0];
			result[1] += b * b2 * controlPoints[i][j][1];
			result[2] += b * b2 * controlPoints[i][j][2];
		}
	}
	return result;

}

Mesh bezier::polygonize(int resolution)
{
	std::vector<Vector> vertex;
	std::vector<Vector> normal;
	std::vector<int> triangle;
	Vector p1, p2, p3, p4;
	float step = 1.f / resolution;
	for (int i = 0; i < resolution; i++)
	{
		for (int j = 0; j < resolution; j++)
		{
			float u = i * step;
			float v = j * step;
			p1 = getPoint(u, v);
			p2 = getPoint((i + 1) * step, v);
			p3 = getPoint((i + 1) * step, (j + 1) * step);
			p4 = getPoint(u, (j + 1) * step);

			int index = vertex.size();
			vertex.push_back(p1);
			normal.push_back(p1);
			vertex.push_back(p2);
			normal.push_back(p2);
			vertex.push_back(p3);
			normal.push_back(p3);
			vertex.push_back(p4);
			normal.push_back(p4);

			triangle.push_back(index);
			triangle.push_back(index + 1);
			triangle.push_back(index + 3);

			triangle.push_back(index + 3);
			triangle.push_back(index + 1);
			triangle.push_back(index + 2);
		}
	}

	std::vector<int> normals = triangle;
	return Mesh(vertex,normal,triangle,normals);
}
