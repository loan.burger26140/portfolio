#pragma once
#include "node.h"
class torus :
    public node
{
public :
    torus(const Vector& center, const Vector& t) : center(center), t(t) {}
    double Value(const Vector& p) const override;
private :
    Vector center;
	Vector t;
};

