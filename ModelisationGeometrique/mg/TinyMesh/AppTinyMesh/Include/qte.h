#ifndef __Qte__
#define __Qte__

#include <QtWidgets/qmainwindow.h>
#include "realtime.h"
#include "meshcolor.h"
#include "node.h"

QT_BEGIN_NAMESPACE
	namespace Ui { class Assets; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT
private:
  Ui::Assets* uiw;           //!< Interface
  node* mesh;				//!< Mesh
  MeshWidget* meshWidget;   //!< Viewer
  MeshColor meshColor;		//!< Mesh.

public:
  MainWindow();
  ~MainWindow();
  void showMesh();
  void showMesh(Mesh& mesh);
  
  void CreateActions();
  void UpdateGeometry();

public slots:
  void editingSceneLeft(const Ray&);
  void editingSceneRight(const Ray&);
  void BoxMeshExample();
  void SphereImplicitExample();
  void TorusImplicitExample();
  void CapsuleImplicitExample();
  void BoxImplicitExample();
  void ResetCamera();
  void melangeExample();
  void smoothMelangeExample();
  void intersectionExample();
  void smoothIntersectionExample();
  void differenceExample();
  void smoothDifferenceExample();
  void UpdateMaterial();
  void dig();
  void bezierExemple();
  void revolutionExemple();
  void twistExemple();
  void wtwistExemple();
};

#endif
