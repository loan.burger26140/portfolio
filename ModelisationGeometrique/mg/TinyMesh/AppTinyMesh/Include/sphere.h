#pragma once
#include "node.h"
class sphere :
    public node
{
public :
    sphere(const Vector& center, const float& r) : center(center), r(r) {}
    double Value(const Vector& p) const override;
    void Polygonize(int n, Mesh& g, const Box& box, const double& epsilon = 1e-4) const override {
        node::Polygonize(n, g, box, epsilon);
    }


private :
    Vector center;
    float r;
};

