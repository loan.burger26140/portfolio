#pragma once
#include "node.h"
class boxImplicit :
    public node
{
public :
    boxImplicit(const Vector& center, const Vector& b) : center(center), b(b) {}
    virtual double Value(const Vector& p) const override;

private :
    Vector center;
    Vector b;

};

