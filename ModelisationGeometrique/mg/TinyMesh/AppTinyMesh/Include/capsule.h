#pragma once
#include "node.h"
class capsule :
    public node
{
public :
    capsule(const Vector& center, float h, float r) : center(center), h(h), r(r) {}
    double Value(const Vector& p) const override;

private:
    Vector center;
    float h;
    float r;
};

