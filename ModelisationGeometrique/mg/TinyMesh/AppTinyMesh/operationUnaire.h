#pragma once
#include "node.h"
class translation :
    public node
{
public :
    translation(node* n1, Vector v) : n1(n1), v(v) {};
	double Value(const Vector& p) const override;
private:
	node* n1;
	Vector v;
};

class rotation :
	public node
{
public:
	rotation(node* n1, double angle) : n1(n1), angle(angle) {};
	double Value(const Vector& p) const override;
private:
	node* n1;
	double angle;
};

class scale :
	public node
{
public :
	scale(node* n1, Vector v) : n1(n1), v(v) {};
	double Value(const Vector& p) const override;
private:
	node* n1;
	Vector v;

};



