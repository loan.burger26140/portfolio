#include "operationUnaire.h"
#include "node.h"

double translation::Value(const Vector& p) const
{
	return n1->Value(p - v);
}

double rotation::Value(const Vector& p) const
{
	Vector q = p;
	double c = cos(angle);
	double s = sin(angle);
	q[0] = c * p[0] - s * p[1];
	q[1] = s * p[0] + c * p[1];
	return n1->Value(q);
}

double scale::Value(const Vector& p) const
{
	Vector pScale = p;
	pScale *= v.Inverse();
	return n1->Value(pScale);
}
