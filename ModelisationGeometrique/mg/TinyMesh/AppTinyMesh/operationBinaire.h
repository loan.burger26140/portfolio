#pragma once
#include "node.h"
class Difference :
    public node
{
public:
	Difference(node* n1, node* n2) : n1(n1), n2(n2) {};
	double Value(const Vector& p) const override;

private:
	node* n1;
	node* n2;
};

class smoothDifference
	: public node
{
public:
	smoothDifference(node* n1, node* n2, double k) : n1(n1), n2(n2), k(k) {};
	double Value(const Vector& p) const override;
private:
	node* n1;
	node* n2;
	double k;

};



class Intersection :
	public node
{
public:
	Intersection(node* n1, node* n2) : n1(n1), n2(n2) {};
	double Value(const Vector& p) const override;
private :
	node* n1;
	node* n2;
};


class smoothIntersection :
	public node
{
public :
	smoothIntersection(node* n1, node* n2, double k) : n1(n1), n2(n2), k(k) {};
	double Value(const Vector& p) const override;
private:
	node* n1;
	node* n2;
	double k;
};


class Melange :
	public node
{
public:
	Melange(node* n1, node* n2) : n1(n1), n2(n2) {};
	double Value(const Vector& p) const override;
private:
	node* n1;
	node* n2;
};


class smoothMelange :
	public node
{
public :
	smoothMelange(node* n1, node* n2, double k) : n1(n1), n2(n2), k(k) {};
	double Value(const Vector& p) const override;
private :
	node* n1;
	node* n2;
	double k;
};

