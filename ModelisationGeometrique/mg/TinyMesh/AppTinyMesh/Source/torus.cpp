#include "torus.h"

double torus::Value(const Vector& p) const {
	Vector pCenter = p - center;
	Vector q = Vector(Norm(Vector(pCenter[0],0.0, pCenter[2])) - t[0], pCenter[1], 0.0);
	return Norm(q) - t[1];
}