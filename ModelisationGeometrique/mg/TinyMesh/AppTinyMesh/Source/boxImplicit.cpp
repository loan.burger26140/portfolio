#include "boxImplicit.h"

double boxImplicit::Value(const Vector& p) const {
	Vector pCenter = p - center;
	Vector q = Abs(pCenter) - b;
	Vector nul = Vector(0.0,0.0,0.0);
	return Norm(Vector::Max(q, nul)) + Math::Min(Math::Max(q[0], Math::Max(q[1], q[2])), 0.0);
}