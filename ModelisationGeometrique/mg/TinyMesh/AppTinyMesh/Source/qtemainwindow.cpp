#include "qte.h"
#include "implicits.h"
#include "torus.h"
#include "sphere.h"
#include "boxImplicit.h"
#include "../operationBinaire.h"
#include "../operationUnaire.h"
#include "../bezier.h"
#include "../revolution.h"
#include "../deform.h"
#include "capsule.h"
#include "ui_interface.h"
#include <random>

node* chooseMesh(int ind) {
	switch (ind) {
	case 0: return new sphere(Vector(0, 0, 0), 1);
	case 1: return new capsule(Vector(0, 0, 0), 1, 0.5);
	case 2: return new torus(Vector(0, 0, 0), Vector(1, 0.2, 0));
	case 3: return new boxImplicit(Vector(0, 0, 0), Vector(1, 1, 1));
	}
}

MainWindow::MainWindow() : QMainWindow(), uiw(new Ui::Assets)
{
	// Chargement de l'interface
    uiw->setupUi(this);

	// Chargement du GLWidget
	meshWidget = new MeshWidget;
	QGridLayout* GLlayout = new QGridLayout;
	GLlayout->addWidget(meshWidget, 0, 0);
	GLlayout->setContentsMargins(0, 0, 0, 0);
    uiw->widget_GL->setLayout(GLlayout);
	// Creation des connect
	CreateActions();

	meshWidget->SetCamera(Camera(Vector(10, 0, 0), Vector(0.0, 0.0, 0.0)));
}

MainWindow::~MainWindow()
{
	delete meshWidget;
}

void MainWindow::CreateActions()
{
	// Buttons
    connect(uiw->boxMesh, SIGNAL(clicked()), this, SLOT(BoxMeshExample()));
    connect(uiw->sphereImplicit, SIGNAL(clicked()), this, SLOT(SphereImplicitExample()));
	connect(uiw->capsuleImplicit, SIGNAL(clicked()), this, SLOT(CapsuleImplicitExample()));
	connect(uiw->boxImplicit, SIGNAL(clicked()), this, SLOT(BoxImplicitExample()));
	connect(uiw->torusImplicit, SIGNAL(clicked()), this, SLOT(TorusImplicitExample()));
    connect(uiw->resetcameraButton, SIGNAL(clicked()), this, SLOT(ResetCamera()));
	connect(uiw->melange, SIGNAL(clicked()), this, SLOT(melangeExample()));
    connect(uiw->smoothMelange, SIGNAL(clicked()), this, SLOT(smoothMelangeExample()));
	connect(uiw->intersection, SIGNAL(clicked()), this, SLOT(intersectionExample()));
	connect(uiw->smoothIntersection, SIGNAL(clicked()), this, SLOT(smoothIntersectionExample()));
	connect(uiw->difference, SIGNAL(clicked()), this, SLOT(differenceExample()));
    connect(uiw->smoothDifference, SIGNAL(clicked()), this, SLOT(smoothDifferenceExample()));
	connect(uiw->shoot, SIGNAL(clicked()), this, SLOT(dig()));
	connect(uiw->bezierButton, SIGNAL(clicked()), this, SLOT(bezierExemple()));
	connect(uiw->revolutionButton, SIGNAL(clicked()), this, SLOT(revolutionExemple()));
	connect(uiw->twistButton, SIGNAL(clicked()), this, SLOT(twistExemple()));
	connect(uiw->wtwistButton, SIGNAL(clicked()), this, SLOT(wtwistExemple()));
    connect(uiw->wireframe, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_1, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
    connect(uiw->radioShadingButton_2, SIGNAL(clicked()), this, SLOT(UpdateMaterial()));
	

	// Widget edition
	connect(meshWidget, SIGNAL(_signalEditSceneLeft(const Ray&)), this, SLOT(editingSceneLeft(const Ray&)));
	connect(meshWidget, SIGNAL(_signalEditSceneRight(const Ray&)), this, SLOT(editingSceneRight(const Ray&)));
}

void MainWindow::editingSceneLeft(const Ray&)
{
}

void MainWindow::editingSceneRight(const Ray&)
{
}

void MainWindow::showMesh() {
	Mesh implicitMesh;
	mesh->Polygonize(64, implicitMesh, Box(2.0));

	std::vector<Color> cols;
	cols.resize(implicitMesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(implicitMesh, cols, implicitMesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::showMesh(Mesh& mesh) {
	std::vector<Color> cols;
	cols.resize(mesh.Vertexes());
	for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(0.8, 0.8, 0.8);

	meshColor = MeshColor(mesh, cols, mesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::BoxMeshExample()
{
	Mesh boxMesh = Mesh(Box(1.0));


	std::vector<Color> cols;
	cols.resize(boxMesh.Vertexes());
    for (size_t i = 0; i < cols.size(); i++)
		cols[i] = Color(double(i) / 6.0, fmod(double(i) * 39.478378, 1.0), 0.0);

	meshColor = MeshColor(boxMesh, cols, boxMesh.VertexIndexes());
	UpdateGeometry();
}

void MainWindow::SphereImplicitExample()
{
	mesh = new sphere(Vector(0, 0, 0), 1);
	showMesh();
}

void MainWindow::CapsuleImplicitExample()
{
	mesh = new capsule(Vector(0, 0, 0), 1, 1);
	showMesh();
}

void MainWindow::BoxImplicitExample()
{
	//mesh = new boxImplicit(Vector(0, 0, 0), Vector(0.5,0.3,0.4));
	//showMesh();
	
	//sphere* tete = new sphere(Vector(0, 0, 0), 1.5);
	//sphere* oeilG = new sphere(Vector(0.75,1.2,0.6), 0.5);
	//sphere* oeilD = new sphere(Vector(-0.75, 1.2, 0.6), 0.5);
	//mesh = new Melange(tete, oeilG);
	//mesh = new smoothMelange(mesh, oeilD,0.25);
	//torus* bouche = new torus(Vector(0, 2.35, -2.5), Vector(1, 0.25, 1));
	//scale* boucheScale = new scale(bouche, Vector(1,0.5,0.25));
	//
	//mesh = new smoothDifference(boucheScale, mesh, 0.5);
	// 
	sphere* cabine = new sphere(Vector(0, 0, 0), 1.5);
	scale* cabineScale = new scale(cabine, Vector(0.3, 0.5, 1));
	
	sphere* aileron = new sphere(Vector(0, 0, 0), 1);
	scale* aileronScale = new scale(aileron, Vector(1.6, 0.3, 0.5));
	smoothMelange* cabineScale2 = new smoothMelange(cabineScale, aileronScale, 0.2);

	mesh = new scale(cabineScale2, Vector(1.2,0.7, 1.2));
	showMesh();


}

void MainWindow::TorusImplicitExample()
{
	mesh = new torus(Vector(0, 0, 0), Vector(1, 0.2,0));
	showMesh();

}

void MainWindow::melangeExample()
{
	node* implicit = chooseMesh(uiw->Forme1->currentIndex());
	node* implicit2 = chooseMesh(uiw->Forme2->currentIndex());
	mesh = new Melange(implicit, implicit2);
	showMesh();
}

void MainWindow::smoothMelangeExample()
{
	node* implicit = chooseMesh(uiw->Forme1->currentIndex());
	node* implicit2 = chooseMesh(uiw->Forme2->currentIndex());
	mesh = new smoothMelange(implicit, implicit2, uiw->kSpinBox->value());
	showMesh();
}

void MainWindow::intersectionExample()
{
	node* implicit = chooseMesh(uiw->Forme1->currentIndex());
	node* implicit2 = chooseMesh(uiw->Forme2->currentIndex());
	mesh = new Intersection(implicit, implicit2);
	showMesh();
}

void MainWindow::smoothIntersectionExample()
{
	node* implicit = chooseMesh(uiw->Forme1->currentIndex());
	node* implicit2 = chooseMesh(uiw->Forme2->currentIndex());
	mesh = new smoothIntersection(implicit, implicit2,uiw->kSpinBox->value());
	showMesh();
	
}

void MainWindow::differenceExample()
{
	node* implicit = chooseMesh(uiw->Forme1->currentIndex());
	node* implicit2 = chooseMesh(uiw->Forme2->currentIndex());
	mesh = new Difference(implicit, implicit2);
	showMesh();
}

void MainWindow::smoothDifferenceExample()
{
	node* implicit = chooseMesh(uiw->Forme1->currentIndex());
	node* implicit2 = chooseMesh(uiw->Forme2->currentIndex());
	mesh = new smoothDifference(implicit, implicit2, uiw->kSpinBox->value());
	showMesh();
}

void MainWindow::dig() {
	double d;

	for (int i = 0; i < 40; i++) {
		std::random_device rd;
		std::mt19937 generator(rd());
		std::uniform_real_distribution<double> distributionCoord(-0.20, 0.20);
		std::uniform_real_distribution<double> distributionK(0, 0.25);

		Ray ray(Vector(5, 0, 0), Vector(-1, distributionCoord(generator), distributionCoord(generator)));
		if (mesh->Intersect(ray, d)) {
			sphere* i = new sphere(ray.Origin() + ray.Direction() * d, 0.2);
			smoothDifference* a = new smoothDifference(i, mesh, 0.1);//distributionK(generator));
			mesh = a;
		}
	}
	showMesh();
}

void MainWindow::bezierExemple() {

	bezier b;
	std::vector<Vector> points1;
	points1.push_back(Vector(-1, -2, 0) * 1.75);
	points1.push_back(Vector(2, 0.5, 1) * 1.75);
	points1.push_back(Vector(0, -1, 2) * 1.75);
	b.addControlPoint(points1);
	std::vector<Vector> points2;
	points2.push_back(Vector(2, 2, 0) * 1.75);
	points2.push_back(Vector(0, 2, 1) * 1.75);
	points2.push_back(Vector(-1, 2, 2) * 1.75);
	b.addControlPoint(points2);
	std::vector<Vector> points3;
	points3.push_back(Vector(1, 3, 0) * 1.75);
	points3.push_back(Vector(2, 1, 1) * 1.75);
	points3.push_back(Vector(2, 2, 2) * 1.75);
	b.addControlPoint(points3);
	

	//std::vector<std::vector<Vector>> points;
	//int divBeta = 30;
	//int divAlpha = divBeta / 2;
	//points.resize(divAlpha);
	////int nbV = divAlpha * divBeta;
	//double alpha1, beta;
	//for (int i = 0; i < divAlpha; i++) {
	//	alpha1 = -0.5f * M_PI + float(i) * M_PI / divAlpha;
	//	for (int j = 0; j <= divBeta; j += 1) {
	//		beta = float(j) * 2.0f * M_PI / divBeta;
	//		points[i].push_back(Vector(cos(beta) * (1 + cos(alpha1)), sin(beta) * (1 + cos(alpha1)), sin(alpha1)));
	//	}
	//}
	//for (int i = 0; i < points.size(); i++) {
	//	b.addControlPoint(points[i]);
	//}
	Mesh result = b.polygonize(16);
	showMesh(result);

}

void MainWindow::revolutionExemple() {
	revolution r;

	r.addControlPoint(Vector(-3, 0, 0));
	r.addControlPoint(Vector(-2, 4, 0));
	r.addControlPoint(Vector(-1, 1, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(0, 1, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(1, 2, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(2, 5, 0)); // Point � mi-hauteur sur le c�t� du vase
	r.addControlPoint(Vector(3, 1.2, 0));  // Point � l'extr�mit� sup�rieure du vase
	r.addControlPoint(Vector(4, 1, 0)); // Point au-dessus de l'extr�mit� du vase
	r.addControlPoint(Vector(5, 0.9, 0)); // Point au sommet du vase
	r.addControlPoint(Vector(6, 2, 0)); // Point au-dessus du sommet du vase


	Mesh result = r.polygonize(64);
	//Scale s(1, 3, 1);
	//s.DeformMesh(result);
	showMesh(result);
}

void MainWindow::twistExemple() {
	//revolution r;

	//r.addControlPoint(Vector(-3, 0, 0));
	//r.addControlPoint(Vector(-2.9, 3, 0));
	//r.addControlPoint(Vector(-2.8, 3, 0));  // Point de d�part � la base du vase
	//r.addControlPoint(Vector(-2.7, 3, 0));  // Point de d�part � la base du vase
	//r.addControlPoint(Vector(-2, 3, 0));  // Point de d�part � la base du vase
	//r.addControlPoint(Vector(-1, 3, 0)); // Point � mi-hauteur sur le c�t� du vase
	//r.addControlPoint(Vector(0, 3, 0));  // Point � l'extr�mit� sup�rieure du vase
	//r.addControlPoint(Vector(1, 3, 0)); // Point au-dessus de l'extr�mit� du vase
	//r.addControlPoint(Vector(2, 3, 0)); // Point au sommet du vase
	//r.addControlPoint(Vector(2.7, 3, 0));
	//r.addControlPoint(Vector(2.8, 3, 0));  // Point de d�part � la base du vase
	//r.addControlPoint(Vector(2.9, 3, 0));  // Point de d�part � la base du vase
	//r.addControlPoint(Vector(3, 0, 0));  // Point de d�part � la base du vase
	
	revolution r;

	r.addControlPoint(Vector(-3, 0, 0));
	r.addControlPoint(Vector(-2, 4, 0));
	r.addControlPoint(Vector(-1, 1, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(0, 1, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(1, 2, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(2, 5, 0)); // Point � mi-hauteur sur le c�t� du vase
	r.addControlPoint(Vector(3, 1.2, 0));  // Point � l'extr�mit� sup�rieure du vase
	r.addControlPoint(Vector(4, 1, 0)); // Point au-dessus de l'extr�mit� du vase
	r.addControlPoint(Vector(5, 0.9, 0)); // Point au sommet du vase
	r.addControlPoint(Vector(6, 2, 0)); // Point au-dessus du sommet du vase

	Mesh result = r.polygonize(64);
	Twist t(1.5);
	t.DeformMesh(result);
	showMesh(result);
}

void MainWindow::wtwistExemple() {
	revolution r;

	r.addControlPoint(Vector(-3, 0, 0));
	r.addControlPoint(Vector(-2.9, 3, 0));
	r.addControlPoint(Vector(-2.8, 3, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(-2.7, 3, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(-2, 3, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(-1, 3, 0)); // Point � mi-hauteur sur le c�t� du vase
	r.addControlPoint(Vector(0, 3, 0));  // Point � l'extr�mit� sup�rieure du vase
	r.addControlPoint(Vector(1, 3, 0)); // Point au-dessus de l'extr�mit� du vase
	r.addControlPoint(Vector(2, 3, 0)); // Point au sommet du vase
	r.addControlPoint(Vector(2.7, 3, 0));
	r.addControlPoint(Vector(2.8, 3, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(2.9, 3, 0));  // Point de d�part � la base du vase
	r.addControlPoint(Vector(3, 0, 0));  // Point de d�part � la base du vase

	Mesh result = r.polygonize(64);

	showMesh(result);
}

void MainWindow::UpdateGeometry()
{
	meshWidget->ClearAll();
	meshWidget->AddMesh("BoxMesh", meshColor);
	/*meshWidget->AddMesh("Mesh", mesh);*/

    uiw->lineEdit->setText(QString::number(meshColor.Vertexes()));
    uiw->lineEdit_2->setText(QString::number(meshColor.Triangles()));

	UpdateMaterial();
}

void MainWindow::UpdateMaterial()
{
    meshWidget->UseWireframeGlobal(uiw->wireframe->isChecked());

    if (uiw->radioShadingButton_1->isChecked())
		meshWidget->SetMaterialGlobal(MeshMaterial::Normal);
	else
		meshWidget->SetMaterialGlobal(MeshMaterial::Color);
}

void MainWindow::ResetCamera()
{
	meshWidget->SetCamera(Camera(Vector(-10.0), Vector(0.0)));
}


