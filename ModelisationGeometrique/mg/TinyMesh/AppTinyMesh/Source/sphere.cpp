#include "sphere.h"

double sphere::Value(const Vector& p) const
{
	Vector pCenter = p - center;
	return Norm(pCenter) - r;
}
