#include "capsule.h"

double capsule::Value(const Vector& p) const {
	Vector pCenter = p - center;
	double newY = pCenter[1] - Math::Clamp(pCenter[1], 0.0, h);
	Vector q = Vector(pCenter[0], newY, pCenter[2]);
	return Norm(q) - r;
}