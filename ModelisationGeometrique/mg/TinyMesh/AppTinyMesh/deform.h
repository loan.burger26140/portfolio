#pragma once

#include "mesh.h"

class Scale {
public:
    Scale(float factorX, float factorY, float factorZ)
        : factorX(factorX), factorY(factorY), factorZ(factorZ) {}

    Vector Warp(const Vector& point) const;
    void DeformMesh(Mesh& mesh);

private:
    float factorX;
    float factorY;
    float factorZ;
};

class Twist {
public:
    Twist(float angle, Vector axis = Vector(0,0,1)) : angle(angle), axis(axis){}
    Vector Warp(const Vector& point) const;
    void DeformMesh(Mesh& mesh);
    

private:
    float angle;
    Vector axis;
};



