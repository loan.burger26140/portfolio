#include "deform.h"


// Produit scalaire (dot product)
float Dot(const Vector& v1, const Vector& v2) {
    return v1[0] *v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

// Produit vectoriel (cross product)
Vector Cross(const Vector& v1, const Vector& v2) {
    return Vector(
        v1[1] * v2[2] - v1[2] * v2[1],
        v1[2] * v2[0] - v1[0] * v2[2],
        v1[0] * v2[1] - v1[1] * v2[0]
    );
}

Vector Scale::Warp(const Vector& point) const {
    float x = point[0] * factorX;
    float y = point[1] * factorY;
    float z = point[2] * factorZ;
    return Vector(x, y, z);
}

void Scale::DeformMesh(Mesh& mesh) {
    for (int i = 0; i < mesh.vertices.size(); ++i) {
        mesh.vertices[i] = Warp(mesh.vertices[i]);
    }
}

Vector Twist::Warp(const Vector& point) const {
    Vector normalizedAxis = Normalized(axis);

    float distanceAlongAxis = Dot(point, normalizedAxis);
    float twistAmount = angle * distanceAlongAxis;
    float cosAngle = cos(twistAmount);
    float sinAngle = sin(twistAmount);
    return point * cosAngle + Cross(normalizedAxis, point) * sinAngle + normalizedAxis * Dot(normalizedAxis, point) * (1 - cosAngle);
}

void Twist::DeformMesh(Mesh& mesh) {
    for (int i = 0; i < mesh.vertices.size(); ++i) {
        Vector p = Warp(mesh.vertices[i]);
        mesh.vertices[i] = p;
        if(mesh.normals.size() > i)
            mesh.normals[i] = p;
    }
}
