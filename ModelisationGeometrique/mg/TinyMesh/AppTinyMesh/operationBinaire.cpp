#include "operationBinaire.h"

double mix(double d1, double d2, float h) {
	return d1 * (1 - h) + d2 * h;
}

double Difference::Value(const Vector& p) const
{
	return std::max(-(n1->Value(p)), n2->Value(p));
}

double smoothDifference::Value(const Vector& p) const
{
	const double d1 = n1->Value(p);
	const double d2 = n2->Value(p);
	const double h = Math::Clamp(0.5 - 0.5 * (d2 + d1) / k);
	return mix(d2, -d1, h) + k * h * (1.0 - h);
}

double Intersection::Value(const Vector& p) const
{
	return std::max(n1->Value(p), n2->Value(p));
}

double smoothIntersection::Value(const Vector& p) const
{
	const double d1 = n1->Value(p);
	const double d2 = n2->Value(p);
	const double h = Math::Clamp(0.5 - 0.5 * (d2 - d1) / k);
	return mix(d2, d1, h) + k * h * (1.0 - h);
}

double Melange::Value(const Vector& p) const
{
	return std::min(n1->Value(p) , n2->Value(p));
}

double smoothMelange::Value(const Vector& p) const
{
	const double d1 = n1->Value(p);
	const double d2 = n2->Value(p);
	const double h = Math::Clamp(0.5 + 0.5 * (d2 - d1) / k);
	return mix(d2, d1, h) - k * h * (1.0 - h);
}

