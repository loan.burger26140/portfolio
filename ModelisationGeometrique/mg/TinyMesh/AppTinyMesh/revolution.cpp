#include "revolution.h"
#include <QtCore/qmath.h>

revolution::revolution()
{
}

revolution::~revolution()
{
}

void revolution::addControlPoint(Vector point)
{
	controlPoints.push_back(point);
}


Vector revolution::getPoint(float u, float theta) {
	Vector result(0, 0, 0);
	int n = controlPoints.size() - 1;
	for (int i = 0; i <= n; i++) {
		float b = binomials[n][i] * pow(u, i) * pow(1 - u, n - i);
		float x = controlPoints[i][0];
		float y = controlPoints[i][1] * cos(theta) - controlPoints[i][2] * sin(theta);
		float z = controlPoints[i][1] * sin(theta) + controlPoints[i][2] * cos(theta);

		result[0] += b * x;
		result[1] += b * y;
		result[2] += b * z;
	}
	return result;
}


Mesh revolution::polygonize(int resolution) {
    std::vector<Vector> vertex;
    std::vector<Vector> normal;
    std::vector<int> triangle;
	Vector p1, p2, p3, p4;
    for (int i = 0; i < resolution; i++) {
		float u = (float)i / (float)resolution;
		float uNext = (float)(i+1) / (float)resolution;
		for (unsigned int rot = 0; rot <= resolution; ++rot) {
			float ang = (float)rot / (float)resolution;
			ang *= 2 * M_PI;
			float angNext = (float)(rot + 1) / (float)resolution;
			angNext *= 2 * M_PI;

			p1 = getPoint(u, ang);
			p2 = getPoint(uNext, ang);
			p3 = getPoint(uNext, angNext);
			p4 = getPoint(u, angNext);

			int index = vertex.size();
			vertex.push_back(p1);
			normal.push_back(p1);
			vertex.push_back(p2);
			normal.push_back(p2);
			vertex.push_back(p3);
			normal.push_back(p3);
			vertex.push_back(p4);
			normal.push_back(p4);

			triangle.push_back(index);
			triangle.push_back(index + 1);
			triangle.push_back(index + 3);

			triangle.push_back(index + 3);
			triangle.push_back(index + 1);
			triangle.push_back(index + 2);
		}
	}

	std::vector<int> normals = triangle;
	return Mesh(vertex, normal, triangle, normals);
}