
#ifndef VIEWER_ETUDIANT_H
#define VIEWER_ETUDIANT_H
#define MAXTAILLE 30

#include "Viewer.h"



class ViewerEtudiant : public Viewer
{
public:
    ViewerEtudiant();
    int init();
    int render();
    int update( const float time, const float delta );
    int compteur = 0;




protected:

    /* -----------------------------------------
     Pour creer un nouvel objet vous devez :

     1. declarer ici dans le fichier Viewer_etudiant.h
     le Mesh,
     la texture si besoin,
     une fonction 'init_votreObjet'
     une fonction 'draw_votreObjet(const Transform& T)'

     --- Rq : regarder comment cela est effectue dans le fichier Viewer.h


     2. Appeler la fonction 'init_votreObjet' dans la fonction 'init' du Viewer_etudiant.cpp
     --- Rq : regarder comment cela est effectue dans le fichier Viewer.cpp


     3. Appeler la fonction 'draw_votreObjet' dans la fonction 'render' du Viewer_etudiant.cpp
     --- Rq : regarder comment cela est effectue dans le fichier Viewer.cpp

      ----------------------------------------- */

    Mesh m_cube;
    void init_Cube();

    Mesh m_Cylinder;
    void init_Cylinder();

    Mesh m_cone;
    void init_cone();

    Mesh m_sphere;
    void init_sphere();

    Mesh m_triangle;
    void init_triangle();

    Mesh m_terrain;
    void init_terrain(Mesh& m_terrain, const Image& im);

    Mesh m_billboard;
    void init_billboard(const Image& im );
    Point pos[10];

    Mesh m_cubeMap;
    void init_CubeMap(Mesh& m_cubeMap);

    void create_vertex_normal_banc();
    void init_banc();
    int obj_extru_NBPT = 6;
    int obj_extru_NBROT = 5;
    Point obj_extru_p[6];
    Point obj_extru_v[5][6];
    Vector obj_extru_vn[5][6];
    Mesh m_banc;

    Transform Mouvement;





    /// Declaration des Textures

     GLuint texture_avion;
     Image m_terrainAlti;
     GLuint terrain_texture;
     GLuint arbre_texture;
      GLuint texture_dirigeable;
     GLuint texture_cubeMap;
     GLuint texture_animation1;
     GLuint texture_animation2;
     GLuint texture_animation3;
     GLuint texture_animation4;
     GLuint texture_test;


    /// Declaration des fonction de creation de Mesh du type init_votreObjet()


    /// Declaration des fonctions draw_votreObjet(const Transform& T)

    void draw_cube(const Transform& T, unsigned int tex);
    void draw_cylinder(const Transform& T, unsigned int tex);
    void draw_cone (const Transform& T, unsigned int tex);
    void draw_sphere (const Transform& T, unsigned int tex);
    void draw_triangle(const Transform& T);
    void draw_CorpsAvion(const Transform& T,  unsigned int tex);
    void draw_avion(const Transform& T,  unsigned int tex);
    void draw_dirigeable(const Transform& T,  unsigned int tex);
    void draw_terrain(const Transform& T, unsigned int tex);
    void draw_billboard(const Transform& T, unsigned int tex,const Image& im);
    void draw_cubeMap(const Transform& T, unsigned int tex);
    void draw_banc(const Transform& T,const Image& im);
    void draw_anime(const Transform& T, unsigned int tex1,unsigned int tex2,unsigned int tex3,unsigned int tex4,const Image& im);

};



#endif
