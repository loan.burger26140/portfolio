
#include <cassert>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "draw.h"        // pour dessiner du point de vue d'une camera
#include "Viewer_etudiant.h"
#define ConvTaille 1

using namespace std;


/*
 * Fonction dans laquelle les initialisations sont faites.
 */
int ViewerEtudiant::init()
{
    Viewer::init();



    m_camera.lookat( Point(0,0,0), 150 );
    m_terrainAlti=  read_image("data/terrain/terrain.png");



    /// Appel des fonctions init_votreObjet pour creer les Mesh
      init_Cube();
      init_Cylinder();
      init_cone();
      init_sphere();
      init_triangle();
      init_terrain(m_terrain, m_terrainAlti);
      init_billboard(m_terrainAlti);
      init_CubeMap(m_cubeMap);

      create_vertex_normal_banc();
      init_banc();

    /// Chargement des textures
    texture_avion = read_texture(0,"data/metal.jpg");
    terrain_texture = read_texture(0,"data/terrain/terrain_texture.png");
    arbre_texture= read_texture(0,"data/billboard/tree2.png");
    texture_dirigeable= read_texture(0,"data/mariniere.jpg");
    texture_cubeMap= read_texture(0,"data/cubemap/MontagneHD.jpg");
    texture_animation1= read_texture(0,"data/animation/perso1.png");
    texture_animation2= read_texture(0,"data/animation/perso2.png");
    texture_animation3= read_texture(0,"data/animation/perso3.png");
    texture_animation4= read_texture(0,"data/animation/perso4.png");
    texture_test = read_texture(0,"data/monde.jpg");





    return 0;
}


    /// Declaration des Mesh
    //Initialisation du mesh du cube
    void ViewerEtudiant::init_Cube()
    {
        static float pt[8][3] = {{-1,-1,1},{1,-1,1},{1,-1,-1},{-1,-1,-1},{-1,1,1},{1,1,1},{1,1,-1},{-1,1,-1}};
        static int f[6][4] = {{0,1,2,3},{0,1,5,4},{1,2,6,5},{2,3,7,6},{3,0,4,7},{4,5,6,7}};
        static float n[6][3] = {{0,-1,0},{0,0,1},{1,0,0},{0,0,-1},{-1,0,0},{0,1,0}};

        m_cube= Mesh(GL_TRIANGLE_STRIP);
        for (int i=0;i<6;i++)
        {
            m_cube.normal(n[i][0],n[i][1],n[i][2]);
            m_cube.texcoord(0,0);
            m_cube.vertex(pt[f[i][0]][0],pt[f[i][0]][1],pt[f[i][0]][2]);
            m_cube.texcoord(1,0);
            m_cube.vertex(pt[f[i][1]][0],pt[f[i][1]][1],pt[f[i][1]][2]);
            m_cube.texcoord(0,1);
            m_cube.vertex(pt[f[i][3]][0],pt[f[i][3]][1],pt[f[i][3]][2]);
            m_cube.texcoord(1,1);
            m_cube.vertex(pt[f[i][2]][0],pt[f[i][2]][1],pt[f[i][2]][2]);
            m_cube.restart_strip();
        }

    }

      //Initialisation du mesh du Cylindre
    void ViewerEtudiant::init_Cylinder()
    {
        const int div= 25;
        float alpha;
        float step=2*M_PI/div;
        m_Cylinder = Mesh(GL_TRIANGLE_STRIP);
        for (int i=0;i<=div;i++)
        {
            alpha=i*step;
            m_Cylinder.normal(Vector(cos(alpha),0,sin(alpha)));
            m_Cylinder.texcoord(i/(float)div,0);
            m_Cylinder.vertex(Point(cos(alpha),-2,sin(alpha)));
            m_Cylinder.normal(Vector(cos(alpha),0,sin(alpha)));
            m_Cylinder.texcoord(i/(float)div,1);
            m_Cylinder.vertex(Point(cos(alpha),2,sin(alpha)));

        }
    }
      //Initialisation du mesh du cone

    void ViewerEtudiant::init_cone()
    {
        const int div = 20;
        float alpha;
        float step=2*M_PI/div;
        m_cone = Mesh(GL_TRIANGLE_STRIP);
        for (int i=0;i<=div;i++)
        {
            alpha = i*step;
            m_cone.normal(Vector(cos(alpha)/sqrt(2.f),1.f/sqrt(2.f),sin(alpha)/sqrt(2.f)));
            m_cone.texcoord(i/(float)div,0);
            m_cone.vertex(Point(cos(alpha),0,sin(alpha)));
            m_cone.normal(Vector(cos(alpha)/sqrt(2.f),1.f/sqrt(2.f),sin(alpha)/sqrt(2.f)));
            m_cone.texcoord(i/(float)div,1);
            m_cone.vertex(Point(0,1,0));

        }
    }

 //Initialisation du mesh de la sphere
    void ViewerEtudiant::init_sphere()
    {
        const int divBeta = 20;
        const int divAlpha = divBeta/2;
        float beta,alpha,alpha2;
        m_sphere = Mesh(GL_TRIANGLE_STRIP);
        for (int i=0;i<divAlpha;i++)
        {
            alpha = -0.5*M_PI+ float(i)*M_PI/divAlpha;
            alpha2 = -0.5*M_PI+ float(i+1)*M_PI/divAlpha;
            for (int j = 0;j<=divBeta;j++ )
            {
                beta = float(j)*2.f*M_PI/divBeta;
                m_sphere.normal(Vector( cos(alpha)*cos(beta) , sin(alpha) , cos(alpha)*sin(beta) ));
                m_sphere.texcoord(1-beta/(2*M_PI),0.5+alpha/M_PI);
                m_sphere.vertex(Point( cos(alpha)*cos(beta) , sin(alpha) , cos(alpha)*sin(beta) ));
                m_sphere.normal(Vector( cos(alpha2)*cos(beta) , sin(alpha2) , cos(alpha2)*sin(beta) ));
                m_sphere.texcoord(1-beta/(2*M_PI),0.5+alpha2/M_PI);
                m_sphere.vertex(Point( cos(alpha2)*cos(beta) , sin(alpha2) , cos(alpha2)*sin(beta) ));
            }
        }
    }

    //Pour l'arriere et le dessus de mon objet complexe
    void ViewerEtudiant::init_triangle()
    {
        m_triangle = Mesh(GL_TRIANGLE_STRIP);

        m_triangle.normal(Vector(0,1,0));
        m_triangle.vertex(Point(0,0,-1));

        m_triangle.vertex(Point(-1,0,1));

        m_triangle.vertex(Point(1,0,1));


        m_triangle.normal(Vector(0,-1,0));
        m_triangle.vertex(Point(0,0,-1));

        m_triangle.vertex(Point(-1,0,1));

        m_triangle.vertex(Point(1,0,1));


    }
    Vector terrainNormal(const Image& im, const int i, const int j)
  {
      int ip= i-1;
      int in = i+1;
      int jp= j-1;
      int jn= j+1;
      Vector a( ip, im(ip, j).r,j );
      Vector b( in, im(in, j).r,j );
      Vector c( i, im(i, jp).r,jp);
      Vector d( i, im(i, jn).r,jn);
      Vector ab = normalize(b -a);
      Vector cd = normalize(d -c);
      Vector n = -cross(ab,cd);
      return n;

  }


    void ViewerEtudiant::init_terrain(Mesh& m_terrain, const Image& im)
    {
        m_terrain = Mesh(GL_TRIANGLE_STRIP);
        for (int i = 1; i<im.width()-2 ; ++i)
        {
            for (int j = 1; j<im.height()-1 ; ++j)
            {
                m_terrain.normal(terrainNormal(im,i+1,j));
                m_terrain.texcoord((float)i/im.width(),(float)j/im.height());
                m_terrain.vertex(Point(i+1,25.f*im(i+1,j).r,j));

                m_terrain.normal(terrainNormal(im,i,j));
                m_terrain.texcoord((float)i/im.width(),(float)j/im.height());
                m_terrain.vertex(Point(i,25.f*im(i,j).r,j));

            }
            m_terrain.restart_strip();
        }
    }

    // Une face carr�
    void ViewerEtudiant::init_billboard(const Image& im)
    {

        m_billboard = Mesh(GL_TRIANGLE_STRIP);
        m_billboard.normal(Vector(0,0,1));
        m_billboard.texcoord(0,0);
        m_billboard.vertex(Point(0,0,0));

        m_billboard.normal(Vector(0,0,1));
        m_billboard.texcoord(1,0);
        m_billboard.vertex(Point(1,0,0));

        m_billboard.normal(Vector(0,0,1));
        m_billboard.texcoord(0,1);
        m_billboard.vertex(Point(0,1,0));

        m_billboard.normal(Vector(0,0,1));
        m_billboard.texcoord(1,1);
        m_billboard.vertex(Point(1,1,0));

        for (int i = 0; i<10 ; i++)
        {
            pos[i].x = rand()%(im.height()-3);
            pos[i].z = rand()%(im.width()-3);
        }




    }

    // utilisation d'un tableau 3D pour mettre les coordonn�es de texture pour chaque sommet de chaque face
    void ViewerEtudiant::init_CubeMap(Mesh& m_cubeMap)
    {
        static float pt[8][3] = {{-1,-1,1},{1,-1,1},{1,-1,-1},{-1,-1,-1},{-1,1,1},{1,1,1},{1,1,-1},{-1,1,-1}};
        static int f[6][4] = { {0,1,2,3}, {5,4,7,6}, {2,1,5,6}, {0,3,7,4}, {3,2,6,7}, {1,0,4,5} };
        static float n[6][3] = {{0,1,0},{0,-1,0},{-1,0,0},{1,0,0},{0,0,1},{0,0,-1}};  //{{0,-1,0},{0,0,-1},{-1,0,0},{0,0,1},{1,0,0},{0,1,0}};
        static float tc[6][4][2] = { {{ 1/4.f , 0 } , { 2/4.f , 0 } , { 1/4.f , 1/3.f } , { 2/4.f , 1/3.f }} ,    //Bas
                                   {{ 1/4.f , 2/3.f } , { 2/4.f , 2/3.f } , { 1/4.f , 1 } , { 2/4.f , 1 }}   ,   // haut
                                    {{ 2/4.f , 1/3.f } , { 3/4.f , 1/3.f } , { 2/4.f , 2/3.f } , { 3/4.f , 2/3.f }} ,   //droite
                                    {{ 0 , 1/3.f } , { 1/4.f , 1/3.f } , { 0 , 2/3.f } , { 1/4.f , 2/3.f }} ,   // gauche
                                    {{ 1/4.f , 1/3.f } , { 2/4.f , 1/3.f } , { 1/4.f , 2/3.f } , { 2/4.f , 2/3.f }}  ,  //devant
                                    {{ 3/4.f , 1/3.f } , { 1 , 1/3.f } , { 3/4.f , 2/3.f } , { 1 , 2/3.f }} //derriere
                                    };

        m_cubeMap= Mesh(GL_TRIANGLE_STRIP);
        for (int i=0;i<6;i++)
        {
            m_cubeMap.normal(Vector(n[i][0],n[i][1],n[i][2]));

            m_cubeMap.texcoord(tc[i][0][0],tc[i][0][1]);
            m_cubeMap.vertex(pt[f[i][0]][0],pt[f[i][0]][1],pt[f[i][0]][2]);


            m_cubeMap.texcoord(tc[i][1][0],tc[i][1][1]);
            m_cubeMap.vertex(pt[f[i][1]][0],pt[f[i][1]][1],pt[f[i][1]][2]);


            m_cubeMap.texcoord(tc[i][2][0],tc[i][2][1]);
            m_cubeMap.vertex(pt[f[i][3]][0],pt[f[i][3]][1],pt[f[i][3]][2]);


            m_cubeMap.texcoord(tc[i][3][0],tc[i][3][1]);
            m_cubeMap.vertex(pt[f[i][2]][0],pt[f[i][2]][1],pt[f[i][2]][2]);

            m_cubeMap.restart_strip();
        }

    }


    void ViewerEtudiant::create_vertex_normal_banc()
    {
        obj_extru_p[0] = Point(0,0,0);
        obj_extru_p[1] = Point(1,0,0);
        obj_extru_p[2] = Point(1.0,1.0,0);
        obj_extru_p[3] = Point(0,1.0,0);
        obj_extru_p[4] = Point(0,2,0);
        obj_extru_p[5] = Point(0,0,0);

        for(int i= 0; i<obj_extru_NBROT; i++)
        {

            float mat[16] = {
                1,  0,  0,  0,
                0,  1,  0,  0,
                0,  0,  1,  i,
                0,  0,  0,  1 };

                for(int j=0; j<obj_extru_NBPT; j++)
                {
                    obj_extru_v[i][j].x = mat[0] * obj_extru_p[j].x + mat[1] * obj_extru_p[j].y + mat[2]  * obj_extru_p[j].z + mat[3]  * 1;
                    obj_extru_v[i][j].y = mat[4] * obj_extru_p[j].x + mat[5] * obj_extru_p[j].y + mat[6]  * obj_extru_p[j].z + mat[7]  * 1;
                    obj_extru_v[i][j].z = mat[8] * obj_extru_p[j].x + mat[9] * obj_extru_p[j].y + mat[10]  * obj_extru_p[j].z + mat[11]  * 1;
                }
        }

        for(int i=0; i<obj_extru_NBROT;i++)
        {
            for(int j=0; j<obj_extru_NBPT;j++)
            {
                obj_extru_vn[i][j] = Vector(0,0,0);

                Vector a, b, vtmp;

                a = normalize(obj_extru_v[i][j]-obj_extru_v[i][j+1]);
                b = normalize(obj_extru_v[i][j]-obj_extru_v[(i+1)%obj_extru_NBROT][j]);

                vtmp = cross(a,b);

                obj_extru_vn[i][j] = vtmp + obj_extru_vn[i][j];
                obj_extru_vn[(i+1)%obj_extru_NBROT][j] = vtmp + obj_extru_vn[(i+1)%obj_extru_NBROT][j];
                obj_extru_vn[i][j+1] = vtmp + obj_extru_vn[i][j+1];
                obj_extru_vn[(i+1)%obj_extru_NBROT][j+1] = vtmp + obj_extru_vn[(i+1)%obj_extru_NBROT][j+1];
            }
        }
         for(int i=0; i<obj_extru_NBROT;i++)
        {
            for(int j=0; j<obj_extru_NBPT;j++)
            {
                float q = 4.0f;
                if(j==obj_extru_NBPT-1)
                {
                    q=2.0f;
                    obj_extru_vn[i][j] = obj_extru_vn[i][j]/q;
                }
            }
        }

    }

void ViewerEtudiant::init_banc()
{
     m_banc = Mesh(GL_TRIANGLES);


    for(int i=0; i<obj_extru_NBROT; i++)
    {
        for(int j=0; j<obj_extru_NBPT-1; j++)
        {
            //triangle 1
            m_banc.normal(obj_extru_vn[i][j]);
            m_banc.vertex(obj_extru_v[i][j]);

            m_banc.normal(obj_extru_vn[(i+1)%obj_extru_NBROT][j+1]);
            m_banc.vertex(obj_extru_v[(i+1)%obj_extru_NBROT][j+1]);

            m_banc.normal(obj_extru_vn[(i+1)%obj_extru_NBROT][j]);
            m_banc.vertex(obj_extru_v[(i+1)%obj_extru_NBROT][j]);

            //triangle 2

            m_banc.normal(obj_extru_vn[i][j]);
            m_banc.vertex(obj_extru_v[i][j]);

            m_banc.normal(obj_extru_vn[i][j+1]);
            m_banc.vertex(obj_extru_v[i][j+1]);

            m_banc.normal(obj_extru_vn[(i+1)%obj_extru_NBROT][j+1]);
            m_banc.vertex(obj_extru_v[(i+1)%obj_extru_NBROT][j+1]);


        }
    }

}









void ViewerEtudiant::draw_cube(const Transform& T, unsigned int tex)
{
 gl.model( T );
 gl.texture(tex);
 gl.lighting(false);
 gl.draw( m_cube );
}

void ViewerEtudiant::draw_cylinder(const Transform& T,unsigned int tex)
{
 gl.model( T );
 gl.texture(tex);
 gl.lighting(false);
 gl.draw( m_Cylinder );
}

void ViewerEtudiant::draw_cone(const Transform& T ,unsigned int tex)
{
 gl.model( T );
 gl.texture(tex);
 gl.draw( m_cone );
}

void ViewerEtudiant::draw_sphere(const Transform& T, unsigned int tex)
{
 gl.model( T );
 gl.texture(tex);
 gl.lighting(false);
 gl.draw( m_sphere );
}

void ViewerEtudiant::draw_triangle(const Transform& T)
{
 gl.model( T );
 gl.lighting(false);
 gl.draw( m_triangle );
}



void ViewerEtudiant::draw_CorpsAvion(const Transform& T, unsigned int tex)
{

  gl.lighting(false);
  gl.model(T*Rotation(Vector(1,0,0),90) );
  gl.texture(tex);
  gl.draw(m_Cylinder);

  Transform TconeAv = Rotation(Vector(1,0,0),270) * Translation(0,2,0);
  gl.model(T * TconeAv );
  gl.draw(m_cone);

  Transform TconeDer = Rotation(Vector(1,0,0),90) * Translation(0,2,0);
  gl.model(T * TconeDer );
  gl.draw(m_cone);

}

void ViewerEtudiant::draw_avion(const Transform& T, unsigned int tex)
{
    draw_CorpsAvion(T * Identity(),tex);
    draw_CorpsAvion(T *Translation(3,0.3,0) * Scale(0.3,0.3,0.3),tex);
    draw_CorpsAvion(T * Translation(-3,0.3,0) * Scale(0.3,0.3,0.3),tex);
    draw_cube(T * Translation(0,0.5,0) * Scale(5,0.1,0.5),tex);

}

void ViewerEtudiant::draw_dirigeable(const Transform& T, unsigned int tex)
{
    gl.lighting(false);
    gl.model(T * Scale(0.5,0.5,2));
    gl.texture(tex);
    gl.draw(m_sphere);     // ballon

    gl.model(T *Translation(0,-1/4.f,-1/2.f)* Scale(1/16.f,1/2.f,1/16.f));
    gl.draw(m_cube);   // barre 1 de soutien de la cabine

    gl.model(T *Translation(0,-1/4.f,1/2.f)* Scale(1/16.f,1/2.f,1/16.f));
    gl.draw(m_cube);    // barre 2 de soutien de la cabine

    gl.model(T* Translation(0,-3/4.f,1/8.f) * Scale(1/6.f,1/8.f,3/4.f));
    gl.draw(m_cube);    // Cabine

    gl.model(T * Translation(0,0,-1.5) * RotationY(180)  * RotationZ(45) * Scale(1.5,0,0.5));
    gl.draw(m_triangle);  //aileron arriere

    gl.model(T * Translation(0,0,-1.5) * RotationY(180) * RotationZ(-45) * Scale(1.5,0,0.5));
    gl.draw(m_triangle);    //aileron arriere


    gl.model(T * Translation(0,2/4.f,0)* RotationY (90)* RotationX(90) * Scale(1/4.f,1/4.f,1/4.f));
    gl.draw(m_triangle);   //aileron superieur
}

void ViewerEtudiant::draw_terrain(const Transform& T, unsigned int tex)
{
    gl.model(T);
    gl.lighting(false);
    gl.texture(tex);
    gl.draw(m_terrain);

}
// Draw billboard a 2 face recto-verso pour pouvoir le voir de tout les cot� + position aleatoire donc hauteur definis par rapport au terrain
void ViewerEtudiant::draw_billboard(const Transform& T, unsigned int tex,const Image& im)
{

    gl.lighting(false);
    gl.texture(tex);
    gl.alpha(0.5f);
    for (int i = 0; i< 10 ; i++)
    {

        gl.model(T*Translation(-100*ConvTaille,-50,-100*ConvTaille)*Translation((pos[i].x + 1) * ConvTaille, (25.f * im(pos[i].x + 1, pos[i].z).r) * ConvTaille , pos[i].z * ConvTaille)* Scale(3,3,3));
        gl.texture(tex);
        gl.draw(m_billboard);
                              // translation car map centrer en 0,0 // translation pour croiser les quad // Translation de positionement aleatoire et ajustement de la hauteur     // Rotation pour croiser les quad // Scale pour agrandir
        gl.model(T*Translation(-100*ConvTaille,-50,-100*ConvTaille)*Translation(1.5,0,1.5)* Translation((pos[i].x + 1) * ConvTaille, (25.f * im(pos[i].x + 1, pos[i].z).r) * ConvTaille , pos[i].z * ConvTaille)*RotationY(90) * Scale(3,3,3) );  /// Decalage psk map pas au centre * decalage pour billboard * decalage par rapport au coordone choisi * rotation pour billboard
        gl.texture(tex);
        gl.draw(m_billboard);


        gl.model(T*Translation(-100*ConvTaille,-50,-100*ConvTaille)*Translation(3,0,0 )*Translation((pos[i].x + 1) * ConvTaille, (25.f * im(pos[i].x + 1, pos[i].z).r) * ConvTaille , pos[i].z * ConvTaille)*RotationY(180)* Scale(3,3,3));
        gl.texture(tex);
        gl.draw(m_billboard);


        gl.model(T*Translation(-100*ConvTaille,-50,-100*ConvTaille)*Translation(1.5,0,-1.5)*Translation((pos[i].x + 1) * ConvTaille, (25.f * im(pos[i].x + 1, pos[i].z).r) * ConvTaille , pos[i].z * ConvTaille)*RotationY(-90) * Scale(3,3,3));
        gl.texture(tex);
        gl.draw(m_billboard);

    }
}


void ViewerEtudiant::draw_cubeMap(const Transform& T, unsigned int tex)
{
    gl.model( T );
    gl.texture(tex);

    gl.draw( m_cubeMap);
}

void ViewerEtudiant::draw_banc(const Transform& T,const Image& im)
{

    /*for (int i = 0; i< 10 ; i++)
    {
    gl.model(T*Translation(-100*ConvTaille,-50,-100*ConvTaille)*Translation((pos[i].x + 1) * ConvTaille, (25.f * im(pos[i].x + 6, pos[i].z).r) * ConvTaille , pos[i].z * ConvTaille)*Scale(1/2.f,1/2.f,1/2.f));
    gl.draw(m_banc);
    }*/
    gl.model(T);
    gl.draw(m_banc);
}


void ViewerEtudiant::draw_anime(const Transform& T,unsigned int tex1,unsigned int tex2,unsigned int tex3,unsigned int tex4,const Image& im)
{
    gl.alpha(0.5f);
    if (compteur%4==0)
    {
        gl.texture(tex1);
        gl.model(T*Translation(-100*ConvTaille,-50,-100*ConvTaille)*Translation((pos[1].x + 1) * ConvTaille, (25.f * im(pos[1].x + 8, pos[1].z).r) * ConvTaille , pos[1].z * ConvTaille)*Scale(1/2.f,1,1/2.f));
        gl.draw(m_quad);

    }
    else if (compteur%4==1)
    {
        gl.texture(tex2);
         gl.model(T*Translation(-100*ConvTaille,-50,-100*ConvTaille)*Translation((pos[1].x + 1) * ConvTaille, (25.f * im(pos[1].x + 8, pos[1].z).r) * ConvTaille , pos[1].z * ConvTaille)*Scale(1/2.f,1,1/2.f));
        gl.draw(m_quad);
    }
    else if (compteur%4==2)
    {
        gl.texture(tex3);
         gl.model(T*Translation(-100*ConvTaille,-50,-100*ConvTaille)*Translation((pos[1].x + 1) * ConvTaille, (25.f * im(pos[1].x + 8, pos[1].z).r) * ConvTaille , pos[1].z * ConvTaille)*Scale(1/2.f,1,1/2.f));
        gl.draw(m_quad);
    }
    else if (compteur%4==3)
    {
        gl.texture(tex4);
        gl.model(T*Translation(-100*ConvTaille,-50,-100*ConvTaille)*Translation((pos[1].x + 1) * ConvTaille, (25.f * im(pos[1].x + 8, pos[1].z).r) * ConvTaille , pos[1].z * ConvTaille)*Scale(1/2.f,1,1/2.f));
        gl.draw(m_quad);
    }

}









/*
 * Fonction dans laquelle les appels pour les affichages sont effectues.
 */



int ViewerEtudiant::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    manageCameraLight();

    gl.camera(m_camera);

        ///Appel des fonctions du type 'draw_votreObjet'

   // draw_cube(Mouvement,texture_test);
    //draw_cylinder(Identity(),texture_test);
   // draw_cone(Identity(),texture_test);
    //draw_sphere(Identity(),texture_test);
   //draw_avion(Mouvement*Translation(-40,50,-30),texture_avion);
   draw_terrain(Translation(-100*ConvTaille,-50,-100*ConvTaille)*Scale(1*ConvTaille,1*ConvTaille,1*ConvTaille),terrain_texture);    //Augmentation/dimunution de la taille via une variable pour pouvoir toujours centrer la map en 0;0;0 quelque soit notre choix
   draw_billboard(Identity(),arbre_texture, m_terrainAlti);
   draw_cubeMap(Translation(0,30,0)*Scale(100,100,100),texture_cubeMap);
   draw_dirigeable(Mouvement*Translation(20,20,10)*Scale(4,4,4),texture_dirigeable);
   draw_dirigeable(Mouvement*Translation(60,40,20)*Scale(4,4,4),texture_dirigeable);
    draw_banc(Translation(5,0,0),m_terrainAlti);
    draw_anime(Translation(7,1,0),texture_animation1,texture_animation2,texture_animation3,texture_animation4,m_terrainAlti);






    return 1;

}


/*
 * Fonction dans laquelle les mises a jours sont effectuees.
 */
int ViewerEtudiant::update( const float time, const float delta )
{
    // time est le temps ecoule depuis le demarrage de l'application, en millisecondes,
    // delta est le temps ecoule depuis l'affichage de la derniere image / le dernier appel a draw(), en millisecondes.
    int tmp = int(time);
    if (tmp%20==1)
    {compteur++;}

    float ts = time/1000;
    int te = int(ts);
    int ite = te%m_anim.nb_points();
    float pds = ts - te;
    Point p0 = m_anim[ite];
    int ite_suiv = (te+1)%m_anim.nb_points();
    int ite_suiv_suiv = (ite_suiv+1)%m_anim.nb_points();
    Point p1 = m_anim[ite_suiv];
    Point p2 = m_anim[ite_suiv_suiv];

    Vector pos = Vector(p0) + (Vector(p1)-Vector(p0))*pds ;
    Vector pos_suiv = Vector(p1) + (Vector(p2) - Vector(p1)) * pds;

    Vector dir = normalize(pos_suiv - pos);
    Vector up = Vector(0,1,0);
    Vector codir = cross(dir, up);

    Mouvement = Transform(dir, up, codir, pos);

    return 0;
}


/*
 * Constructeur.
 */

ViewerEtudiant::ViewerEtudiant() : Viewer()
{
}


/*
 * Programme principal.
 */
int main( int argc, char **argv )
{

    srand(time(NULL));
    ViewerEtudiant v;
    v.run();
    return 0;
}
