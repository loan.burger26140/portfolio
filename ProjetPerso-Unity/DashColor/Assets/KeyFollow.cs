using UnityEngine;
using UnityEngine.UI;

public class KeyFollow : MonoBehaviour
{
    public GameObject pointToFollow;
    public float timeOffset;
    private Vector3 velocity;

    private bool isInRange;
    private Text interactUI;
    private bool isFollowing = false;

    private void Awake() {
        interactUI = GameObject.FindGameObjectWithTag("InteractUI").GetComponent<Text>();
    }

    void Update()
    {   
        if(isInRange && Input.GetKeyDown(KeyCode.E) && Inventory.instance.followingObject == null){
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            Inventory.instance.followingObject = gameObject;
            interactUI.enabled = false;
            isFollowing = true;

        }
        if(isFollowing){
            transform.position = Vector3.SmoothDamp(transform.position, pointToFollow.transform.position, ref velocity, timeOffset); 
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            isInRange = true;
            interactUI.enabled = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player")){
            isInRange = false;
            interactUI.enabled = false;
        }
    }
}
