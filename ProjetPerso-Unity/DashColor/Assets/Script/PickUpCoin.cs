
using UnityEngine;

public class PickUpCoin : MonoBehaviour
{
    public AudioClip soundEffect;
    
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            AudioManager.instance.playClip(soundEffect, transform.position);
            Inventory.instance.addCoin(1);
            CurrentSceneManager.instance.coinCountInThisScene++;
            Destroy(gameObject);
        }
    }

}
