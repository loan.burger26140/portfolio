using UnityEngine;

public class WeakSpot : MonoBehaviour
{
    public GameObject toDestroy;
    public AudioClip soundEffect;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            AudioManager.instance.playClip(soundEffect, transform.position);
            Destroy(toDestroy);
        }
    }
}
