using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;
    public float climbSpeed;
    public float jumpForce;

    private bool isJumping = false;
    private bool isGrounded = false;
    [HideInInspector]
    public bool isClimbing = false;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask collisionLayer;

    public SpriteRenderer spriteRenderer;
    public Animator animator;
    public Rigidbody2D rb;
    public CapsuleCollider2D myCollider;
    private Vector3 velocity = Vector3.zero;
    private float horizontalMovement;
    private float verticalMovement;

    public AudioClip soundEffectJump;

    public static PlayerMovement instance;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple PlayerMovement");
            return;
        }
        instance = this;
    }
    
    void Update()
    {
        if (Input.GetButtonDown("Jump") && isGrounded && !isClimbing) {
            AudioManager.instance.playClip(soundEffectJump, transform.position);
            isJumping = true;
        }

        flip(rb.velocity.x);
        float charaVelocity = Mathf.Abs(rb.velocity.x);
        animator.SetFloat("Speed", charaVelocity);
        animator.SetBool("isClimbing", isClimbing);

    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, collisionLayer);
        movePlayer(horizontalMovement, verticalMovement);
        horizontalMovement = Input.GetAxis("Horizontal") * moveSpeed * Time.fixedDeltaTime;
        verticalMovement = Input.GetAxis("Vertical") * climbSpeed * Time.fixedDeltaTime;
    }

    void movePlayer(float _horizontalMovement, float _verticalMovement)
    {
        Vector3 targetVelocityHorizontal = new Vector2(_horizontalMovement, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocityHorizontal, ref velocity, .05f);

        if(!isClimbing){
            if (isJumping) {
                rb.AddForce(new Vector2(0f, jumpForce));
                isJumping = false;
            }
        }else{
            Vector3 targetVelocityVertical = new Vector2(rb.velocity.x, _verticalMovement);
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocityVertical, ref velocity, .05f);

        }
        
    }

    void flip(float _velocity)
    {
        if(_velocity > 0.1f) {
            spriteRenderer.flipX = false;
        }else if( _velocity < -0.1f) { 
            spriteRenderer.flipX = true;
        }

    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }
}
    