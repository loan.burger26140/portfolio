
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{

    public Text nameTxt;
    public Text dialogueText;
    public Image HeadNpc;

    public bool isFinish = false;
    public Animator animator;
    private Queue<string> sentences;
    public static DialogueManager instance;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple DialogueManager");
            return;
        }
        instance = this;
        sentences = new Queue<string>();
        
    }
    
    public void startDialogue(Dialogue dialogue){
        isFinish = false;
        animator.SetBool("Dialogue", true);
        nameTxt.text = dialogue.name;
        HeadNpc = dialogue.npcHead;
        sentences.Clear();

        foreach(string sentence in dialogue.sentences){
            sentences.Enqueue(sentence);
        }

        AfficheNextSentence();
    }

    public void AfficheNextSentence(){
        if(sentences.Count == 0){
            isFinish = true;
            FinDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(typingMachineSentence(sentence));
    }

    IEnumerator typingMachineSentence(string sentence){
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray()){
            dialogueText.text += letter;
            yield return new WaitForSeconds(0.005f);
        }

    }

    public void FinDialogue(){
        
        animator.SetBool("Dialogue", false);
    }
}
