
using UnityEngine;

public class itemDataBase : MonoBehaviour
{
    public ItemData[] allItem;
    public static itemDataBase instance;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple itemDataBase");
            return;
        }
        instance = this;

    }
}
