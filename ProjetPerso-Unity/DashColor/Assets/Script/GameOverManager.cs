
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    public GameObject gameOverUI;

    public static GameOverManager instance;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple GameOverManager");
            return;
        }
        instance = this;
    }

    public void onPlayerDeath(){
        gameOverUI.SetActive(true);
    }

    public void retryButton(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        gameOverUI.SetActive(false);
        Inventory.instance.removeCoin(CurrentSceneManager.instance.coinCountInThisScene);
        PlayerHealth.instance.respawn();
    }

    public void mainMenuButton(){
        SceneManager.LoadScene("MainMenu");
    }

    public void quitButton(){
        Application.Quit();
    }

}
