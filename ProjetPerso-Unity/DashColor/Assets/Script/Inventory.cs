using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
    public int coinsCount = 0;
    public Text coinsCountTxt ;

    public List<ItemData> content = new List<ItemData>();
    private int contentCurrentIndex = 0;
    public Image ItemImage;
    public Text ItemText;

    public Sprite itemVide;

    public GameObject followingObject;

    public static Inventory instance;

    public PlayerEffect playerEffect;

    public bool isLightOn = false;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple inventory");
            return;
        }
        instance = this;
       
    }

    private void Start() {
        updateInventoryUI();
    }

    public void addCoin(int nbCoins){
        coinsCount+=nbCoins;
        updateText();
    }

    public void removeCoin(int nbCoins){
        coinsCount-=nbCoins;
        updateText();
    }

    public void updateText(){
        coinsCountTxt.text = coinsCount.ToString();
    }

    public void consommeItem(){
        if(content.Count == 0){
            return;
        }
        ItemData currentItem = content[contentCurrentIndex];
        PlayerHealth.instance.heal(currentItem.heal);
        playerEffect.AddSpeed(currentItem.boostSpeed, currentItem.speedDuration);
        isLightOn = currentItem.isLighting;
        if(isLightOn){
            playerEffect.AddLight();
        }
        content.Remove(currentItem); 
        getNextItem();
        updateInventoryUI();
    }

    public void getNextItem(){
        if(content.Count == 0){
            return;
        }
        contentCurrentIndex = ((contentCurrentIndex+1) >= content.Count) ? 0 : (contentCurrentIndex+1);
        updateInventoryUI(); 
    }

    public void getPreviousItem(){
        if(content.Count == 0){
            return;
        }
        contentCurrentIndex = ((contentCurrentIndex-1) < 0) ? (content.Count-1) : (contentCurrentIndex-1);
        updateInventoryUI();
    }

    public void updateInventoryUI(){
        if(content.Count == 0){
            ItemImage.sprite = itemVide;
            ItemText.text = "No Item";
        }else{
            ItemImage.sprite = content[contentCurrentIndex].image;
            ItemText.text = content[contentCurrentIndex].itemName;
        }
    }

    public void useKey(){
        Destroy(followingObject);
    }
}
