using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadSpecificScene : MonoBehaviour
{
    public string sceneName;
    private bool isInteract;
    private Text interactUI;
    private Animator fadeSystem;
    private Animator DoorAnim;
    private Animator cadenaAnim;
    public bool isLocked;
    public GameObject cadena;
 
    void Awake() {
        interactUI = GameObject.FindGameObjectWithTag("InteractUI").GetComponent<Text>();
        fadeSystem = GameObject.FindGameObjectWithTag("FadeSystem").GetComponent<Animator>();
        DoorAnim = GetComponent<Animator>();
        cadenaAnim = cadena.GetComponent<Animator>();
        if(isLocked){
            cadena.SetActive(true);
        }
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.E) && isInteract && !isLocked){
            interactUI.enabled = false;
            StartCoroutine(loadNextScene());
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            interactUI.enabled = true;
            isInteract = true;
            if(isLocked && Inventory.instance.followingObject != null){ //inventory possede clef
                unlock();
            }
            if(!isLocked){
                DoorAnim.SetTrigger("OpenDoor");
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player")){
            interactUI.enabled = false;
            isInteract = false;
            if(!isLocked){
                DoorAnim.SetTrigger("CloseDoor");
            }
        }
    }

    public IEnumerator loadNextScene(){
        LoadAndSaveData.instance.saveData();
        fadeSystem.SetTrigger("FadeIn");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(sceneName);
    }

    
    private void unlock(){
        cadenaAnim.SetTrigger("unlockDoor");
        isLocked = false;
        Inventory.instance.useKey();
    }
}
