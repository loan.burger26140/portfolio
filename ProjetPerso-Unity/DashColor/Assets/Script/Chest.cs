
using UnityEngine;
using UnityEngine.UI;

public class Chest : MonoBehaviour
{
    private bool isTaken = false;
    private bool isInRange;
    private Text interactUI;
    private Animator chestAnim;

    public AudioClip soundEffect;
    public int nbPieceInside;



    private void Awake() {
        interactUI = GameObject.FindGameObjectWithTag("InteractUI").GetComponent<Text>();
        chestAnim = GetComponent<Animator>();
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.E) && isInRange){
            interactUI.enabled = false;
            isTaken=true;
            Inventory.instance.addCoin(nbPieceInside);
            AudioManager.instance.playClip(soundEffect,transform.position);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            if (!isTaken){
                interactUI.enabled = true;
                isInRange = true;
                chestAnim.SetTrigger("IsInRange");
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player")){
            interactUI.enabled = false;
            isInRange = false;
            if (!isTaken){
                chestAnim.SetTrigger("IsNotInRange");
            }
        }
    }

}
