using UnityEngine;

public class CurrentSceneManager : MonoBehaviour
{
    public int coinCountInThisScene;
    public Vector3 respawnPoint;
    public int levelToUnlock;
    public static CurrentSceneManager instance;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple CurrentSceneManager");
            return;
        }
        instance = this;

        respawnPoint = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

}
