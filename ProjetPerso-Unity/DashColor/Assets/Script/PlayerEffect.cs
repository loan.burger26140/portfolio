
using System.Collections;
using UnityEngine;

public class PlayerEffect : MonoBehaviour
{
    private bool isFaster;
    public SpriteRenderer graphics;

    public void AddSpeed(int speedGiven, float speedDuration){
        PlayerMovement.instance.moveSpeed += speedGiven;
        isFaster = true;
        StartCoroutine(SpeedFlash());
        StartCoroutine(RemoveSpeed(speedGiven,speedDuration));
    }

    private IEnumerator RemoveSpeed(int speedGiven, float speedDuration){
        yield return new WaitForSeconds(speedDuration);
        PlayerMovement.instance.moveSpeed -= speedGiven;
        isFaster = false;
    }

    public IEnumerator SpeedFlash(){
        while(isFaster){
            graphics.color = new Color(0.51068f, 0.8293777f, 0.8962264f);
            yield return new WaitForSeconds(0.3f);
            graphics.color = new Color(1f, 1f, 1f);
            yield return new WaitForSeconds(0.3f);
        }
    }

    public void AddLight(){
        graphics.color = new Color(0.51068f, 0.8293777f, 0.8962264f);
    }
}
