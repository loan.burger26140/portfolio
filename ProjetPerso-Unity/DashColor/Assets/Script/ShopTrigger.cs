using UnityEngine.UI;
using UnityEngine;

public class ShopTrigger : MonoBehaviour
{
    private bool isInRange;

    private Text interactUI;

    public string nameShopTxt;
    public ItemData[] itemToSell;

    private void Awake() {
        interactUI = GameObject.FindGameObjectWithTag("InteractUI").GetComponent<Text>();
    }

    void Update()
    {
        if(isInRange && Input.GetKeyDown(KeyCode.E)){
            interactUI.enabled = false;
            ShopManager.instance.OpenShop(itemToSell,nameShopTxt);
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            isInRange = true;
            interactUI.enabled = true;
        }
        
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player")){
            isInRange = false;
            interactUI.enabled = false;
            ShopManager.instance.closeShop();

        }
    }
}
