
using UnityEngine;

public class NextNPCToDisplay : MonoBehaviour
{
    public GameObject[] GuidePos;
    public int currentPos;
    public bool inDialogue;

    public static NextNPCToDisplay instance;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple NextNPCToDisplay");
            return;
        }
        instance = this;
    }

    private void Update() {
        if(!inDialogue && DialogueManager.instance.animator.GetBool("Dialogue")){
            inDialogue = true;
        }

        if(inDialogue && !DialogueManager.instance.animator.GetBool("Dialogue")){
            if (DialogueManager.instance.isFinish){
                DisplayNextOne();
            }
            inDialogue = false;
        }
    }

    public void DisplayNextOne(){
        GuidePos[currentPos].SetActive(false);
        currentPos++;
        if(currentPos  == GuidePos.Length){
            return;
        }
        GuidePos[currentPos].SetActive(true);
    }





}
