using UnityEngine;
using UnityEngine.UI;

public class sellButtonItem : MonoBehaviour
{
    public Text nameItem;
    public Text prix;
    public Image Graphics;

    public ItemData item;

    
    public void BuyItem(){
        Inventory inventory = Inventory.instance;
        if(inventory.coinsCount >= item.prix){
            inventory.content.Add(item);
            inventory.updateInventoryUI();
            inventory.coinsCount-= item.prix;
            inventory.updateText();
        }
    }
}
