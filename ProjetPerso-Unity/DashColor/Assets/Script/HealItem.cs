
using UnityEngine;

public class HealItem : MonoBehaviour
{
    public int valueHeal;
    public AudioClip soundEffect;
    

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            if(PlayerHealth.instance.currentHealth < PlayerHealth.instance.MaxHealth){
                AudioManager.instance.playClip(soundEffect, transform.position);
                PlayerHealth.instance.heal(valueHeal);
                Destroy(gameObject);
            }
        }
        
    }
}
