using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private Animator CheckPointAnim;

    private void Awake() {  
        CheckPointAnim = GameObject.FindGameObjectWithTag("CheckPoint").GetComponent<Animator>();   
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            CurrentSceneManager.instance.respawnPoint = transform.position;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            CheckPointAnim.SetTrigger("isCheckValid");

        }
    }

}
