
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour
{
    public Button[] buttons;

    private void Start() {
        int reach = PlayerPrefs.GetInt("Level",1);
        for (int i = 0; i< buttons.Length; i++){
            if(i + 1 > reach){
                buttons[i].interactable = false;
            }
        }
    }

    public void loadLevelSelect(string levelName){
        SceneManager.LoadScene(levelName);
    }

}
