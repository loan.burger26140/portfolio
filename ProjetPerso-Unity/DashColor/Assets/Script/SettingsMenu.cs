using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer mixer;
    public Dropdown resolutionDropDown;
    private Resolution[] resolutions;

    public Slider musicSlider;
    public Slider soundSlider;

    private void Start() {
        mixer.GetFloat("Music", out float musicValue);
        musicSlider.value = musicValue;

        mixer.GetFloat("Sound", out float soundValue);
        soundSlider.value = soundValue;

        resolutions = Screen.resolutions.Select(resolution => new Resolution {width = resolution.width, height = resolution.height}).Distinct().ToArray();
        resolutionDropDown.ClearOptions();

        List<string> options= new List<string>();
        int currentIndex = 0;
        for(int i=0; i< resolutions.Length; i++){
            string myOption = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(myOption);

            if(resolutions[i].width == Screen.width && resolutions[i].height == Screen.height){
                currentIndex = i;
            }
        }   
        resolutionDropDown.AddOptions(options);
        resolutionDropDown.value = currentIndex;
        resolutionDropDown.RefreshShownValue();
    }

    public void setMusicVolume(float volume){
        mixer.SetFloat("Music",volume);
    }

    public void setSoundVolume(float volume){
        mixer.SetFloat("Sound",volume);
    }

    public void setFullScreen(bool isFullScreen){
        Screen.fullScreen = isFullScreen;
    }

    public void setResolution(int resolutionIndex){
        Resolution reso = resolutions[resolutionIndex];
        Screen.SetResolution(reso.width,reso.height,Screen.fullScreen) ;
    }

    public void clearData(){
        PlayerPrefs.DeleteAll();
    }


}
