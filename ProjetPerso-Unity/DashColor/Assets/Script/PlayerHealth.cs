
using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    public int MaxHealth = 100;
    public int currentHealth;

    private bool isInvincible = false;
    public SpriteRenderer graphics;

    public HealthBar healthBar;
    public Animator bloodAnim;

    public AudioClip soundEffectDamage;
    public AudioClip soundEffectDie;

    public static PlayerHealth instance;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple PlayerHealth");
            return;
        }
        instance = this;
    }

    void Start()
    {
        currentHealth = MaxHealth;
        healthBar.setMaxHealth(MaxHealth);
    }

    public void TakeDamage(int damage){
        if(!isInvincible){
            currentHealth -= damage;
            healthBar.setHealth(currentHealth);
            if(currentHealth <= 0){
                die();
                return;
            }
            AudioManager.instance.playClip(soundEffectDamage,transform.position);
            bloodAnim.SetTrigger("takeDamage");
            isInvincible = true;
            StartCoroutine(invincibleFlash());
            StartCoroutine(invincibleDelay());
        }
    }

    public void die(){
        AudioManager.instance.playClip(soundEffectDie,transform.position);
        PlayerMovement.instance.enabled = false;
        PlayerMovement.instance.animator.SetTrigger("isDie");
        PlayerMovement.instance.rb.bodyType = RigidbodyType2D.Kinematic;
        PlayerMovement.instance.rb.velocity = Vector3.zero;
        PlayerMovement.instance.myCollider.enabled = false;
        GameOverManager.instance.onPlayerDeath();
    }

    public void respawn(){
        PlayerMovement.instance.enabled = true;
        PlayerMovement.instance.animator.SetTrigger("isRespawn");
        PlayerMovement.instance.rb.bodyType = RigidbodyType2D.Dynamic;
        PlayerMovement.instance.myCollider.enabled = true;
        currentHealth = MaxHealth;
        healthBar.setHealth(currentHealth);
    }

    public void heal(int value){
        currentHealth = Mathf.Min(MaxHealth,currentHealth+value);
        healthBar.setHealth(currentHealth);
    }

    public IEnumerator invincibleFlash(){
        while(isInvincible){
            graphics.color = new Color(1f, 1f, 1f, 0.25f);
            yield return new WaitForSeconds(0.3f);
            graphics.color = new Color(1f, 1f, 1f, 1f);
            yield return new WaitForSeconds(0.3f);
        }
    }

    public IEnumerator invincibleDelay(){
        yield return new WaitForSeconds(2f);
        isInvincible = false;
    }
}
