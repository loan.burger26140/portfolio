using UnityEngine;

public class Ladder : MonoBehaviour
{   
    public bool isInRange;
    private PlayerMovement playerMovement;
    public BoxCollider2D myCollider;

    private void Awake() {
        playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }


    private void Update() {
        if(isInRange && (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow))){
            playerMovement.isClimbing = true;
            myCollider.enabled = false;
        }    
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            isInRange= true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player")){
            isInRange= false;
            playerMovement.isClimbing = false;
            myCollider.enabled = true;;
        }
    }

}
