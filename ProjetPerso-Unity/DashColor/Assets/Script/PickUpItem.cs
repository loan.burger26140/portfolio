
using UnityEngine;
using UnityEngine.UI;

public class PickUpItem : MonoBehaviour
{
    private bool isInRange;
    private Text interactUI;

    public AudioClip soundEffect;
    public ItemData item;



    private void Awake() {
        interactUI = GameObject.FindGameObjectWithTag("InteractUI").GetComponent<Text>();
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.E) && isInRange){
            interactUI.enabled = false;
            Inventory.instance.content.Add(item);
            Inventory.instance.updateInventoryUI();
            AudioManager.instance.playClip(soundEffect,transform.position);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            interactUI.enabled = true;
            isInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player")){
            interactUI.enabled = false;
            isInRange = false;
        }
    }

}
