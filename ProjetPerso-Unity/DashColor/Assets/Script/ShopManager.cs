
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public Text nameShopTxt;
    public GameObject sellButton;
    public Transform sellButtonTransform;

    public static ShopManager instance;
    public Animator animator;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple ShopManager");
            return;
        }
        instance = this;        
    }

    public void OpenShop(ItemData[] items, string name){
        nameShopTxt.text = name;
        UpdateItemToSell(items);
        animator.SetBool("Dialogue", true);
    }

    void UpdateItemToSell(ItemData[] items){
        for(int i = 0; i<sellButtonTransform.childCount; i++){
            Destroy(sellButtonTransform.GetChild(i).gameObject);
        }

        for(int i = 0; i<items.Length; i++){
            GameObject tmpButton = Instantiate(sellButton, sellButtonTransform);
            sellButtonItem buttonScript = tmpButton.GetComponent<sellButtonItem>();
            buttonScript.nameItem.text = items[i].itemName;
            buttonScript.Graphics.sprite = items[i].image;
            buttonScript.prix.text = items[i].prix.ToString();
            buttonScript.item = items[i];
        }
    }

    

    public void closeShop(){
        animator.SetBool("Dialogue", false);
    }
}
