
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public GameObject settingWindow;

    public GameObject audioManager;

    private void Start() {
        DontDestroyOnLoad(audioManager);
    }

    public void startGame(){
        SceneManager.LoadScene("SelectLvl");
    }

    public void settingGame(){
        settingWindow.SetActive(true);
    }

    public void closeSettingGame(){
        settingWindow.SetActive(false);
    }

    public void startCredit(){
        SceneManager.LoadScene("Credit");
    }

    public void quitGame(){
        Application.Quit();
        
    }
}

//TODO :
// code sur une porte pour deverouiller
// porte de tp dans une meme scene (script avec 1 GameObject si onTriggerEnter1 alors tp a ce gameObject), 1 script par porte pour tp aller retour
// Different type d'ennemi
// un niveau de tuto plus long avec chaque type de chose montrer
// Canvas inventaire avec un stock des potion/item qu'on a (pouvoir selectioner un item pour voir ca description a droite et un bouton Utiliser)
// pouvoir mettre une clef dans son inventaire (clef trouvable dans coffre)
// item :   boostJump (potion ou ressort utilisable sur 1 saut)
//          Invisible potion (pas detectable par ennemi)
//          clef 

//Pouvoir dash en avant (effet flamme sur une trajectoire)
//Pouvoir sauter entre 2 mur (plus de mobilité / reactivité dans le jeux)
//Mettre une partie de niveau dans une grotte /avoir un cercle de lumiere autour de soit.
