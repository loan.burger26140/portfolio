
using System;
using System.Linq;
using UnityEngine;

public class LoadAndSaveData : MonoBehaviour
{

    public static LoadAndSaveData instance;

    private void Awake(){
        if(instance != null){
            Debug.LogWarning("Multiple LoadAndSaveData");
            return;
        }
        instance = this;

    }

    private void Start() {
        Inventory.instance.coinsCount = PlayerPrefs.GetInt("Coin",0);
        Inventory.instance.updateText(); 

        string[] itemSaved = PlayerPrefs.GetString("itemInInventory", "").Split(',');
        for (int i = 0; i < itemSaved.Length; i++)
        {
            if(itemSaved[i] != ""){
                int id = int.Parse(itemSaved[i]);
                ItemData currentItem = itemDataBase.instance.allItem.Single(x=> x.id == id);
                Inventory.instance.content.Add(currentItem);
            }
        }
        Inventory.instance.updateInventoryUI();
    }

    public void saveData(){
        PlayerPrefs.SetInt("Coin", Inventory.instance.coinsCount);
        if(CurrentSceneManager.instance.levelToUnlock > PlayerPrefs.GetInt("Level",1)){
            PlayerPrefs.SetInt("Level", CurrentSceneManager.instance.levelToUnlock);
        }

        string itemInInventory = string.Join(",", Inventory.instance.content.Select(x=> x.id));
        PlayerPrefs.SetString("itemInInventory", itemInInventory);


    }


}
