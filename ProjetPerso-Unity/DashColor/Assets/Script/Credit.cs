
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credit : MonoBehaviour
{

    public void loadMainMenu(){

        SceneManager.MoveGameObjectToScene(GameObject.FindGameObjectWithTag("AudioManager"), SceneManager.GetActiveScene());
        SceneManager.LoadScene("MainMenu");
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape)){
            loadMainMenu();
        }
    }

}
