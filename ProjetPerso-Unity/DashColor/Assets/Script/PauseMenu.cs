using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool isPaused = false;
    public GameObject pauseMenuUI;
    public GameObject settingWindow;

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape)){
            if(isPaused){
                resume();
            }else{
                pause();
            }
        }
        
    }

    private void pause(){
        PlayerMovement.instance.enabled = false;
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        isPaused = true;
    }

    public void resume(){
        PlayerMovement.instance.enabled = true;
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1;
        isPaused = false;
        settingWindow.SetActive(false);
    }

    public void mainMenuButton(){
        resume();
        SceneManager.MoveGameObjectToScene(GameObject.FindGameObjectWithTag("AudioManager"), SceneManager.GetActiveScene());
        SceneManager.LoadScene("MainMenu");
    }

    public void settingButton(){
        settingWindow.SetActive(true);
    }

    public void closeSettingGame(){
        settingWindow.SetActive(false);
    }
}


