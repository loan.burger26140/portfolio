
using UnityEngine;
using UnityEngine.UI;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    private bool isInRange;

    private Text interactUI;
    private bool inDialogue = false;

    private void Awake() {
        interactUI = GameObject.FindGameObjectWithTag("InteractUI").GetComponent<Text>();
    }

    void Update()
    {
        if(isInRange && Input.GetKeyDown(KeyCode.E)){
            interactUI.enabled = false;
            if(inDialogue){ 
                bool haveSuite = DialogueManager.instance.animator.GetBool("Dialogue");
                if(haveSuite){
                    DialogueManager.instance.AfficheNextSentence();
                }
            }else{
                inDialogue = true;
                DialogueManager.instance.startDialogue(dialogue);
            }
            //triggerDialogue();
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("Player")){
            isInRange = true;
            interactUI.enabled = true;
        }
        
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("Player")){
            isInRange = false;
            inDialogue = false;
            interactUI.enabled = false;
            DialogueManager.instance.FinDialogue();

        }
    }
}
