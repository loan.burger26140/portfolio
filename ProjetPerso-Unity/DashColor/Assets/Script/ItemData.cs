using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Inventory/Item")]
public class ItemData : ScriptableObject
{
    public int id;
    public string itemName;
    public string description;
    public Sprite image;
    public int heal;
    public int boostSpeed;
    public float speedDuration;
    public bool isLighting;
    public int prix;

}
