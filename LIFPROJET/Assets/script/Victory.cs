using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Victory : MonoBehaviour
{
    private GameObject[] enemies;
    private int sizeOfEnemies;
    public bool niv1;
    private bool niv2;
    public GameObject[] toDestroy;
    private AudioClip bruitDecouv; 
    private AudioSource audioDecouv;

    // Start is called before the first frame update
    void Start()
    {
        audioDecouv = GameObject.Find("decouverte").GetComponent<AudioSource>();
        bruitDecouv = GameObject.Find("decouverte").GetComponent<AudioSource>().clip;
        niv1 = false; 
        niv2 = false; 
    }

    // Update is called once per frame
    void Update()
    {
        enemies = GameObject.FindGameObjectsWithTag("ennemi");
        sizeOfEnemies = enemies.Length;

        if (sizeOfEnemies == 0){
            if (niv1){
                niv2 = true;
            } 
            foreach(GameObject objet in toDestroy){
                Destroy(objet);
            }
            audioDecouv.PlayOneShot(bruitDecouv);
            niv1 = true;
            if(niv1 && !niv2)
                GameObject.Find("Terrain").GetComponent<Generation>().passageniv2();
            
            if (niv1 && niv2){
                Debug.Log("VICTORY");
                audioDecouv.PlayOneShot(bruitDecouv);
                GameObject[] piece;
                piece = GameObject.FindGameObjectsWithTag("coin");
                foreach(GameObject objet in piece){
                    objet.SetActive(true);
                }
                
            }
        }
    }
}
