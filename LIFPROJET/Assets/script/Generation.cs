using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generation : MonoBehaviour
{
    public GameObject mana;
    public GameObject vie;
    public GameObject[] emplacementItem;
    public GameObject[] emplacementItem2;
    private int nbItem= 8;

    public GameObject ennemi;
    public GameObject[] emplacementEnnemi;
    public GameObject[] emplacementEnnemi2;
    private int nbEnnemi = 3;
    public Transform pran;


    
    // Start is called before the first frame update
    void Start()
    {  
        foreach(GameObject objet in emplacementItem){
            objet.SetActive(false);
        }
        foreach(GameObject objet in emplacementEnnemi){
            objet.SetActive(false);
        }
        foreach(GameObject objet in emplacementItem2){
            objet.SetActive(false);
        }
        foreach(GameObject objet in emplacementEnnemi2){
            objet.SetActive(false);
        }
        spawn(emplacementEnnemi,emplacementItem);
        
        
    }

    public void passageniv2(){
        spawn(emplacementEnnemi2,emplacementItem2);
        nbEnnemi = 6;
    }

    public void spawn(GameObject[] Ennemi, GameObject[] Item){
        spawnEnnemi(Ennemi,nbEnnemi);
        spawnItem(Item);
        
    }

    private void spawnEnnemi(GameObject[] Ennemi,int x){
        int randomPos;
        for (int i = 0 ; i < nbEnnemi ; i++){
            randomPos= Random.Range(0, Ennemi.Length);
            spawnIndividualEnnemi(Ennemi[randomPos].transform);
        }
    }


    private void spawnItem( GameObject[] Item){
        int randomObject;
        int randomPos;
        for (int i = 0 ; i < nbItem ; i++){
            randomObject = Random.Range(1, 3);   // 1 = Vie  2= Mana
            randomPos= Random.Range(0, Item.Length);
            if(randomObject == 1){
                spawnVie(Item[randomPos].transform);
            }
            if(randomObject == 2){
                spawnMana(Item[randomPos].transform);
            }
        }
    }
    
    public void spawnIndividualEnnemi(Transform t){
        GameObject ter = GameObject.Find("Terrain");
        Transform terTransform = ter.GetComponent<Transform>(); 
        GameObject v = Instantiate(ennemi,t.position,Quaternion.identity, terTransform) as GameObject;
    }

    public void spawnVie(Transform t){
        GameObject ter = GameObject.Find("Terrain");
        Transform terTransform = ter.GetComponent<Transform>(); 
        GameObject v = Instantiate(vie,t.position,Quaternion.identity, terTransform) as GameObject;
    }
    public void spawnMana(Transform t){
        GameObject ter = GameObject.Find("Terrain");
        Transform terTransform = ter.GetComponent<Transform>(); 
        GameObject v = Instantiate(mana,t.position,Quaternion.identity, terTransform) as GameObject;
    }
}
