using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moovBras : MonoBehaviour
{   
    private float rotationX = 0f;
    private float rotationY = 0f;
    public float sensitivity= 5f;
    
    // bloquer la rotation a un certain angle
    void lockedRotation()
    {
        if(Input.GetKey(KeyCode.LeftShift)){
            rotationX += Input.GetAxis("Mouse X") * sensitivity;
            rotationX = Mathf.Clamp (rotationX, -60, 60);
            rotationY += Input.GetAxis("Mouse Y") * sensitivity;
            rotationY = Mathf.Clamp (rotationY, -60, 60);
            if(gameObject.tag == "orange"){
                transform.localEulerAngles = new Vector3(-rotationY, 0 , rotationX);
            }
            if (gameObject.tag == "jolie"){
                transform.localEulerAngles = new Vector3(0,rotationY , rotationX);
            } 
        }
        if(Input.GetKeyUp(KeyCode.LeftShift)) {
            rotationX = 0;
            rotationY = 0;
            transform.localEulerAngles = new Vector3(0  , 0, 0);
        }
        
    }

    void viser(){
        if(Input.GetKeyDown(KeyCode.Mouse1)){
            GameObject.Find("ViseCamera").GetComponent<Camera>().depth = 1;
        }
        if(Input.GetKeyUp(KeyCode.Mouse1)){
            GameObject.Find("ViseCamera").GetComponent<Camera>().depth =-2;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameObject.Find("Scirpt4Button").GetComponent<Bouton_click_modify>().isPaused){
            lockedRotation(); 
            viser();
        }
    }
}
