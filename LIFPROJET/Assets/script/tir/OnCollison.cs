    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollison : MonoBehaviour
{
    public GameObject toile;
    public Material[] tabMaterial;
    public GameObject[] tabCol = new GameObject[3];
    public int[] tabCoil = new int[3];
   //private int indice = 0;
    public int mode;
    
    // Start is called before the first frame update
    void Start()
    {
       mode = GameObject.Find("eject").GetComponent<Tir>().modeTir;
    }
    // Update is called once per frame
    void Update(){}

    private void OnCollisionEnter(Collision col) {

        Debug.Log("Collision avec " + col.gameObject.tag);
        if (mode == 1){
            toile.GetComponent<Renderer>().material = tabMaterial[0];
            afficheImpact(col);
            faitDegat(col,50);
        }
        if (mode == 2){
            toile.GetComponent<Renderer>().material = tabMaterial[1];
            afficheImpact(col);
        }

        
    }

    private void afficheImpact(Collision col){
        GameObject impact = Instantiate(toile,transform.position,col.gameObject.transform.rotation) as GameObject;
        //tabCol[indice] = impact;
        //tabCoil[indice] = indice;
        //indice ++;
        Destroy(gameObject,0f);
        Destroy(impact,5f);

    }

    private void faitDegat(Collision col, int x){
        if(col.gameObject.tag == "ennemi"){
            vieEnemi e = col.gameObject.transform.GetComponent<vieEnemi>();
            e.degat(x);
        }
    }
}
