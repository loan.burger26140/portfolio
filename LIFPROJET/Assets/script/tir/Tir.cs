using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tir : MonoBehaviour
{
    public GameObject projectile; 
    private AudioClip bruitTir; 
    private AudioSource audioTir;
    public int modeTir = 1;
    private int force = 10;
    private float puissance=1;
    private float time;
    private float timeEvent;
    
    // Start is called before the first frame update
    void Start()
    {
        audioTir = GameObject.Find("Tir").GetComponent<AudioSource>();
        bruitTir = GameObject.Find("Tir").GetComponent<AudioSource>().clip;
    }

    // Update is called once per frame
    void Update()
    {
        if(verify()){
            if(Input.GetKeyDown(KeyCode.Mouse0)){
                time = Time.time;
            }
            if(Input.GetKeyUp(KeyCode.Mouse0)){
                timeEvent = Time.time - time;
                if (timeEvent>2)
                    puissance = 2;
                else
                    puissance = timeEvent;
                tir();
            }
        } 
    }

    private void tir(){
        if(modeTir == 1 || modeTir == 2){
            GameObject boule = Instantiate(projectile,transform.position,Quaternion.identity) as GameObject;
            if (puissance ==2)
                boule.GetComponent<Rigidbody>().useGravity = false;    
            audioTir.PlayOneShot(bruitTir);
            GameObject.Find("Spider").GetComponent<ATHvieMana>().perdMana(10);
            boule.GetComponent<Rigidbody>().velocity = transform.TransformDirection(Vector3.left) * force * puissance;
            Destroy(boule,4f);  
        }
        if (modeTir == 3){
            if(gameObject.tag == "orange"){
                GameObject.Find("Spider").GetComponent<Grappin>().Grapple();
            }
            if (gameObject.tag == "jolie"){
                GameObject.Find("Spider").GetComponent<GrappinNoRigidBody>().Grapple();
            } 
            
        }

    }

    private bool verify(){
        return !GameObject.Find("Scirpt4Button").GetComponent<Bouton_click_modify>().isPaused && 
                GameObject.Find("Spider").GetComponent<ATHvieMana>().mana>=10 &&
                Input.GetKey(KeyCode.LeftShift);

    }

    
   

     



}
