using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappin : MonoBehaviour
{

    public Camera cam;
	public RaycastHit hit;

    public LayerMask surfaces;
	public int maxDistance;
	
	public bool isMoving;
	public Vector3 location;
	
	public float speed = 10;
	public Transform hook;
	
	
	public LineRenderer LR;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isMoving)
			MoveToSpot();
        // Annulation / décrochage du grappin
		if(Input.GetKey(KeyCode.Space) && isMoving){
            isMoving = false;
			LR.enabled = false;
            gameObject.GetComponent< Rigidbody>().useGravity = true;  
        } 
    }

    // Lors de l'envois du grappin
	public void Grapple(){
        // On créé un raycast de "maxDistance" unités depuis la caméra vers l'avant.
        // Si ce raycast touche quelque chose c'est que la grappin est utilisable
		if(Physics.Raycast(cam.transform.position, cam.transform.TransformDirection(Vector3.forward), out hit, maxDistance, surfaces)){
            isMoving = true;
			location = hit.point;
            gameObject.GetComponent< Rigidbody>().useGravity = false;
			LR.enabled = true;
			LR.SetPosition(1, location);
		}
    }

    public void MoveToSpot(){
		transform.position = Vector3.Lerp(transform.position, location, speed * Time.deltaTime / Vector3.Distance(transform.position, location));
		LR.SetPosition(0, hook.position);
        // Si on est à  - de 1 unité(s) de la cible final on décroche le grappin automatiquement
		if(Vector3.Distance(transform.position, location) < 1f){
            isMoving = false;
			LR.enabled = false;
            gameObject.GetComponent< Rigidbody>().useGravity = true;
        }
	}

}
