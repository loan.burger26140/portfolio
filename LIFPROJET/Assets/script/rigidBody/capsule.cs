using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class capsule : MonoBehaviour
{
    private float speed = 7.0f;

    private float rotateSpeed = 12.0f;

    private float jumpSpeed = 8.0f;

    public float gravity; //= 20.0f;

    private Vector3 moveD = Vector3.zero;

    private Rigidbody _rigidbody;



    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        
        
    }

    // Update is called once per frame
    void Update()
    {   
        if(!GameObject.Find("Scirpt4Button").GetComponent<Bouton_click_modify>().isPaused){
            
            moveD = new Vector3(Input.GetAxis("Vertical"),0,0);
            moveD = - transform.TransformDirection(moveD);
            moveD *= speed;
            _rigidbody.AddForce(moveD*Time.deltaTime);

            if(Input.GetButton("Jump")){
                moveD.y = jumpSpeed;
                _rigidbody.AddForce(moveD*Time.deltaTime);
            }
            
            moveD.y -= gravity * Time.deltaTime;
            transform.Rotate(Vector3.up * Input.GetAxis("Horizontal")*Time.deltaTime * rotateSpeed * 10);
            
            _rigidbody.AddForce(moveD*Time.deltaTime);
        }





    }

   
}
