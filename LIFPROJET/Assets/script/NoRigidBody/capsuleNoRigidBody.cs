using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class capsuleNoRigidBody : MonoBehaviour
{
    private float speed = 7.0f;

    private float rotateSpeed = 12.0f;

    private float jumpSpeed = 8.0f;

    public float gravity;

    private Vector3 moveD = Vector3.zero;

    CharacterController cac;



    // Start is called before the first frame update
    void Start()
    {
        cac = GetComponent<CharacterController>();

        
        
    }

    // Update is called once per frame
    void Update()
    {   
        if(!GameObject.Find("Scirpt4Button").GetComponent<Bouton_click_modify>().isPaused){
            if (cac.isGrounded){
                moveD = new Vector3(Input.GetAxis("Vertical"),0,0);
                moveD = - transform.TransformDirection(moveD);
                moveD *= speed;

                if(Input.GetButton("Jump")){
                    moveD.y = jumpSpeed;
                }
            }
            moveD.y -= gravity * Time.deltaTime;
            transform.Rotate(Vector3.up * Input.GetAxis("Horizontal")*Time.deltaTime * rotateSpeed * 10);
            
            cac.Move(moveD*Time.deltaTime);
        }





    }

   
}
