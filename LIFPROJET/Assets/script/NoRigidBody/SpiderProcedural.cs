using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderProcedural : MonoBehaviour
{
    public float legMoveSpeed = 7f;
    public float moveDistance = 2f;
    public float moveStoppingDistance = 0.4f;

    public Transform[] legTargets;
    public Transform[] legAim;
    public int[] oppositeLeg;

    private Vector3[] legPos;
    Vector3 originalPosition;

    private int nbLegs;
    private float[] distToAim;
    private bool[] legMoving;

    // Start is called before the first frame update
    void Start()
    {
        nbLegs = legTargets.Length;

        legMoving = new bool[nbLegs];
        distToAim = new float[nbLegs];
        legPos = new Vector3[nbLegs];
    
        for (int i = 0; i < nbLegs; i++)
        {
            legPos[i] = legTargets[i].position;
            legMoving[i] = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for (int i = 0; i < nbLegs; ++i)
        {
            // Code de l'araignée qui bouge pas
                /* legTargets[i].position = lastLegPos[i];
                lastLegPos[i] = legTargets[i].position;
                legMoving[i] = true; */ 

            distToAim[i] = Vector3.Distance(legTargets[i].position, legAim[i].position);
            if( (distToAim[i] >= moveDistance && !isItMoving(oppositeLeg[i])) || legMoving[i]) {
                legMoving[i] = true;
                legTargets[i].position = Vector3.Lerp(legTargets[i].position, legAim[i].position + new Vector3(0f, 0.3f, 0f), Time.deltaTime * legMoveSpeed);
                legPos[i] = legTargets[i].position;   

                if(distToAim[i] < moveStoppingDistance)
                {
                    legMoving[i] = false;
                }
            } else {
                legTargets[i].position = Vector3.Lerp(legTargets[i].position, legPos[i] + new Vector3(0f, -0.4f, 0f), Time.deltaTime * legMoveSpeed * 3f);
                legMoving[i] = false;
            }
        }
    }

    public bool isItMoving(int i) {
        return legMoving[i];
    }
}
