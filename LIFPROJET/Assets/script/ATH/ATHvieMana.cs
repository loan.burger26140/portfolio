using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ATHvieMana : MonoBehaviour
{
    private AudioClip bruitRamasse; 
    private AudioSource audioRamasse;
     private AudioClip bruitCoin; 
    private AudioSource audioCoin;

    private float healt = 60.0f;
    private float maxHealt = 100.0f;
    public Image barvieImage;
    public Text txtvie;
    public float mana = 60.0f;
    private float maxMana = 100.0f;
    public Image barmanaImage;
    public Text txtmana;
    // Start is called before the first frame update
    void Start()
    {
        audioRamasse = GameObject.Find("Collect").GetComponent<AudioSource>();
        bruitRamasse = GameObject.Find("Collect").GetComponent<AudioSource>().clip;
        audioCoin = GameObject.Find("coinB").GetComponent<AudioSource>();
        bruitCoin = GameObject.Find("coinB").GetComponent<AudioSource>().clip;
    }

    // Update is called once per frame
    void Update()
    {

        barvieImage.fillAmount = healt/maxHealt;
        txtvie.text = (barvieImage.fillAmount *100).ToString("00") + "% healt";
        barmanaImage.fillAmount = mana/maxMana;
        txtmana.text = (barmanaImage.fillAmount *100).ToString("00") + "% mana";
        
    }

     private void regeneVie(){
        healt += 20;
    }

    private void regeneMana(){
        mana += 20;
    }

    public void perdMana(int x){
        mana -= x;
    }

    public void perdVie(int x){
        healt -= x;
        if (healt<=0){
            GameObject.Find("Scirpt4Button").GetComponent<Bouton_click_modify>().activMort();
        }
    }

    void OnTriggerEnter(Collider other) {
        if(other.transform.tag == "Vie"){
            if(healt<=maxHealt - 20){
                audioRamasse.PlayOneShot(bruitRamasse);
                regeneVie();
                Destroy(other.gameObject);
            }
        }

        if(other.transform.tag == "Mana"){
            if(mana<=maxMana - 20){
                audioRamasse.PlayOneShot(bruitRamasse);
                regeneMana();
                Destroy(other.gameObject);
            }
        }

        if(other.transform.tag == "coin"){
            audioCoin.PlayOneShot(bruitCoin);
            Destroy(other.gameObject);
        }
        if(other.transform.tag == "tresor"){
            audioCoin.PlayOneShot(bruitCoin);
            Destroy(other.gameObject);
            GameObject.Find("Scirpt4Button").GetComponent<Bouton_click_modify>().activWin();

        }
        

    }
}
