using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Bouton_click_modify : MonoBehaviour
{
 /////////////////////////////////////////////////////////////////tirMenu/////////////////////////////////////////////////////////////////////////////////////////////////   
 // Event click pour choisir le mode de tir  ( 1 : Tir direct sur l'ennemi pour le stall/kill   2 : un tir sur le mur puis sur l'ennemi pour stall 5s  3 : 3 tir sur le mur pour crée une toile)
    public GameObject menuATHUI;
    public void ClickMod1(){
        GameObject.Find("eject").GetComponent<Tir>().modeTir = 1;
    }

    public void ClickMod2(){
        GameObject.Find("eject").GetComponent<Tir>().modeTir = 2;
    }

    public void ClickMod3(){
        GameObject.Find("eject").GetComponent<Tir>().modeTir = 3;
    }




    /////////////////////////////////////////////////////////pauseMenu///////////////////////////////////////////////////////////////////////////
    public bool isPaused = false;
    public GameObject menuPauseUI;  

    public void quitPause(){
        menuPauseUI.SetActive(false);
        menuATHUI.SetActive(true);
        Time.timeScale = 1f;
        isPaused = false;
    }

    public void joinPause(){
        menuATHUI.SetActive(false);
        menuPauseUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void loadMenu(){
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }

    public void quit(){
        Debug.Log("Vous avez quitté le jeu ...");
        Application.Quit();
    }
/////////////////////////////////////////////////////////DeathMenu///////////////////////////////////////////////////////////////////////////

    public GameObject DeathUI;  
    public void activMort(){
        menuATHUI.SetActive(false);
        DeathUI.SetActive(true);
        
    }

    /////////////////////////////////////////////////////////DeathMenu///////////////////////////////////////////////////////////////////////////

    public GameObject WinUI;  
    public void activWin(){
        menuATHUI.SetActive(false);
        WinUI.SetActive(true);
        
    }

///////////////////////////////////////////////////////////////////////////////////barre de slide volume///////////////////////////////////////////////////////////////

    public AudioSource audiosource;
    public Slider slider;
    public Text txtvol;

    public void SliderChange(){
        audiosource.volume = slider.value;  
        txtvol.text = "Volume " + (audiosource.volume *100).ToString("00") + "%"; 

    }
    





    //////////////////////////////////////////////////////////////update///////////////////////////////////////////////////////////////////////////////
    void Start() {
        SliderChange();
    }
    
    void Update() {

        if(Input.GetKeyDown(KeyCode.Escape)){
            if(isPaused){
                quitPause();
            }else{
                joinPause();
            }
        }

        if(!isPaused){
            if(Input.GetKey(KeyCode.Alpha1)){
                ClickMod1();
                GameObject.Find("Scirpt4Button").GetComponent<bouton_click_image>().ClickMod1();
            }

            if(Input.GetKey(KeyCode.Alpha2)){
                ClickMod2();
                GameObject.Find("Scirpt4Button").GetComponent<bouton_click_image>().ClickMod2();
            }

            if(Input.GetKey(KeyCode.Alpha3)){
                ClickMod3();
                GameObject.Find("Scirpt4Button").GetComponent<bouton_click_image>().ClickMod3();
            }
        }



    }

}
