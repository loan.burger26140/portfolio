using UnityEngine;
using System.Collections;

public class Chase : MonoBehaviour
{

    public Transform player;

    // Use this for initialization
    void start()
    {

    }

    // Update is called once per frame
    void update()
    {

        if (Vector3.Distance(player.position, this.transform.position) < 10)
        {
            Vector3 direction = player.position - this.transform.position;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                Quaternion.LookRotation(direction), 0.1f);
        }
    }
}