using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyAI : MonoBehaviour
{

    private NavMeshAgent agent = null;
    [SerializeField] private Transform target;

    private void Start()
    {
        GetReferences();
    }
    
    private void GetReferences()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        MoveToTarget();
    }
    
    private void MoveToTarget()
    {
        agent.SetDestination(target.position);
    }

}
