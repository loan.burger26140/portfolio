using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour
{
    public float radius = 1;
    

    private void OnCollisionEnter(Collision _collision)
    {
 
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach(Collider nearbyObject in colliders)
        {
            if(nearbyObject.tag == "Player")
            {
                GameObject.Find("Spider").GetComponent<ATHvieMana>().perdVie(5);
            }
        }
        Destroy(gameObject);
    }
}
