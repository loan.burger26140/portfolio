using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderController : MonoBehaviour
{
    public float _speed = 1f;
    private float rotateSpeed = 5f;

    private Rigidbody _rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    private void FixedUpdate() {

        if(_rigidbody.velocity.magnitude < _speed)
        {
            float valueY = Input.GetAxis("Vertical");
            if(valueY != 0){
                // _rigidbody.AddForce(0f, 0f, value * Time.fixedDeltaTime * 1000f);
                transform.position += transform.forward * valueY * _speed * Time.fixedDeltaTime;
            }

            /* float valueX = Input.GetAxis("Horizontal");
            if(valueX != 0){
                // _rigidbody.AddForce(value * Time.fixedDeltaTime * 1000f, 0f, 0f);*
                transform.position += Vector3.Cross(transform.up, transform.forward) * valueX * _speed * Time.fixedDeltaTime;
            } */

            // Bien mais rotation pas autour de l'araignée.
            transform.Rotate(Vector3.up * Input.GetAxis("Horizontal") * Time.deltaTime * rotateSpeed * 10);
        }
    }
}
