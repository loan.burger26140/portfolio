using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{
    int layerMask;

    // Start is called before the first frame update
    void Start()
    {
        layerMask = LayerMask.GetMask("Ground");
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position + new Vector3(0f, .75f, 0f), Vector3.down);

        if(Physics.Raycast(ray, out hit, layerMask))
        {
            transform.position = hit.point;
        }
    } 
}
