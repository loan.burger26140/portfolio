using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcTest : MonoBehaviour
{
    public Transform[] legTarget;
    public GameObject[] legAim;
    public int[] oppositeLeg;

    public Transform bodyPos;
    public float moveDuration = 0.1f;
    public float wantStepAtDistance = 2f;

    private Vector3[] legPos;
    private bool[] legMoving;
    public bool bodyOrientation = true;

    private int nbLegs;
    private int layerMask;
    private Vector3 lastBodyPos;
    private Vector3 lastBodyUp;

    // Start is called before the first frame update
    void Start()
    {
        lastBodyUp = bodyPos.up;

        nbLegs = legTarget.Length;
        legPos = new Vector3[nbLegs];
        legMoving = new bool[nbLegs];

        for (int i = 0; i < nbLegs; i++)
        {
            legPos[i] = legTarget[i].position;
            legAim[i].transform.position = legPos[i];
            legMoving[i] = false;
        } 
        lastBodyPos = bodyPos.position;
    }

    IEnumerator nextStep(int index)
    {
        // Indicate we're legmoving (used later)
        legMoving[index] = true;

        // Store the initial conditions
        // Quaternion startRot =legPos[index].rotation;
        Vector3 startPoint = legPos[index];

        // Quaternion endRot = legAim[index].transform.rotation;
        Vector3 endPoint = legAim[index].transform.position;

        // Time since step started
        float timeElapsed = 0;

        // We want to pass through the center point
        Vector3 centerPoint = (startPoint + endPoint) / 2;
        // But also lift off, so we move it up by half the step distance (arbitrarily)
        centerPoint += legAim[index].transform.up * Vector3.Distance(startPoint, endPoint) / 2f;

        // Here we use a do-while loop so the normalized time goes past 1.0 on the last iteration,
        // placing us at the end position before ending.
        do
        {
            // Add time since last frame to the time elapsed
            timeElapsed += Time.deltaTime;

            float normalizedTime = timeElapsed / moveDuration;
            
            // Interpolate position and rotation
            legPos[index] = Vector3.Lerp(
                Vector3.Lerp(startPoint, centerPoint, normalizedTime), 
                Vector3.Lerp(centerPoint, endPoint, normalizedTime),
                normalizedTime
                );

            // Wait for one frame
            yield return null;
        }
        while (timeElapsed < moveDuration);


        // Done legmoving
        legMoving[index] = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for (int i = 0; i < nbLegs; i++)
        {
            legTarget[i].position = legPos[i];
            legPos[i] = legTarget[i].position;

            float dist = Vector3.Distance(legTarget[i].position, legAim[i].transform.position);

            if ((dist > wantStepAtDistance) && !isItMoving(oppositeLeg[i]))
            {
                StartCoroutine(nextStep(i));
            } 
        }

        

        lastBodyPos = bodyPos.position;

        if (nbLegs == 4)
        {
            Vector3 v1 = legTarget[0].position - legTarget[1].position;
            Vector3 v2 = legTarget[2].position - legTarget[3].position;
            Vector3 normal = Vector3.Cross(v1, v2).normalized;
            Vector3 up = Vector3.Lerp(lastBodyUp, normal, 1f / 9f);
            bodyPos.up = up;
            bodyPos.rotation = Quaternion.LookRotation(bodyPos.parent.forward, up);
            lastBodyUp = bodyPos.up; 
            // Debug.Log("normal :      " + normal  + " |    lastBodyUp :     " + lastBodyUp) ;
        } 

    }


    private void OnDrawGizmosSelected() 
    {
        for (int i = 0; i < nbLegs; i++)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(legTarget[i].position, 1f);

            Gizmos.color = Color.red;
            Gizmos.DrawLine(legTarget[i].position, legAim[i].transform.position);

            Gizmos.color = Color.white;
            Gizmos.DrawLine(bodyPos.position, bodyPos.position + new Vector3(0, 10f, 0));
            

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(legAim[0].transform.position, legAim[1].transform.position);
            Gizmos.DrawLine(legAim[2].transform.position, legAim[3].transform.position);
        
        }
    }

    public bool isItMoving(int i) {
        return legMoving[i];
    }
}
