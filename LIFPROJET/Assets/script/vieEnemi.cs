using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class vieEnemi : MonoBehaviour
{

    private float vieEnnemi = 100;
    private float MAXvieEnnemi = 100;
    public int unChanceSur;
    public Image HealtbarEnnemi;
    public GameObject canvasEnnemi;

    private void Update(){
        HealtbarEnnemi.fillAmount = vieEnnemi/MAXvieEnnemi;
    }

    public void degat(int x){
        vieEnnemi -= x;
    
        if (vieEnnemi <= 0){
            mort();
        }
        canvasEnnemi.SetActive(true);
        Invoke("desacVie", 3.0f);
        
    }

    private void desacVie(){
        canvasEnnemi.SetActive(false);

    }

    private void mort(){
        Destroy(this.gameObject);
        int randomChance;
        int randomItem;
        Transform t;
        float f;
        t = transform; 
        f = t.position.y;
        t.position = new Vector3(transform.position.x, transform.position.y , transform.position.z);
        randomChance= Random.Range(1, unChanceSur+1);
        if(randomChance == 1){
            randomItem= Random.Range(1, 3);
            if(randomItem == 1)
                GameObject.Find("Terrain").GetComponent<Generation>().spawnVie(t);
            if(randomItem == 2)
                GameObject.Find("Terrain").GetComponent<Generation>().spawnMana(t);
        }



    }
}
