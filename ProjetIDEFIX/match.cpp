#include "ev3dev.h"

#include <thread>
#include <chrono>
#include <iostream>
#include <fstream>

using namespace std;
using namespace ev3dev;

class control
{
public:
  control();
  ~control();

  void drive(int speed, int time=0);
  void turn(int direction);
  void stop();
  void reset();

  bool initialized() const;

  void tir();
  void comportement();

  void terminate() { _terminate = true; }

protected:

  enum state
  {
    state_idle,
    state_driving,
    state_turning
  };

  large_motor     _motor_piston;
  large_motor     _motor_left;
  large_motor     _motor_right;
  infrared_sensor _sensor_ir;
  
  state _state;
  bool  _terminate;
  float distanceSol;
};

// itinialise les moteur et capteur a chaque port,
// et calcul la distance au sol au debut, pour pouvoir comparer plus tard et voir si la balle est devant nous.
control::control() :
  _motor_piston(OUTPUT_A),
  _motor_left(OUTPUT_B),
  _motor_right(OUTPUT_C),
  _sensor_ir(INPUT_1),
  _state(state_idle),
  _terminate(false)
{
  distanceSol = _sensor_ir.value();
}


control::~control()
{
  reset();
}

//deconnecte chaque moteurs
void control::reset()
{
  if (_motor_left.connected())
    _motor_left.reset();

  if (_motor_right.connected())
    _motor_right.reset();

  if (_motor_piston.connected())
    _motor_piston.reset();

  _state = state_idle;
}

//verifie que chaque moteur/capteur est bien connecté au bon port.
bool control::initialized() const
{
  return (_motor_left .connected() &&
          _motor_right.connected() &&
          _motor_piston.connected() &&
          _sensor_ir  .connected());
}

// avance droit devant lui a la vitesse Speed, pendant time 
// si Time = 0 alors run-forever.
void control::drive(int speed, int time)
{
  _motor_left.set_speed_sp(-speed);

  _motor_right.set_speed_sp(-speed);

  _state = state_driving;

  if (time > 0)
  {
    _motor_left .set_time_sp(time).run_timed();
    _motor_right.set_time_sp(time).run_timed();

    while (_motor_left.state().count("running") || _motor_right.state().count("running"))
      this_thread::sleep_for(chrono::milliseconds(10));

    _state = state_idle;
  }
  else
  {
    _motor_left.run_forever();
    _motor_right.run_forever();
  }
}

void control::turn(int direction)
{
  if (_state != state_idle)
    stop();

  if (direction == 0)
    return;

  _state = state_turning;

  _motor_left.set_position_sp(direction).set_speed_sp(500).run_to_rel_pos();
  _motor_right.set_position_sp(-direction).set_speed_sp(500).run_to_rel_pos();

  while (_motor_left.state().count("running") || _motor_right.state().count("running"))
    this_thread::sleep_for(chrono::milliseconds(10));

  _state = state_idle;
}

void control::comportement(){

  _sensor_ir.set_mode(infrared_sensor::mode_ir_prox);
  int distance;
  while (!_terminate)
  {
    distance = _sensor_ir.value();
    if(distanceSol - distance > 2){    
      tir();
    }
    if(distance <= 2){ //stop le programme quand on passe quelque chose tres proche devant le capteur
      terminate();
      break;
    }
  }

}


void control::tir(){
    _motor_piston.set_speed_sp(360).set_time_sp(500).run_timed();
    sound::play("SUIII.wav",true);
}

//
int main()
{
  control c;

  if (c.initialized())
    c.comportement();
  return 0;
}