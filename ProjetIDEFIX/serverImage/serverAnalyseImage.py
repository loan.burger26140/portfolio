from modules.networking import TCPServerAbstraction
from modules.networking import Buffer
from modules.networking import DisconnectedException
from client import *


import numpy as np
import cv2
import threading



def getCroppedImage(image, points_source):
    points_destination = np.float32(
        [[0, 0], [image.shape[1], 0], [image.shape[1], image.shape[0]], [0, image.shape[0]]])

    # Calculer la matrice de perspective
    matrix = cv2.getPerspectiveTransform(points_source, points_destination)

    # Appliquer la transformation de perspective
    image_perspective = cv2.warpPerspective(image, matrix, (image.shape[1], image.shape[0]))

    # Trouver le rectangle englobant les points de destination
    x, y, w, h = cv2.boundingRect(points_destination)

    # Recadrer l'image finale pour enlever les bords noirs
    return image_perspective[y:y + h, x:x + w], matrix

def merge(img1, img2):
    img1 = cv2.resize(src=img1, dsize=(img2.shape[1], img2.shape[0]))

    return np.vstack((img1, img2))



def findCircle(rawImg, matrix, myColor):
    image = cv2.cvtColor(rawImg, cv2.COLOR_BGR2GRAY)
    circles = cv2.HoughCircles(image=image,
                               method=cv2.HOUGH_GRADIENT,
                               dp=1,
                               minDist=5,
                               param1=110,
                               param2=39,
                               maxRadius=70)

    pos = list()

    if circles is not None:
        for co, i in enumerate(circles[0, :], start=1):
            x = int(i[0])
            y = int(i[1])
            r = int(i[2])

            if x != 0 or y != 0:
                myRobot = False
                if myColor[0]*0.9 < rawImg[y, x, 0] < myColor[0]*1.1 and myColor[1]*0.9 < rawImg[y, x, 1] < myColor[1]*1.1 and myColor[2]*0.9 < rawImg[y, x, 2] < myColor[2]*1.1:
                    myRobot = True

                pts = np.array([[x, y]], dtype="float32")
                pts = np.array([pts])

                pts = cv2.perspectiveTransform(pts, matrix)

                pos.append([pts[0][0][0], pts[0][0][1], r, myRobot])

    return pos

def drawCircle(img, circles, offset):

    for c in circles:
        c[1] += offset

        if c[3]:
            color = (0, 255, 0)
        else:
            color = (0, 0, 255)

        cv2.circle(img, (int(c[0]), int(c[1])), c[2], color, 2)

    return img



class Server(TCPServerAbstraction):

    def __init__(self, bufferSize):
        super().__init__(bufferSize)  # a determiner
        self.cropped2110 = None
        self.cropped2120 = None
        self.args2120 = None
        self.args2110 = None
        self.client212O = None
        self.client211O = None
        self.findRobotThread = None
        self.findRobotThreadRunning = False

        self.points_source2110 = np.float32([[69, 21], [538, 16], [546, 453], [85, 468]])
        self.points_source2120 = np.float32([[103, 27], [580, 27], [568, 475], [108, 464]])
        self.myColor = (227, 194, 136)  # (128, 176, 134)

        self.positionRobot = (0, 0)

    def start(self):
        self.listenToClients()
        self.passiveReceive(receiveCallBack)
        self.findRobot()
        print("Server started")

    def findRobot(self):
        self.findRobotThread = threading.Thread(target=self._threadFindRobot)
        self.findRobotThread.start()

    def _threadFindRobot(self):
        self.findRobotThreadRunning = True

        self.connectToCameraServer()

        try:
            self.client211O.start(self.args2110)
            self.client212O.start(self.args2120)
            while self.client211O.connected and self.client212O.connected:
                if self.client211O.frame is not None and self.client212O.frame is not None:

                    self.cropped2120, matrix2120 = getCroppedImage(self.client212O.frame, self.points_source2120)
                    self.cropped2110, matrix2110 = getCroppedImage(self.client211O.frame, self.points_source2110)
                    self.merged = merge(self.cropped2110, self.cropped2120)

                    self.circlePos2110 = findCircle(self.client211O.frame, matrix2110, self.myColor)
                    self.circlePos2120 = findCircle(self.client212O.frame, matrix2120, self.myColor)

                    self.analysePositions()

                    # doesn't work on non-main thread macOS
                    drawCircle(self.merged, self.circlePos2110, 0)
                    drawCircle(self.merged, self.circlePos2120, self.cropped2110.shape[0])
                    #cv2.imshow("terrain", merged)

                    key = cv2.waitKey(1) & 0x0FF

                    if key == ord('x'):
                        break

            self.client211O.stop()
            self.client212O.stop()
        except DisconnectedException:
            print("Plantage d'un serveur et/ou de la connexion")
            self.client211O.stop()
            self.client212O.stop()


        self.findRobotThreadRunning = False


    def analysePositions(self):
        if len(self.circlePos2120)==0 and len(self.circlePos2110)==0:
            return

        tmpX = 0
        tmpY = 0
        tmpRadius = 0

        for pos in self.circlePos2120:
            if pos[2] > tmpRadius:
                tmpRadius = pos[2]
                tmpX = pos[0]
                tmpY = pos[1]

        for pos in self.circlePos2110:
            if pos[2] > tmpRadius:
                tmpRadius = pos[2]
                tmpX = pos[0]
                tmpY = pos[1] + self.cropped2110.shape[0]

        self.positionRobot = (int(tmpX), int(tmpY))

    def connectToCameraServer(self):
        self.client211O = Client()
        self.client212O = Client()

        parser = argparse.ArgumentParser()
        parser.add_argument('-s', '--server', action='store', default='127.0.0.1', type=str,
                            help='address of server to connect')
        parser.add_argument('-p', '--port', action='store', default=12345, type=int, help='port on server')

        self.args2110 = parser.parse_args()
        self.args2110.server = "192.168.1.169"
        self.args2110.port = 2110

        self.args2120 = parser.parse_args()
        self.args2120.server = "192.168.1.169"
        self.args2120.port = 2120


def receiveCallBack(s, b, myServer):
    if b is not None:
        print("Message reçu :", b.displayString())

        x = myServer.positionRobot[0]
        y = myServer.positionRobot[1]

        myServer.sendTo(s, Buffer("{}, {}".format(x, y).encode("utf-8")))


def connexionCallBack(socketClient, addresseClient="null"):
    # do nothing
    return 0


if __name__ == "__main__":
    server = Server(2048)
    server.initialize("192.168.1.162", 32007, connexionCallBack)
    server.start()

