from modules.networking import TCPClientAbstraction, DisconnectedException
from modules.networking import Buffer

class Client(TCPClientAbstraction):

    def __init__(self):
        super().__init__(2048)

    def __del__(self):
        self.finalize()
        #print("Disconnected")

    def start(self, address, port):
        self.initialize(address, port)
        #print("Connected")


    def sendMessage(self):
        msg = "position"
        buffer = Buffer(msg.encode("utf-8"))
        self.send(buffer)


if __name__ == "__main__":
    client = Client()

    client.start("192.168.1.144", 32008)
    client.sendMessage()
    print(client.receive().displayString())

