#include "ev3dev.h"

#define M_PI 3.14159265358979323846
#include <math.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <fstream>

using namespace std;
using namespace ev3dev;

class control
{
public:
  control();
  ~control();

  void MoveRobot(int angle, int Vlinear, int Vangular,int time= 0);
  void stop();
  void reset();

  bool initialized() const;

  int value2angle(int value);
  void tir();
  void comportementGardien();

  void terminate() { _terminate = true; }
protected:

  enum state
  {
    state_idle,
    state_driving,
    state_turning
  };

  large_motor     _motor_face;
  large_motor     _motor_left;
  large_motor     _motor_right;

  i2c_sensor _sensor_irAngle;
  
  state _state;
  bool  _terminate;
  float distanceSol;
  float timePositive;
};

// itinialise les moteur et capteur a chaque port,
// et calcul la distance au sol au debut, pour pouvoir comparer plus tard et voir si la balle est devant nous.
control::control() :
  _motor_face(OUTPUT_C),
  _motor_left(OUTPUT_B),
  _motor_right(OUTPUT_D),
  _sensor_irAngle(INPUT_AUTO),
  _state(state_idle),
  _terminate(false),
  timePositive(0)
{}

control::~control()
{
  reset();
}

//deconnecte chaque moteurs
void control::reset()
{
  if (_motor_left.connected())
    _motor_left.reset();

  if (_motor_right.connected())
    _motor_right.reset();

  if (_motor_face.connected())
    _motor_face.reset();
  

  _state = state_idle;
}

//verifie que chaque moteur/capteur est bien connecté au bon port.
bool control::initialized() const
{
  cout<<_motor_left.connected() << endl; 
  cout<<_motor_right.connected() << endl;
  cout<<_motor_face.connected() << endl;
  cout<<_sensor_irAngle.connected() << endl;

  return (_motor_left .connected() &&
          _motor_right.connected() && 
          _motor_face.connected() &&
          _sensor_irAngle.connected());
}

void control::MoveRobot(int angle, int Vlinear, int Vangular, int time) 
{
  // Calculate the motor speeds
  float cosDegree = cos(angle * M_PI / 180.0);
  float sinDegree = sin(angle * M_PI / 180.0);
  float VwA = Vangular + Vlinear * cosDegree;
  float VwB = Vangular + Vlinear * (-0.5 *cosDegree - 0.866 * sinDegree);
  float VwC = Vangular + Vlinear * (-0.5 *cosDegree + 0.866 * sinDegree);
  _motor_left.set_speed_sp(round(VwC));
  _motor_right.set_speed_sp(round(VwA));
  _motor_face.set_speed_sp(round(VwB));

  _state = state_driving;

  if (time > 0)
  {
    _motor_left .set_time_sp(time).run_timed();
    _motor_right.set_time_sp(time).run_timed();
    _motor_face.set_time_sp(time).run_timed();
    while (_motor_left.state().count("running") || _motor_right.state().count("running") || _motor_face.state().count("running"))
      this_thread::sleep_for(chrono::milliseconds(10));
  
    _state = state_idle;
  }
  else
  {
    _motor_left.run_forever();
    _motor_right.run_forever();
    _motor_face.run_forever();
  }
}

int control::value2angle(int value){
  switch (value)
  {
  case 1:
    return -120;
    break;
  case 2:
    return -90;
    break;
  case 3:
    return -60;
    break;
  case 4:
    return -30;
    break;
  case 5:
    return 0;
    break;
  case 6:
    return 30;
    break;
  case 7:
    return 60;
    break;
  case 8:
    return 90;
    break;
  case 9:
    return 120;
    break;
  case 0:
    return 1222;
    break;
  
  default:
    return 1222;
    break;
  }
}

void control::stop()
{
  _motor_left .stop();
  _motor_right.stop();
  _motor_face.stop();

  _state = state_idle;
}

void control::comportementGardien(){
  bool say;

  while(!_terminate){
    if(_sensor_irAngle.value() != 0){
      say =false;
      if(_sensor_irAngle.value() < 5 && timePositive > -1){
        MoveRobot(0, -700, 0, 150);
        timePositive-= 0.15;
      }else if(_sensor_irAngle.value() > 5 && timePositive < 1) {   //value 2 
        MoveRobot(0, 700, 0, 150);
        timePositive+= 0.15; 
      }
    }else if(_sensor_irAngle.value() == 0 && timePositive!=0){
      if(!say){
        sound::play("save.wav",true);
        say =true;
      }
    }
  }
  stop();
}


int main()
{
  control c;
  if (c.initialized()){
    sound::play("start.wav",true);
    c.comportementGardien();
    //sound::play("save.wav",true);

  }
  return 0;
}
