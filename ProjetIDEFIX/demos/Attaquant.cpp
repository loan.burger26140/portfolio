#include "ev3dev.h"

#define M_PI 3.14159265358979323846
#include <math.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <fstream>

#include <string>
#include <array>
#include <cstdio>

#include <vector>
#include <cstring>
#include "networking/TCPClient.h"
#include "networking/Buffer.h"

using namespace std;
using namespace ev3dev;

class ClientRobot : public networking::TCPClientAbstraction {
public:
    ClientRobot() : TCPClientAbstraction(2048) { }
    ~ClientRobot() { finalize(); }
};

void parseMessage(unsigned char * ptr, int lenght, int & x, int & y){
    char * str = new char[lenght + 1];
    memcpy(str, ptr, lenght);
    str[lenght] = '\0';

    char * token = strtok(str, ",");
    if(token){
        x = atoi(token);
    }

    token = strtok(NULL, ",");
    if(token){
        y = atoi(token);
    }

    delete [] str;
}

class control
{
public:
  control();
  ~control();

  void MoveRobot(int angle, int Vlinear, int Vangular,int time= 0);
  void stop();
  void reset();
  void updateCoord();
  bool isInTerrain();
  void searchBall();

    void shootingPosition();

  bool initialized() const;

  int value2angle(int value);
  void tir();
  void comportement();
  void test_tourne();

  //ClientRobot client;

  void terminate() { _terminate = true; }
protected:

  enum state
  {
    state_idle,
    state_driving,
    state_turning
  };

  large_motor     _motor_piston;
  large_motor     _motor_face; // EN FACE DE LA BRIQUER
  large_motor     _motor_left; // GAUCHE DE LA BRIQUE
  large_motor     _motor_right; // DROITE DE LA BRIQUE
  //infrared_sensor _sensor_irBalle;
  ultrasonic_sensor _sensor_irBalle;
  i2c_sensor _sensor_irDist;
  
  state _state;
  bool  _terminate;
  float distanceSol;
  int coord[2];
};

// itinialise les moteur et capteur a chaque port,
// et calcul la distance au sol au debut, pour pouvoir comparer plus tard et voir si la balle est devant nous.
control::control() :
  _motor_piston(OUTPUT_A),
  _motor_face(OUTPUT_C),
  _motor_left(OUTPUT_B),
  _motor_right(OUTPUT_D),
  _sensor_irBalle(INPUT_AUTO),
  //_sensor_irBalle(INPUT_2),
  _sensor_irDist(INPUT_AUTO),
  _state(state_idle),
  _terminate(false)
{
  distanceSol = _sensor_irBalle.value();
  cout << "distanceSol : " << distanceSol << std::endl;
  coord[0] = 0;
  coord[1] = 0;
  //client.initialize("192.168.1.144", 32008);
}

control::~control()
{
  reset();
}

//deconnecte chaque moteurs
void control::reset()
{
  if (_motor_left.connected())
    _motor_left.reset();

  if (_motor_right.connected())
    _motor_right.reset();

  if (_motor_face.connected())
    _motor_face.reset();
    
  if (_motor_piston.connected())
    _motor_piston.reset();

  _state = state_idle;
}

//verifie que chaque moteur/capteur est bien connecté au bon port.
bool control::initialized() const
{
  cout << "Le moteur piston est connecté : " << _motor_piston.connected() << " - au port " << _motor_piston .address() << endl;
  cout << "Le moteur gauche est connecté : " << _motor_left.connected() << " - au port " << _motor_left.address() << endl;
  cout << "Le moteur droit est connecté : " << _motor_right.connected() << " - au port " <<_motor_right.address() << endl;
  cout << "Le moteur face est conncté : " <<_motor_face.connected() << " - au port "<< _motor_face.address() << endl;
  cout << "Le capteur de distance ultra son est connecté : "<<_sensor_irBalle.connected()<< " - au port "<<_sensor_irBalle.address() << endl;
  cout << "Le capteur infra rouge est connecté : "<<_sensor_irDist.connected()<< " - au port "<< _sensor_irDist.address() << endl;

  return (_motor_piston.connected() &&
          _motor_left .connected() &&
          _motor_right.connected() && 
          _motor_face.connected() &&
          _sensor_irBalle.connected() && 
          _sensor_irDist.connected());
}

void control::MoveRobot(int angle, int Vlinear, int Vangular, int time) 
{
  // Calculate the motor speeds
  float cosDegree = cos(angle * M_PI / 180.0);
  float sinDegree = sin(angle * M_PI / 180.0);
  float VwA = Vangular + Vlinear * cosDegree;
  float VwB = Vangular + Vlinear * (-0.5 *cosDegree - 0.866 * sinDegree);
  float VwC = Vangular + Vlinear * (-0.5 *cosDegree + 0.866 * sinDegree);
  _motor_left.set_speed_sp(round(VwC));
  _motor_right.set_speed_sp(round(VwA));
  _motor_face.set_speed_sp(round(VwB));

  _state = state_driving;

  if (time > 0)
  {
    _motor_left .set_time_sp(time).run_timed();
    _motor_right.set_time_sp(time).run_timed();
    _motor_face.set_time_sp(time).run_timed();
    while (_motor_left.state().count("running") || _motor_right.state().count("running") || _motor_face.state().count("running"))
      this_thread::sleep_for(chrono::milliseconds(10));

    _state = state_idle;
  }
  else
  {
    _motor_left.run_forever();
    _motor_right.run_forever();
    _motor_face.run_forever();
  }
}

void control::test_tourne(){
  _motor_left.set_speed_sp(round(1000));
  _motor_right.set_speed_sp(round(140));
  _motor_face.set_speed_sp(round(70));
  

  _motor_left .set_time_sp(2000).run_timed();
  _motor_right.set_time_sp(2000).run_timed();
  _motor_face.set_time_sp(2000).run_timed();

  while (_motor_left.state().count("running") || _motor_right.state().count("running") || _motor_face.state().count("running"))
      {
        this_thread::sleep_for(chrono::milliseconds(10));
      }

  //terminate();
}

void control::updateCoord(){


  std::string command = "python robot.py";

  std::array<char, 128> buffer;
  std::string result;
  FILE* pipe = popen(command.c_str(), "r");     //_popen pour windows /// popen pour autre 
  if (pipe) {
      while (!feof(pipe)) {
          if (fgets(buffer.data(), 128, pipe) != nullptr) {
              result += buffer.data();
          }
      }
      pclose(pipe); // ferme le pipe
  }

  std::size_t found = result.find(',');
  coord[0] = std::stoi(result.substr(0, found));
  coord[1] = std::stoi(result.substr(found+2, result.size()));
  std::cout << "Coordonnées : " << coord[0] << " " << coord[1] << std::endl;

    /*string msg = "position";
    vector<unsigned char> data(msg.begin(), msg.end());
    networking::Buffer bufferMsg(data);
    client.send(bufferMsg);
    cout << "here" << endl;
    networking::Buffer received = client.receive();
    cout << "here2" << endl;
    int x, y;
    parseMessage(received.data(), received.length(), x, y);
    cout << "X = " << x << " || Y = " << y << endl;*/

   }

int control::value2angle(int value){
  switch (value)
  {
  case 1:
    return -120;
    break;
  case 2:
    return -90;
    break;
  case 3:
    return -60;
    break;
  case 4:
    return -30;
    break;
  case 5:
    return 0;
    break;
  case 6:
    return 30;
    break;
  case 7:
    return 60;
    break;
  case 8:
    return 90;
    break;
  case 9:
    return 120;
    break;
  case 0:
    return 1222;
    break;
  
  default:
    return 1222;
    break;
  }
}

void control::stop()
{
  _motor_piston.stop();
  _motor_left .stop();
  _motor_right.stop();
  _motor_face.stop();

  _state = state_idle;
}

bool control::isInTerrain(){
    updateCoord();
    return (coord[0] > 10 && coord[0] < 950 && coord[1] > 10 && coord[1] < 630);
}

void control::searchBall() {

    if(_sensor_irDist.value() == 0){  //tourne car il trouve pas la balle                  //update les coordonnées du robot 2 fois pour avoir la direction du robot et le replacer au milieu du terrain si il est excentrer et perdu
        MoveRobot(0, 0, 180);
    }
    else{
        MoveRobot(0, 200, value2angle(_sensor_irDist.value())*2);   //avance et se tourne vers la balle
        std::cout << "detecte balle : " << value2angle(_sensor_irDist.value()) << std::endl;
    }
}

void control::shootingPosition() {

    if(abs(distanceSol - _sensor_irBalle.value()) > (distanceSol/4)){  //Si la balle est sous le capteur
        cout << "balle sous le capteur" << endl;
        MoveRobot(0, 0, value2angle(_sensor_irDist.value())*2, 300); // aligne la balle pile en face du robot
        test_tourne();  //avoir les coordonné du robot 2 fois d'affiler pour avoir sa direction -> donne coordonné balle car balle devant le robot -> aligner robot balle cage avant de tirer
        // pour alligner nous suffit de tourner jusqua ce que notre coordonné balle soit sur le vecteur Robot/Cage
        tir();
    }
}

void control::tir(){
    _motor_piston.set_speed_sp(700).set_time_sp(20000).run_timed();
    cout << _motor_piston.max_speed() << endl;
    sound::play("SUIII.wav",true);
}

void control::comportement(){
    int cmpt = 0;
    while(!_terminate){

        // if(cmpt % 10 == 0) {
        //     if(!isInTerrain()) {
        //         cout<<"jsuis dehors"<<endl;
        //         MoveRobot(0, 0, 700, 600); //si il est pas dans le terrain il avance et ce tourne pour rentrer dans le terrain
        //         MoveRobot(0, 700, 0, 2000);
        //     }
        // }

        shootingPosition();

        if (_sensor_irBalle.value() < 7){  //si on passe main devant capteur alors stop le programme
            terminate();
        }

        searchBall();

        //cmpt++;
    }
    stop();
}

int main()
{
  control c;
  if (c.initialized())
    c.comportement();
  return 0;
}
