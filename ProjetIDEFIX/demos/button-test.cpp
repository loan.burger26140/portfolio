#include "ev3dev.h"

#define M_PI 3.14159265358979323846
#include <math.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <fstream>

using namespace std;
using namespace ev3dev;

class control
{
public:
  control();
  ~control();

  void MoveRobot(int angle, int Vlinear, int Vangular,int time= 0);
  void stop();
  void reset();

  bool initialized() const;

  int value2angle(int value);
  void tir();
  void comportement();
  void test_tourne();

  void terminate() { _terminate = true; }
protected:

  enum state
  {
    state_idle,
    state_driving,
    state_turning
  };

  large_motor     _motor_piston;
  large_motor     _motor_face; // EN FACE DE LA BRIQUER
  large_motor     _motor_left; // GAUCHE DE LA BRIQUE
  large_motor     _motor_right; // DROITE DE LA BRIQUE
  //infrared_sensor _sensor_irBalle;
  ultrasonic_sensor _sensor_irBalle;
  i2c_sensor _sensor_irDist;
  
  state _state;
  bool  _terminate;
  float distanceSol;
};

// itinialise les moteur et capteur a chaque port,
// et calcul la distance au sol au debut, pour pouvoir comparer plus tard et voir si la balle est devant nous.
control::control() :
  //_motor_piston(OUTPUT_A),
  _motor_face(OUTPUT_C),
  _motor_left(OUTPUT_B),
  _motor_right(OUTPUT_D),
  _sensor_irBalle(INPUT_AUTO),
  _sensor_irDist(INPUT_AUTO),
  _state(state_idle),
  _terminate(false)
{
  distanceSol = _sensor_irBalle.value();
  cout << "distanceSol : " << distanceSol << std::endl;
}

control::~control()
{
  reset();
}

//deconnecte chaque moteurs
void control::reset()
{
  if (_motor_left.connected())
    _motor_left.reset();

  if (_motor_right.connected())
    _motor_right.reset();

  if (_motor_face.connected())
    _motor_face.reset();
    
  if (_motor_piston.connected())
    _motor_piston.reset();

  _state = state_idle;
}

//verifie que chaque moteur/capteur est bien connecté au bon port.
bool control::initialized() const
{
  cout << "Le moteur piston est connecté : " << _motor_piston.connected() << " - au port " << _motor_piston .address() << endl;
  cout<< "Le moteur gauche est connecté : " << _motor_left.connected() << " - au port " << _motor_left.address() << endl;
  cout<< "Le moteur droit est connecté : " << _motor_right.connected() << " - au port " <<_motor_right.address() << endl;
  cout<< "Le moteur face est conncté : " <<_motor_face.connected() << " - au port "<< _motor_face.address() << endl;
  cout<< "Le capteur de distance ultra son est connecté : "<<_sensor_irBalle.connected()<< " - au port "<<_sensor_irBalle.address() << endl;
  cout<<"Le capteur infra rouge est connecté : "<<_sensor_irDist.connected()<< " - au port "<< _sensor_irDist.address() << endl;

  return (_motor_piston.connected() &&
          _motor_left .connected() &&
          _motor_right.connected() && 
          _motor_face.connected() &&
          _sensor_irBalle.connected() && 
          _sensor_irDist.connected());
}

void control::MoveRobot(int angle, int Vlinear, int Vangular, int time) 
{
  // Calculate the motor speeds
  float cosDegree = cos(angle * M_PI / 180.0);
  float sinDegree = sin(angle * M_PI / 180.0);
  float VwA = Vangular + Vlinear * cosDegree;
  float VwB = Vangular + Vlinear * (-0.5 *cosDegree - 0.866 * sinDegree);
  float VwC = Vangular + Vlinear * (-0.5 *cosDegree + 0.866 * sinDegree);
  _motor_left.set_speed_sp(round(VwC));
  _motor_right.set_speed_sp(round(VwA));
  _motor_face.set_speed_sp(round(VwB));

  _state = state_driving;

  if (time > 0)
  {
    _motor_left .set_time_sp(time).run_timed();
    _motor_right.set_time_sp(time).run_timed();
    _motor_face.set_time_sp(time).run_timed();
    while (_motor_left.state().count("running") || _motor_right.state().count("running") || _motor_face.state().count("running"))
      this_thread::sleep_for(chrono::milliseconds(10));

    _state = state_idle;
  }
  else
  {
    _motor_left.run_forever();
    _motor_right.run_forever();
    _motor_face.run_forever();
  }
}

void control::test_tourne(){
  _motor_left.set_speed_sp(round(1000));
  //_motor_right.set_speed_sp(round(100));
  //_motor_face.set_speed_sp(round(100));

  _motor_left .set_time_sp(2000).run_timed();
  //_motor_right.set_time_sp(2000).run_timed();
  //_motor_face.set_time_sp(2000).run_timed();

  while (_motor_left.state().count("running") || _motor_right.state().count("running") || _motor_face.state().count("running"))
      {
        this_thread::sleep_for(chrono::milliseconds(10));
      }

  terminate();
}

int control::value2angle(int value){
  switch (value)
  {
  case 1:
    return -120;
    break;
  case 2:
    return -90;
    break;
  case 3:
    return -60;
    break;
  case 4:
    return -30;
    break;
  case 5:
    return 0;
    break;
  case 6:
    return 30;
    break;
  case 7:
    return 60;
    break;
  case 8:
    return 90;
    break;
  case 9:
    return 120;
    break;
  case 0:
    return 1222;
    break;
  
  default:
    return 1222;
    break;
  }
}

void control::stop()
{
  _motor_piston.stop();
  _motor_left .stop();
  _motor_right.stop();
  _motor_face.stop();

  _state = state_idle;
}

void control::comportement(){
  while(!_terminate){

    if(abs(distanceSol - _sensor_irBalle.value()) > (distanceSol/4)){ 
      cout << "balle detecte" << endl;
      stop();
      MoveRobot(0, 0, value2angle(_sensor_irDist.value())*2, 300); // aligne la balle pile en face du robot
      test_tourne();
      tir();
    }

    if (_sensor_irBalle.value() < 7){
      terminate();
    }

    if(_sensor_irDist.value() == 0){
      MoveRobot(0, 0, 180);
      //tir();
    }
    else{
      MoveRobot(0, 200, value2angle(_sensor_irDist.value())*2);
      std::cout << "detecte balle : " << value2angle(_sensor_irDist.value()) << std::endl;
    }
  }
  stop();
}


void control::tir(){
    _motor_piston.set_speed_sp(700).set_time_sp(2000).run_timed();
    sound::play("SUIII.wav",true);
}

int main()
{
  control c;
  if (c.initialized())
    c.comportement();
  return 0;
}
